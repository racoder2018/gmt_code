package org.gmt.persistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.dao.UserDoDAO;
import org.gmt.persistence.model.util.UsersTO;
import org.gmt.persistence.model.util.UsersUtil;


/**
 * Session Bean implementation class UsersBean
 * - keeps the session - stateful
 * 
 * RF:	- check parameter values for null
 * 
 * @author Ralf Mehle
 */
@Stateful
@LocalBean
public class UsersBean implements UsersBeanLocal 
{
    private static final long serialVersionUID = 1L;
    private UserDoDAO userDao = UserDoDAO.getInstance();
       
    @PersistenceContext(unitName = "gmtpersistence")
    private EntityManager em;
    
      
    /**
     * Gets a list of all registered users
     */
    @Override
    public UsersTO getAllUsers()
    {	
	return UsersUtil.getAllUsers(em);
    }
        
    /**
     * Gets a user by its ID
     */
    @Override
    public UsersTO getUserById(Long id)
    {
	UserDO user = userDao.getUserById(id, em);
	return UsersUtil.createUsersTO("User with ID " + id, user);
    }
    
    /**
     * Creates a new user
     */
    @Override
    public UsersTO createUser(UserDO user)
    {	
	return UsersUtil.createUsersTO("User added", userDao.createUser(user, em));
    }
    
    /**
     * Updates data of an existing user
     */
    @Override
    public UsersTO updateUser(UserDO user)
    {
	return UsersUtil.createUsersTO("Data of goal " + user.getId() + " changed.", 
		userDao.updateUser(user, em));
    }
    
    /**
     * Deletes a user identified by its ID
     */
    @Override
    public UsersTO deleteUserById(Long id)
    {
	UserDO user = userDao.deleteUserById( id, em );
	return UsersUtil.createUsersTO( "User deleted with ID " + id, user );
    }
}
