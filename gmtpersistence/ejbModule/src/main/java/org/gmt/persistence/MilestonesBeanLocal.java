package org.gmt.persistence;

import java.io.Serializable;

import javax.ejb.Local;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;


@Local
public interface MilestonesBeanLocal extends Serializable 
{
    public MilestonesTO getAllMilestones( Long goalId, Long subGoalId);
    public MilestonesTO getMilestoneById( Long goalId, Long subGoalId, Long id );
    public MilestonesTO createMilestone( Long goalId, Long subGoalId, MilestoneDO milestone );
    public MilestonesTO updateMilestoneById( Long goalId, Long subGoalId, Long id, MilestoneDO milestone);
    public MilestonesTO deleteMilestoneById( Long goalId, Long subGoalId, Long id );
}
