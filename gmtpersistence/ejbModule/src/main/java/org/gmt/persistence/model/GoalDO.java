/**
 * 
 */
package org.gmt.persistence.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The class represents the domain object for a goal.
 * 
 * Additionally, to the definitions in the superclass we need for a goal:
 * "status"	: defined|running|achieved	- A status value is set by caller
 * "category"	: e.g. "private|profession|hobby|family ..." - A category is set by caller
 * "subGoals"	: a list containing all subgoal domain objects associated to a goal
 *
 * RF:	- status and category could be designed more flexible in another release
 * 
 *  @author Ralf Mehle
 */
@Entity
@Table(schema="GMT", name="GOAL")
public class GoalDO extends AbstractGoalDO implements Serializable
{
    private static final long serialVersionUID 	= 1L;
   
    @Column(name = "STATUS") 
    @Enumerated(EnumType.STRING)
    private Status status;    
    
    @Column(name = "CATEGORY") 
    @Enumerated(EnumType.STRING)
    private Category category;
    
    /*
     * We use a bidirectional One-to-many-many-to-one association here which means: 
     * A goal has many subgoals and a subgoal has one goal. 
     * A subgoal consists of many milestones and a milestone belongs to one subgoal.
     * Classes: GoalDO, SubGoalDO, MilestoneDO
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "goal")
    private List<SubGoalDO> subGoals		= new ArrayList<SubGoalDO>();
        
    
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }
    
    /**
     * @return the status
     */
    public Status getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }

    /**
     * @return the category
     */
    public Category getCategory()
    {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(Category category)
    {
        this.category = category;
    }
    
    /**
     * @return the subGoals
     */
    public List<SubGoalDO> getSubGoals()
    {
        return subGoals;
    }
   
    /**
     * @param subGoals 	the subGoals to set
     */
    public void setSubGoals(List<SubGoalDO> subGoals)
    {
        this.subGoals = subGoals;
    }
}
