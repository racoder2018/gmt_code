/**
 * 
 */
package org.gmt.persistence.model.util;

import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.dao.GoalDoDAO;


/**
 * Utilities for subgoals functionality 
 * - builds the subgoals transfer object
 * 
 * RF: move getAllSubGoals() to GoalDoDAO?
 * 
 * @author Ralf Mehle
 */
public class SubGoalsUtil
{  
    public static final Integer RET_CODE_OK 	= new Integer( 0 );
    public static final Integer RET_CODE_ERROR 	= new Integer( -1 );
       
    
    /**
     * Gets a list of all subgoals of a goal identified by its ID
     * 
     * @param 	goalId 	the goal id as long value
     * @param	em	the EntityManager object
     * @return	a SubGoalsTO object
     */
    public static SubGoalsTO getAllSubGoals(Long goalId, EntityManager em)
    {
	SubGoalsTO goalsTO = new SubGoalsTO();
	
	// Get all subgoals from the associated goal
	List<SubGoalDO> subGoalsList = GoalDoDAO.getInstance().getAllSubGoals(goalId, em);
		
	if( subGoalsList == null ) return errorSubGoalsTO();
	goalsTO.setMessage( subGoalsList.size() + " Subgoals" );
	
	goalsTO.getResults().addAll( subGoalsList );
	goalsTO.setReturncode( RET_CODE_OK );
	
	return goalsTO;
    }  
       
    /**
     * Creates a subgoals transfer object
     * 
     * @param 	message		the message as string
     * @param 	subGoalDO	the subgoal
     * @return a SubGoalsTO object
     */
    public static SubGoalsTO createSubGoalsTO(String message, SubGoalDO subGoalDO)
    {
	if( subGoalDO == null ) return errorSubGoalsTO();
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.getResults().add( subGoalDO );
	subGoalsTO.setMessage( message );
	subGoalsTO.setReturncode( RET_CODE_OK );
	      
	return subGoalsTO;
    }  
    
    /**
     * Returns an error message and error code as a SubGoalsTO object.
     * 
     * @return a SubGoalsTO object
     */
    private static SubGoalsTO errorSubGoalsTO()
    {
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage( "Parameter error" );
	subGoalsTO.setReturncode( RET_CODE_ERROR );
        
	return subGoalsTO;
    }
}
