/**
 * 
 */
package org.gmt.persistence.model.util;

import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.dao.GoalDoDAO;


/**
 * Utilities for goals functionality
 *  - builds the goals transfer object
 *  
 * RF: move getAllGoals(), getAllGoalsByStatus to GoalDoDAO?
 *  
 * @author Ralf Mehle
 */
public class GoalsUtil
{
    public static final Integer RET_CODE_OK 	= new Integer( 0 );
    public static final Integer RET_CODE_ERROR 	= new Integer( -1 );
    
    
    /**
     * Gets a list of all goals defined
     * 
     * @param	em	the EntityManager object
     * @return a GoalsTO object
     */
    public static GoalsTO getAllGoals(EntityManager em)
    {
	GoalsTO goalsTO = new GoalsTO();
	List<GoalDO> goalsList = GoalDoDAO.getInstance().getAllGoals(em);
	     	
	if(goalsList == null) return errorGoalsTO();
	goalsTO.setMessage(goalsList.size() + " Goals");
	
	goalsTO.getResults().addAll(goalsList);
	goalsTO.setReturncode(RET_CODE_OK);
	
	return goalsTO;
    }
    
    /**
     * Gets a list of all goals defined identified by status
     * 
     * @param 	status	the status the goals are identified
     * @param	em	the EntityManager object
     * @return a GoalsTO object
     */
    public static GoalsTO getAllGoalsByStatus(Status status, EntityManager em)
    {
	GoalsTO goalsTO = new GoalsTO();
	List<GoalDO> goalsList = GoalDoDAO.getInstance().getAllGoalsByStatus(status, em);
	
	if(goalsList == null) return errorGoalsTO();
	goalsTO.setMessage(goalsList.size() + " Goals");
	
	goalsTO.getResults().addAll(goalsList);
	goalsTO.setReturncode(RET_CODE_OK);
	return goalsTO;
    }
        
    /**
     * Creates a goals transfer object
     * 
     * @param 	message	the message as string
     * @param 	goalDO	the GoalDO object
     * @return a GoalsTO object
     */
    public static GoalsTO createGoalsTO(String message, GoalDO goalDO)
    {
	if(goalDO == null) return errorGoalsTO();
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.getResults().add(goalDO);
	goalsTO.setMessage(message);
	goalsTO.setReturncode(RET_CODE_OK);
	      
	return goalsTO;
    }
    
    /**
     * Returns an error message and error code as a GoalsTO object.
     * 
     * @return a GoalsTO object
     */
    private static GoalsTO errorGoalsTO()
    {
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Parameter error");
	goalsTO.setReturncode(RET_CODE_ERROR);
        
	return goalsTO;
    }
}
