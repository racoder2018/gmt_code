/**
 * 
 */
package org.gmt.persistence.model.util;

import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.dao.GoalDoDAO;


/**
 * Utilities for milestones functionality
 *  - builds the milestones transfer object
 *  
 * RF: move getAllMilestones() to GoalDoDAO?
 *  
 * @author Ralf Mehle
 */
public class MilestonesUtil
{
    public static final Integer RET_CODE_OK 	= new Integer( 0 );
    public static final Integer RET_CODE_ERROR 	= new Integer( -1 );
    
    
    /**
     * Gets a list of all milestones of a subgoal identified by its ID
     * 
     * @param 	goalId		the id of the goal
     * @param 	subGoalId	the id of the subgoal
     * @param	em		the EntityManager object
     * @return a MilestonesTO object
     */
    public static MilestonesTO getAllMilestones(Long goalId, Long subGoalId, EntityManager em)
    {	
	if ((goalId == null) || (subGoalId == null) || (em == null)) 
	    return errorMilestonesTO();
	
	// Get all milestones from the subgoal	
	List<SubGoalDO> subGoalsList = GoalDoDAO.getInstance().getAllSubGoals(goalId, em);
	List<MilestoneDO> milestonesList = null;

	for (int i = 0; i < subGoalsList.size(); i++)
	{
	    if (subGoalsList.get(i).getId().equals(subGoalId))
	    {		 
		milestonesList = subGoalsList.get(i).getMilestones();
	    }
	}		
	if(milestonesList == null) return errorMilestonesTO();
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage(milestonesList.size() + " Milestones");	
	milestonesTO.getResults().addAll(milestonesList);
	milestonesTO.setReturncode(RET_CODE_OK);
	
	return milestonesTO;
    }
    
    /**
     * Creates a milestones transfer object
     * 
     * @param 	message		the message as string
     * @param 	milestoneDO	the MilestonesTO object
     * @return a MilestonesTO object
     */
    public static MilestonesTO createMilestonesTO(String message, MilestoneDO milestoneDO)
    {
	if(milestoneDO == null) return errorMilestonesTO();
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.getResults().add(milestoneDO);
	milestonesTO.setMessage(message);
	milestonesTO.setReturncode(RET_CODE_OK);
	      
	return milestonesTO;
    }
    
    /**
     * Returns an error message and error code as a MilestonesTO object
     * 
     * @return a MilestonesTO object
     */
    private static MilestonesTO errorMilestonesTO()
    {
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Parameter error");
	milestonesTO.setReturncode(RET_CODE_ERROR);
        
	return milestonesTO;
    }
}
