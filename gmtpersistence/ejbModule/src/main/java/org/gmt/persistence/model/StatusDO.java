/**
 * 
 */
package org.gmt.persistence.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The class represents the domain object for a status.
 * "status"	: contains a list of all status defined in enum Status
 * 
 * @author Ralf Mehle
 */
@Entity
@Table(schema="GMT", name="STATUS")
public class StatusDO implements Serializable
{
    private static final long serialVersionUID 	= 1L;

    @Id
    private Long id 				= 0L;    
    
    @Enumerated(EnumType.STRING)    
    private Status status;

   
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }
 
    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the status
     */
    public Status getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }
}
