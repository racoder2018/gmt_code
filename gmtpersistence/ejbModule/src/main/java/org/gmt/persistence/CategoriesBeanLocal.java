package org.gmt.persistence;

import java.io.Serializable;

import javax.ejb.Local;

import org.gmt.persistence.model.util.CategoriesTO;


@Local
public interface CategoriesBeanLocal extends Serializable 
{
    public CategoriesTO getAllCategories();
}
