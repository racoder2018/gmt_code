package org.gmt.persistence;

import java.io.Serializable;

import javax.ejb.Local;

import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.util.GoalsTO;


@Local
public interface GoalsBeanLocal extends Serializable
{
    public GoalsTO getAllGoals();    
    public GoalsTO getAllGoalsByStatus( Status status );
    public GoalsTO getGoalById( Long id );
    public GoalsTO createGoal( GoalDO goal );
    public GoalsTO updateGoal( GoalDO goal );
    public GoalsTO deleteGoalById( Long id );
}
