package org.gmt.persistence;

import java.io.Serializable;

import javax.ejb.Local;

import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.SubGoalsTO;


@Local
public interface SubGoalsBeanLocal extends Serializable
{
    public SubGoalsTO getAllSubGoals( Long goalId );
    public SubGoalsTO getSubGoalById( Long goalId, Long id );
    public SubGoalsTO createSubGoal( Long goalId, SubGoalDO subGoal );
    public SubGoalsTO updateSubGoalById( Long goalId, Long id, SubGoalDO subGoal);
    public SubGoalsTO deleteSubGoalById( Long goalId, Long id );
}
