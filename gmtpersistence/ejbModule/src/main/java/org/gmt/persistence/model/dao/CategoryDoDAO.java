/**
 * 
 */
package org.gmt.persistence.model.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.gmt.persistence.model.CategoryDO;


/**
 *  DAO (Data Access Object) for CRUD-Operations: Read
 * - data is stored later in a database
 * - all data manipulation methods for categories
 * 
 * @author Ralf Mehle
 */
public class CategoryDoDAO
{
    private static final CategoryDoDAO INSTANCE	= new CategoryDoDAO();   
           
    public static CategoryDoDAO getInstance()
    {
	return INSTANCE;
    }

    
    /**
     * Get all categories defined
     * 
     * @param em	the EntityManager object
     * @return list of CategoryDO objects
     */
    public List<CategoryDO> getAllCategories(EntityManager em)
    {
	if (em == null) return null;

	TypedQuery<CategoryDO> query = em.createQuery("SELECT c FROM CategoryDO AS c", CategoryDO.class);
	List<CategoryDO> categories = query.getResultList();
		
	return Collections.unmodifiableList(new ArrayList<>(categories));
    }
}
