/**
 * 
 */
package org.gmt.persistence.model.util;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.UserDO;


/**
 * Users transfer object implementation
 *  
 * @author Ralf Mehle
 */
public class UsersTO
{
    private String message;
    private Integer returncode;
    private List<UserDO> results = new ArrayList<>();
    
    
    /**
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }
    
    /**
     * @param message the message to set
     */
    public void setMessage(String message)
    {
        this.message = message;
    }
    
    /**
     * @return the returncode
     */
    public Integer getReturncode()
    {
        return returncode;
    }
    
    /**
     * @param returncode the returncode to set
     */
    public void setReturncode(Integer returncode)
    {
        this.returncode = returncode;
    }
    
    /**
     * @return the results
     */
    public List<UserDO> getResults()
    {
        return results;
    }
    
    /**
     * @param results the results to set
     */
    public void setResults(List<UserDO> results)
    {
        this.results = results;
    }
}
