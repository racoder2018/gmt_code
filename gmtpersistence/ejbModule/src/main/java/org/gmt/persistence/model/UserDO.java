/**
 * 
 */
package org.gmt.persistence.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The class represents the domain object for a user.
 * "id"		: the user's ID 
 * "firstName" 	: the first name of a user
 * "lastName"	: the last name of a user
 * "loginName"	: the login name of a user
 * "password"	: the user's password
 * 
 * @author Ralf Mehle
 */
@Entity
@Table(schema="GMT", name="REG_USER")   		// table name "USER" collides with SQL USER reserved word
public class UserDO implements Serializable
{
    private static final long serialVersionUID 	= 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id				= 0L;
    private String firstName 			= "";
    private String lastName 			= "";
    private String loginName			= "";
    private String password			= "";   // Todo: The PW must be encrypted!
   
    
    /**
     * @return the id
     */
    public Long getId()
    {
	return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
	this.id = id;
    }
    
    /**
     * @return the firstName
     */
    public String getFirstName()
    {
        return firstName;
    }
    
    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    
    /**
     * @return the lastName
     */
    public String getLastName()
    {
        return lastName;
    }
    
    /**
     * @param lastName the last name to set
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    
    /**
     * @return the loginName
     */
    public String getLoginName()
    {
        return loginName;
    }
    
    /**
     * @param loginName the loginName to set
     */
    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }
    
    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }
    
    /**
     * @param password the password to set
     */
    public void setPassword(String password)
    {
        this.password = password;
    }
}
