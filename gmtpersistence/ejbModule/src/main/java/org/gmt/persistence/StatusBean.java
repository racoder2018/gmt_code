package org.gmt.persistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gmt.persistence.model.util.StatusTO;
import org.gmt.persistence.model.util.StatusUtil;


/**
 * Session Bean implementation class StatusBean
 */
@Stateless
@LocalBean
public class StatusBean implements StatusBeanLocal 
{    
    private static final long serialVersionUID = 1L;

    @PersistenceContext(unitName = "gmtpersistence")
    private EntityManager em;
     
    
    /**
     * Gets a list of all defined status
     */
    @Override
    public StatusTO getAllStatus()
    {
	return StatusUtil.getAllStatus(em);
    }
}
