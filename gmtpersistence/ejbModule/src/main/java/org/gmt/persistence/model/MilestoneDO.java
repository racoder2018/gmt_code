/**
 * 
 */
package org.gmt.persistence.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The class represents the domain object for a Milestone.
 * 
 * "id"			: the ID of a milestone
 * "name"		: the name or of a milestone
 * "description"	: the description of the milestone
 * "todosFinished"	: a list of todos being finished to fulfill the milestone 
 * "notifications"	: true|false - Do we allow notifications, e.g. if a milestone is achieved? 
 * 
 * RF:	- @ManyToOne bidirectional association for SubGoalDO necessary? 
 * 
 * @author Ralf Mehle
 */
@Entity
@Table(schema="GMT", name="MILESTONE")
public class MilestoneDO implements Serializable
{
    private static final long serialVersionUID 	= 1L;
    
    /*
     * We use a bidirectional One-to-many-many-to-one association here which means: 
     * A goal has many subgoals and a subgoal has one goal. 
     * A subgoal consists of many milestones and a milestone belongs to one subgoal.
     * Classes: GoalDO, SubGoalDO, MilestoneDO
     */
    @ManyToOne(optional = false, targetEntity=SubGoalDO.class, cascade=CascadeType.ALL)
    @JoinColumn(referencedColumnName="ID", name = "SUBGOAL_ID") 
    @JsonbTransient
    private SubGoalDO subGoal;
    
    @Id
    private Long id				= 0L;
    private String name				= "";
    private String description			= "";	
    private List<String> todosFinished		= new ArrayList<String>();
    private Boolean notifications		= true;
  
    
    /**
     * @return the subGoal
     */
    @JsonbTransient
    public SubGoalDO getSubGoal()
    {
        return subGoal;
    } 

    /**
     * @param subGoal the subGoal to set
     */
    @JsonbTransient
    public void setSubGoal(SubGoalDO subGoal)
    {
        this.subGoal = subGoal;
    }
    
    /**
     * @return the id
     */
    public Long getId()
    {
	return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
	this.id = id;
    }
    
    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    /**
     * @return the todosFinished
     */
    public List<String> getTodosFinished()
    {
        return todosFinished;
    }
    
    /**
     * @param todosFinished the todosFinished to set
     */
    public void setTodosFinished(List<String> todosFinished)
    {
        this.todosFinished = todosFinished;
    }
        
    /**
     * @return the notifications
     */
    public Boolean getNotifications()
    {
        return notifications;
    }
    
    /**
     * @param notifications the notifications to set
     */
    public void setNotifications(Boolean notifications)
    {
        this.notifications = notifications;
    } 
}
