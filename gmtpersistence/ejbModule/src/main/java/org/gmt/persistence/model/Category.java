/**
 * 
 */
package org.gmt.persistence.model;

/**
 * Contains all categories being defined for a goal. 
 * 
 * FT: In a later version there could be a solution for adding and deleting categories 		
 * 
 * @author Ralf Mehle
 */
public enum Category
{
    UNKNOWN,
    PERSONAL, 
    PROFESSION, 
    FAMILY,
    HOBBY,
    SCHOOL, 
    UNIVERSITY, 
    BUSINESS
}
