package org.gmt.persistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.dao.GoalDoDAO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.gmt.persistence.model.util.MilestonesUtil;


/**
 * Session Bean implementation class MilestonesBean
 */
@Stateless
@LocalBean
public class MilestonesBean implements MilestonesBeanLocal 
{    
    private static final long serialVersionUID = 1L;
    private GoalDoDAO goalDao = GoalDoDAO.getInstance();

    @PersistenceContext(unitName = "gmtpersistence")
    private EntityManager em;
    
    
    /**
     * Gets a subgoal's list of milestones identified by the subgoal's ID
     */
    @Override
    public MilestonesTO getAllMilestones(Long goalId, Long subGoalId)
    {
	return MilestonesUtil.getAllMilestones(goalId, subGoalId, em);
    }
    
    /**
     * Gets a milestone of a subgoal, identified by its ID
     */
    @Override
    public MilestonesTO getMilestoneById(Long goalId, Long subGoalId, Long id)
    {
	MilestoneDO milestone = goalDao.getMilestoneById(goalId, subGoalId, id, em);
	return MilestonesUtil.createMilestonesTO("Milestone with ID " + id, milestone);
    }
    
    /**
     * Creates a new milestone for a subgoal
     */
    @Override
    public MilestonesTO createMilestone(Long goalId, Long subGoalId, MilestoneDO milestone)
    {
	return MilestonesUtil.createMilestonesTO("Milestone added", 
		goalDao.createMilestone(goalId, subGoalId, milestone, em));
    }
        
    /**
     * Change data of a subgoal's milestone defined by its ID
     */
    @Override
    public MilestonesTO updateMilestoneById(Long goalId, Long subGoalId, Long id, MilestoneDO milestone)
    {
	return MilestonesUtil.createMilestonesTO("Data of milestone " + id + " changed.", 
		goalDao.updateMilestoneById(goalId, subGoalId, milestone, em));
    }
    
    /**
     * Delete a subgoal's milestone identified by its ID
     */
    @Override
    public MilestonesTO deleteMilestoneById(Long goalId, Long subGoalId, Long id)
    {
	 return MilestonesUtil.createMilestonesTO("Milestone deleted with ID " + id, 
		goalDao.deleteMilestoneById(goalId, subGoalId, id, em));
    }    
}
