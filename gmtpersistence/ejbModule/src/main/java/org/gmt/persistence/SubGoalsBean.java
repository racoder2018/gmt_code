package org.gmt.persistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.dao.GoalDoDAO;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.gmt.persistence.model.util.SubGoalsUtil;


/**
 * Session Bean implementation class SubGoalsBean
 */
@Stateless
@LocalBean
public class SubGoalsBean implements SubGoalsBeanLocal 
{    
    private static final long serialVersionUID = 1L;
    private GoalDoDAO goalDao = GoalDoDAO.getInstance();
    
    @PersistenceContext(unitName = "gmtpersistence")
    private EntityManager em;
    
    
    /**
     * Gets a goal's list of subgoals identified by the goal's ID
     */
    @Override
    public SubGoalsTO getAllSubGoals(Long goalId)
    {
	return SubGoalsUtil.getAllSubGoals(goalId, em);
    }

    /**
     * Gets a subgoal of a goal, identified by its ID
     */
    @Override
    public SubGoalsTO getSubGoalById(Long goalId, Long id)
    {
	SubGoalDO subGoal = goalDao.getSubGoalById(goalId, id, em);
	return SubGoalsUtil.createSubGoalsTO("Subgoal with ID " + id, subGoal);
    }
    
    /**
     * Creates a new subgoal for a goal
     */
    @Override
    public SubGoalsTO createSubGoal(Long goalId, SubGoalDO subGoal)
    {
	return SubGoalsUtil.createSubGoalsTO("Subgoal added", 
	        goalDao.createSubGoal(goalId, subGoal, em));	
    }

    /**
     * Change data of a goal's subgoal defined by its ID
     */
    @Override
    public SubGoalsTO updateSubGoalById(Long goalId, Long id, SubGoalDO subGoal)
    {
	return SubGoalsUtil.createSubGoalsTO("Data of subgoal " + id + " changed.", 
		goalDao.updateSubGoalById(goalId, subGoal, em));
    }

    /**
     * Delete a goal's subgoal identified by its ID
     */
    @Override
    public SubGoalsTO deleteSubGoalById(Long goalId, Long id)
    {
	return SubGoalsUtil.createSubGoalsTO("Subgoal deleted with ID " + id, 
		goalDao.deleteSubGoalById(goalId, id, em));
    }
}
