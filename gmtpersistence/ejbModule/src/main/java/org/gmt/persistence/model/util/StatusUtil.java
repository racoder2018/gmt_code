/**
 * 
 */
package org.gmt.persistence.model.util;

import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.StatusDO;
import org.gmt.persistence.model.dao.StatusDoDAO;


/**
 * Utilities for status functionality
 *  - builds the status transfer object
 *  
 * @author Ralf Mehle
 */
public class StatusUtil
{
    public static final Integer RET_CODE_OK 	= new Integer( 0 );
    public static final Integer RET_CODE_ERROR 	= new Integer( -1 );
    
    
    /**
     * Gets a list of all status defined
     * 
     * @param	em	the EntityManager object
     * @return a StatusTO object
     */
    public static StatusTO getAllStatus(EntityManager em)
    {
	StatusTO statusTO = new StatusTO();
	List<StatusDO> statusList = StatusDoDAO.getInstance().getAllStatus(em);
		     	
	if(statusList == null) return errorStatusTO();
	statusTO.setMessage(statusList.size() + " Status");

	statusTO.getResults().addAll(statusList);
	statusTO.setReturncode(RET_CODE_OK);
	
	return statusTO;
    }
    
    /**
     * Returns an error message and error code as a StatusTO object.
     * @return a StatusTO object
     */
    private static StatusTO errorStatusTO()
    {
	StatusTO statusTO = new StatusTO();
	statusTO.setMessage("Parameter error");
	statusTO.setReturncode(RET_CODE_ERROR);
        
	return statusTO;
    }
}
