/**
 * 
 */
package org.gmt.persistence.model.util;

import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.CategoryDO;
import org.gmt.persistence.model.dao.CategoryDoDAO;


/**
 * Utilities for categories functionality
 *  - builds the goals transfer object
 *  
 * @author Ralf Mehle
 */
public class CategoriesUtil
{
    public static final Integer RET_CODE_OK 	= new Integer(0);
    public static final Integer RET_CODE_ERROR 	= new Integer(-1);
    
    
    /**
     * Gets a list of all categories defined
     * 
     * @param	em	the EntityManager object
     * @return a CategoriesTO object
     */
    public static CategoriesTO getAllCategories(EntityManager em)
    {
	CategoriesTO categoriesTO = new CategoriesTO();
	List<CategoryDO> categoriesList = CategoryDoDAO.getInstance().getAllCategories(em);
		     	
	if(categoriesList == null) return errorCategoriesTO();
	categoriesTO.setMessage(categoriesList.size() + " Categories");

	categoriesTO.getResults().addAll(categoriesList);
	categoriesTO.setReturncode(RET_CODE_OK);
	
	return categoriesTO;
    }
    
    /**
     * Returns an error message and error code as a CategoriesTO object.
     * 
     * @return a CategoriesTO object
     */
    private static CategoriesTO errorCategoriesTO()
    {
	CategoriesTO categoriesTO = new CategoriesTO();
	categoriesTO.setMessage("Parameter error");
	categoriesTO.setReturncode(RET_CODE_ERROR);
        
	return categoriesTO;
    }
}
