package org.gmt.persistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gmt.persistence.model.util.CategoriesTO;
import org.gmt.persistence.model.util.CategoriesUtil;


/**
 * Session Bean implementation class CategoriesBean
 */
@Stateless
@LocalBean
public class CategoriesBean implements CategoriesBeanLocal 
{    
    private static final long serialVersionUID = 1L;
    
    @PersistenceContext(unitName = "gmtpersistence")
    private EntityManager em;
    
    
    /**
     * Gets a list of all defined categories
     */
    @Override
    public CategoriesTO getAllCategories()
    {
	return CategoriesUtil.getAllCategories(em);
    }
}
