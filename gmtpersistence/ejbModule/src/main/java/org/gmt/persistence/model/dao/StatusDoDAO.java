/**
 * 
 */
package org.gmt.persistence.model.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.gmt.persistence.model.StatusDO;

import com.sun.msv.verifier.jarv.TheFactoryImpl;


/**
 * DAO (Data Access Object) for CRUD-Operations: Read
 * - data is stored later in a database
 * - all data manipulation methods for status
 * 
 * @author Ralf Mehle
 */
public class StatusDoDAO
{
    private static final StatusDoDAO INSTANCE = new StatusDoDAO();
           
    public static StatusDoDAO getInstance()
    {
	return INSTANCE;
    }

    
    /**
     * Get all status defined
     * 
     * @param 	em	the EntityManager object
     * @return a list of StatusDO objects
     */
    public List<StatusDO> getAllStatus(EntityManager em)
    {
	if (em == null) return null;

	TypedQuery<StatusDO> query = em.createQuery("SELECT s FROM StatusDO AS s", StatusDO.class);
	List<StatusDO> status = query.getResultList();
		
	return Collections.unmodifiableList(new ArrayList<>(status));
    }
}
