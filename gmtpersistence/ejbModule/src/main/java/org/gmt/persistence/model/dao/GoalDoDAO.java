/**
 * 
 */
package org.gmt.persistence.model.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;


/**
 * DAO (Data Access Object) for CRUD-Operations (Create, Read, Update, Delete)
 * - data is stored in a map for now, later in a database
 * - all data manipulation methods for goals, subgoals and milestones
 * 
 * RF: - Data separation for subgoals and milestones in separate modules?
 *     - Should all subgoals be searched by status, too?
 * 
 * @author Ralf Mehle
 */
public class GoalDoDAO
{
    private static final GoalDoDAO INSTANCE = new GoalDoDAO();
           
    public static GoalDoDAO getInstance()
    {
	return INSTANCE;
    }

    
    /**
     * Get all goals defined
     * 
     * @param em 	the EntityManager object
     * @return a list of GoalDO objects
     */
    public List<GoalDO> getAllGoals(EntityManager em)
    {
	if (em == null) return null;

	TypedQuery<GoalDO> query = em.createQuery("SELECT g FROM GoalDO g", GoalDO.class);
	List<GoalDO> goals = query.getResultList();
		
	return Collections.unmodifiableList(new ArrayList<>(goals));
    }
   
    /** 
     * Get all goals defined identified by its status    
     * 
     * @param status	the Status object
     * @param em 	the EntityManager object
     * @return a list of GoalDO objects
    */
    public List<GoalDO> getAllGoalsByStatus(Status status, EntityManager em)
    {
	if ((status == null) || (em == null)) return null;

	TypedQuery<GoalDO> query = em.createQuery(
		"SELECT g FROM GoalDO AS g WHERE g.status = :status", GoalDO.class);
	query.setParameter("status", status);
	List<GoalDO> goals = query.getResultList();
		
	return Collections.unmodifiableList(new ArrayList<>(goals));
    }

    /**
     * Get a goal identified by ID
     * 
     * @param 	id 	the id as long value
     * @param	em	the EntityManager object
     * @return	a GoalDO object
     */
    public GoalDO getGoalById(Long id, EntityManager em)
    {
	if ((id == null) || (em == null)) return null;

	GoalDO goal = em.find(GoalDO.class, id);
	if (goal == null)
	    throw new RuntimeException("Error: There is no goal with ID " + id + ".");
	
	return goal;    
    }
    
    /**
     * Gets a list of all subgoals of a goal identified by its ID
     * 
     * @param 	goalId	the id of the subgoal
     * @param	em	the EntityManager object
     * @return a list of SubGoalDO objects
     */
    public List<SubGoalDO> getAllSubGoals(Long goalId, EntityManager em)
    {
	if ((goalId == null) || (em == null)) return null;

	GoalDO goal = getGoalById(goalId, em);
	return Collections.unmodifiableList(new ArrayList<>(goal.getSubGoals()));
    }
    
    /**
     * Gets a list of all milestones of a subgoal identified by its ID
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoalId	the subgoal id 
     * @param	em		the EntityManager object
     * @return a list of MilestoneDO objects
     */
    public List<MilestoneDO> getAllMilestones(Long goalId, Long subGoalId, EntityManager em)
    {
	if ((goalId == null) || (subGoalId == null) || (em == null)) return null;

	SubGoalDO subGoal = getSubGoalById(goalId, subGoalId, em);	
	return Collections.unmodifiableList(new ArrayList<>(subGoal.getMilestones()));
    }
    
    /**
     * Get a goal's subgoal identified by their IDs
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoalId	the subgoal id
     * @param	em		the EntityManager object
     * @return a SubGoalDO object
     */
    public SubGoalDO getSubGoalById(Long goalId, Long subGoalId, EntityManager em)
    {
	if ((goalId == null) || (subGoalId == null) || (em == null)) return null;
	
	GoalDO goal = getGoalById(goalId, em);
	List<SubGoalDO> subGoals = goal.getSubGoals();
	
	SubGoalDO subGoalById = null;
	for(int i = 0; i < subGoals.size(); i++)
	{
	    if (subGoals.get(i).getId().equals(subGoalId))
	    {
		subGoalById = subGoals.get(i);
		subGoalById.setGoal(goal);
	    }
	}
	if (subGoalById == null)
	    throw new RuntimeException("Error: There is no subgoal with ID " + subGoalId + ".");
	
	return subGoalById;
    }
    
    /**
     * Get a subgoal's milestone identified by their IDs
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoalId	the subgoal id
     * @param 	milestoneId	the milestone id
     * @param	em		the EntityManager object
     * @return a MilestoneDO object
     */
    public MilestoneDO getMilestoneById(Long goalId, Long subGoalId, Long milestoneId, EntityManager em)
    {
	if ((goalId == null) || (subGoalId == null) || 
		(milestoneId == null) || (em == null)) return null;

	SubGoalDO subGoal = getSubGoalById(goalId, subGoalId, em);	
	MilestoneDO milestone = em.find(MilestoneDO.class, milestoneId);
	if (milestone == null)
	    throw new RuntimeException("Error: There is no milestone with ID " + milestoneId + ".");
	
	milestone.setSubGoal(subGoal);
	return milestone;
    }
    
    /**
     * Create a new goal
     * 
     * @param 	goal	the new goal being created 
     * @param	em	the EntityManager object
     * @return a GoalDO object
     */
    public GoalDO createGoal(GoalDO goal, EntityManager em)
    {
	if ((goal == null) || (em == null)) return null;

	// The many-to-many relation needs setting the goal object for all subgoals -> all FKs are saved
	List<SubGoalDO> subGoals = goal.getSubGoals();
	for (int i = 0; i < subGoals.size(); i++)
	{
	    subGoals.get(i).setGoal(goal);
	    List<MilestoneDO> milestones = subGoals.get(i).getMilestones();
	    for (int j = 0; j < milestones.size(); j++)
	    {
		milestones.get(j).setSubGoal(subGoals.get(i));
	    }
	}
	em.persist(goal);
	em.flush();
		
	return goal;
    }

    /**
     * Creates a new subgoal of a goal
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoal		the subgoal
     * @param	em		the EntityManager object
     * 
     * TC: - There must be no milestone added instead a subgoal!
     * 
     * @return a SubGoalDO object
     */
    public SubGoalDO createSubGoal(Long goalId, SubGoalDO subGoal, EntityManager em)
    {
	if ((goalId == null) || (subGoal == null) || (em == null)) return null;
		
	/* The many-to-many relation needs setting the subgoal object for all milestones 
	 * -> all FKs are saved
	 */
	GoalDO goal = getGoalById(goalId, em);
	subGoal.setGoal(goal); 
	
	List<MilestoneDO> milestones = subGoal.getMilestones();
	for (int i = 0; i < milestones.size(); i++)
	{
	    milestones.get(i).setSubGoal(subGoal);
	}	
	em.persist(subGoal);
	em.flush();
	
	return subGoal;
    }
    
    /**
     * Creates a new milestone of a subgoal
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoalId	the subgoal id
     * @param 	milestone	the milestone
     * @param	em		the EntityManager object
     * 
     * TC: - There must be no milestone added instead a subgoal!
     * RF: - extract same code as in updateMilestone() in a separate method 
     * 
     * @return a MilestoneDO object
     */
    public MilestoneDO createMilestone(Long goalId, Long subGoalId, MilestoneDO milestone, EntityManager em)
    {
	if ((goalId == null) || (subGoalId == null) || 
		(milestone == null) || (em == null)) return null;

	/* 
	 * The many-to-many relation needs setting the subgoal object for 
	 * all milestones -> all FKs are saved. 
	 */
	GoalDO goal = getGoalById(goalId, em);
	List<SubGoalDO> subGoals = goal.getSubGoals();
	
	SubGoalDO subGoalById = null;	
	for (SubGoalDO tempSubGoal : subGoals)
	{
	    if (tempSubGoal.getId().equals(subGoalId))
	    {
		subGoalById = tempSubGoal;
		subGoalById.setGoal(goal);
		milestone.setSubGoal(subGoalById);		
		break;
	    }	   
	}	
	if (subGoalById == null)
	    throw new RuntimeException("Error: There is no subgoal with ID " + subGoalId + ".");
	
	em.persist(milestone);
	em.flush();
	
	return milestone;
    }
    
    /**
     * Updates a goal's data identified by its ID
     * 
     * @param 	goal	the goal being updated
     * @param	em	the EntityManager object
     * @return a GoalDO object
     */
    public GoalDO updateGoal(GoalDO goal, EntityManager em)
    {
	if ((goal == null) || (em == null)) return null;
	
	// The many-to-many relation needs setting the goal object for all subgoals -> all FKs are saved
	List<SubGoalDO> subGoals = goal.getSubGoals();
	for (int i = 0; i < subGoals.size(); i++)
	{
	    subGoals.get(i).setGoal(goal);
	    List<MilestoneDO> milestones = subGoals.get(i).getMilestones();
	    for (int j = 0; j < milestones.size(); j++)
	    {
		milestones.get(j).setSubGoal(subGoals.get(i));
	    }
	}
	em.merge(goal);
	em.flush();

	return goal;
    }

    /**
     * Updates a subgoal's data identified by the associated goal's ID
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoal		the subgoal
     * @param	em		the EntityManager object
     * @return	a SubGoalDO object
     */
    public SubGoalDO updateSubGoalById(Long goalId, SubGoalDO subGoal, EntityManager em)
    {
	if ((goalId == null) || (subGoal == null) || (em == null)) return null;

	/* 
	 * The many-to-many relation needs setting the subgoal object for all milestones 
	 * -> all FKs are saved
	 */
	GoalDO goal = getGoalById(goalId, em);
	subGoal.setGoal(goal); 
	
	List<MilestoneDO> milestones = subGoal.getMilestones();
	for (int i = 0; i < milestones.size(); i++)
	{
	    milestones.get(i).setSubGoal(subGoal);
	}
	em.merge(subGoal);
	em.flush();
	
	return subGoal;
    }
    
    /**
     * Updates a milestone's data identified by the associated goal and subgoal IDs
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoalId	the subgoal id
     * @param 	milestone	the milestone
     * @param	em		the EntityManager object
     * 
     * RF: - extract same code as in createMilestone() in a separate method 
     * 
     * @return a MilestoneDO object
     */
    public MilestoneDO updateMilestoneById(Long goalId, Long subGoalId, MilestoneDO milestone, EntityManager em)
    {
	if ((goalId == null) || (subGoalId == null) || 
		(milestone == null) || (em == null)) return null;

	/* 
	 * The many-to-many relation needs setting the milestones object -> all FKs are saved. 
	 */
	GoalDO goal = getGoalById(goalId, em);
	List<SubGoalDO> subGoals = goal.getSubGoals();
	
	SubGoalDO subGoalById = null;	
	for (SubGoalDO tempSubGoal : subGoals)
	{
	    if (tempSubGoal.getId().equals(subGoalId))
	    {
		subGoalById = tempSubGoal;
		subGoalById.setGoal(goal); 
		milestone.setSubGoal(subGoalById);			
		break;
	    }	   
	}	
	if (subGoalById == null)
	    throw new RuntimeException("Error: There is no subgoal with ID " + subGoalId + ".");
		
	em.merge(milestone);
	em.flush();
	
	return milestone;	
    }
    
    /**
     * Deletes a goal identified by its ID
     * 
     * @param 	id	the id of the goal being deleted
     * @param	em	the EntityManager object
     * @return a GoalDO object
     */
    public GoalDO deleteGoalById(Long id, EntityManager em)
    {	
	if ((id == null) || (em == null)) return null;

	GoalDO goal = getGoalById(id, em);
	
	// The many-to-many relation needs setting the goal object for all subgoals -> all FKs are saved
	List<SubGoalDO> subGoals = goal.getSubGoals();
	for (int i = 0; i < subGoals.size(); i++)
	{
	    subGoals.get(i).setGoal(goal);
	    List<MilestoneDO> milestones = subGoals.get(i).getMilestones();
	    for (int j = 0; j < milestones.size(); j++)
	    {
		milestones.get(j).setSubGoal(subGoals.get(i));
	    }
	}	
	em.remove(goal);
	em.flush();

	return goal;
    }
    
    /**
     * Deletes a goal's subgoal identified by their IDs
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoalId	the subgoal id
     * @param	em		the EntityManager object
     * @return 	a SubGoalDO object
     */
    public SubGoalDO deleteSubGoalById(Long goalId, Long subGoalId, EntityManager em)
    {
	if ((goalId == null) || (em == null)) return null;
	
	/* 
	 * The many-to-many relation needs setting the subgoal object for 
	 * all milestones -> all FKs are saved. The goal stays persisted. 
	 * The subgoal and the acc. milestones if avail. are removed.
	 */
	GoalDO goal = getGoalById(goalId, em);
	List<SubGoalDO> subGoals = goal.getSubGoals();
	
	SubGoalDO subGoalById = null;
	for (SubGoalDO tempSubGoal : subGoals)
	{
	    if (tempSubGoal.getId().equals(subGoalId))
	    {
		subGoalById = tempSubGoal;		
		subGoalById.setGoal(null);
		List<MilestoneDO> milestones = subGoalById.getMilestones();
		for (int i = 0; i < milestones.size(); i++)
		{
		    milestones.get(i).setSubGoal(subGoalById);
		}
		break;
	    }	    
	}
	if (subGoalById == null)
	    throw new RuntimeException("Error: There is no subgoal with ID " + subGoalId + ".");
	
	em.remove(subGoalById);
	em.flush();

	return subGoalById;
    }
    
    /**
     * Deletes a subgoal's milestone identified by their IDs
     * 
     * @param 	goalId		the goal id 
     * @param 	subGoalId	the subgoal id
     * @param 	milestoneId	the milestone id
     * @param	em		the EntityManager object
     * @return 	a MilestoneDO object
     */
    public MilestoneDO deleteMilestoneById(Long goalId, Long subGoalId, Long milestoneId, EntityManager em)
    {
	if ((goalId == null) || (subGoalId == null) || 
		(milestoneId == null) || (em == null)) return null;

	/* 
	 * The many-to-many relation needs setting the milestone object 
	 * -> all FKs are saved. The goal and the subgoal 
	 * stay persisted. The acc. milestones if avail. are removed.
	 */
	GoalDO goal = getGoalById(goalId, em);

	SubGoalDO subGoalById = null;	
	MilestoneDO milestoneById = null;
	for (SubGoalDO tempSubGoal : goal.getSubGoals())
	{	    
	    if (tempSubGoal.getId().equals(subGoalId))
	    {
		subGoalById = tempSubGoal;		
		for (MilestoneDO tempMilestone : tempSubGoal.getMilestones())		
		{
		    if (tempMilestone.getId().equals(milestoneId))
		    {
			milestoneById = tempMilestone;
			milestoneById.setSubGoal(null);
			break;
		    }
		}	
		break;
	    }	   
	}	
	if (subGoalById == null)
	    throw new RuntimeException("Error: There is no subgoal with ID " + subGoalId + ".");
	if (milestoneById == null)
	    throw new RuntimeException("Error: There is no milestone with ID " + milestoneId + ".");
	
	em.remove(milestoneById);
	em.flush();
	
	return milestoneById;
    }
}
