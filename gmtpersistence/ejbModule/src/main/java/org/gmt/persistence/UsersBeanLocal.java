package org.gmt.persistence;

import java.io.Serializable;

import javax.ejb.Local;

import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.util.UsersTO;


@Local
public interface UsersBeanLocal extends Serializable 
{
    public UsersTO getAllUsers();
    public UsersTO getUserById( Long id );
    public UsersTO createUser( UserDO user);
    public UsersTO updateUser( UserDO user );
    public UsersTO deleteUserById( Long id );
}
