/**
 * 
 */
package org.gmt.persistence.model.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.GoalDO;


/**
 * Goals transfer object
 * 
 * @author Ralf Mehle
 */
public class GoalsTO implements Serializable
{
    private static final long serialVersionUID = 1L;
 
    private String message;
    private Integer returncode;
    private List<GoalDO> results = new ArrayList<>();
    
    
    /**
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }
    
    /**
     * @param message the message to set
     */
    public void setMessage(String message)
    {
        this.message = message;
    }
    
    /**
     * @return the returncode
     */
    public Integer getReturncode()
    {
        return returncode;
    }
    
    /**
     * @param returncode the returncode to set
     */
    public void setReturncode(Integer returncode)
    {
        this.returncode = returncode;
    }
    
    /**
     * @return the results
     */
    public List<GoalDO> getResults()
    {
        return results;
    }
}
