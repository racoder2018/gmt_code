/**
 * 
 */
package org.gmt.persistence.model.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.gmt.persistence.model.UserDO;


/**
 * DAO (Data Access Object) for CRUD-Operations (Create, Read, Update, Delete)
 * - data is stored in a map for now, later in a database
 * - all data manipulation methods for users
 *
 * RF: 	- change ...ById()? and getAllUsersList() to getAllUsers()?
 * 
 * @author Ralf Mehle
 */
public class UserDoDAO
{
    private static final UserDoDAO INSTANCE = new UserDoDAO();
           
    public static UserDoDAO getInstance()
    {
	return INSTANCE;
    }

    
    /**
     * Get all users defined
     * 
     * @param	em	the EntityManager object
     * @return	a list of UserDO objects
     */
    public List<UserDO> getAllUsers(EntityManager em)
    {	
	if (em == null) return null;

	TypedQuery<UserDO> query = em.createQuery("SELECT u FROM UserDO u", UserDO.class);
	List<UserDO> users = query.getResultList();
		
	return Collections.unmodifiableList(new ArrayList<>(users));
    }
    
    /**
     * Get a user identified by ID
     * 
     * @param	em	the EntityManager object
     * @param 	id	the id of the user
     * @return a UserDO object
     */
    public UserDO getUserById(Long id, EntityManager em)
    {
	if ((id == null) || (em == null)) return null;

	UserDO user = em.find(UserDO.class, id);	
	
	return user;
    }
    
    /**
     * Create a new user
     * 
     * @param 	user	the new user
     * @param	em	the EntityManager object
     * @return a UserDO object
     */
    public UserDO createUser(UserDO user, EntityManager em)
    {
	if ((user == null) || (em == null)) return null;

	em.persist(user);
	em.flush();
	
	return user;
    }
    
    /**
     * Updates data of an existing user
     * 
     * @param 	user	the user being updated
     * @param	em	the EntityManager object
     * @return	a UserDO object
     */
    public UserDO updateUser(UserDO user, EntityManager em)
    {
	if ((user == null) || (em == null)) return null;
	
	em.merge(user);
	em.flush();

	return user;
    }

    /**
     * Deletes a user identified by its ID
     * 
     * @param 	id	the id of the user being deleted
     * @param	em	the EntityManager object
     * @return	a UserDO object
     */
    public UserDO deleteUserById(Long id, EntityManager em)
    {
	if ((id == null) || (em == null)) return null;

	UserDO user = em.find(UserDO.class, id);
	em.remove( user );
	em.flush();

	return user;
    }
}
