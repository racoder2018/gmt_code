/**
 * 
 */
package org.gmt.persistence.model;

/**
 * Contains all status being defined for a goal. 
 * 
 * FT: In a later version there could be a solution for adding and deleting status 		
 * 
 * @author Ralf Mehle
 */
public enum Status
{
    UNKNOWN,
    DEFINED, 
    RUNNING, 
    ACHIEVED
}
