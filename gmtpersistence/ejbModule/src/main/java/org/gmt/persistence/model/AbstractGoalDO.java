/**
 * 
 */
package org.gmt.persistence.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;


/**
 * This class stores the data shared in both classes for goal and subgoal data objects.
 * 
 * The format of an abstract goal domain object is represented as follows:
 * "id" 		: 1				- Unique ID 
 * "shortTerm" 		: "A goal in short"
 * "description"	: "The long version of a goal"
 * "beginDate"		: e.g. 2018-04-01
 * "endDate"		: e.g. 2018-05-10
 * "notifications"	: true|false			- Do we allow notifications, e.g. if a goal is achieved? 
 * 
 * RF:	- set default values if a property is not set in the request
 *  
 * @author Ralf Mehle
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractGoalDO implements Serializable
{
    private static final long serialVersionUID 	= 1L;
    
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id 				= 0L;
    
    @Column(name="SHORTTERM")
    protected String shortTerm			= "";	
    
    @Column(name="DESCRIPTION")
    protected String description		= "";
    
    @Column(name="BEGINDATE")
    @Convert(converter=org.gmt.persistence.model.util.LocalDateTimeAttributeConverter.class)
    protected LocalDateTime beginDate;
    
    @Column(name="ENDDATE")
    protected LocalDateTime endDate;
    
    @Column(name="NOTIFICATIONS")
    protected Boolean notifications		= true;
    
    
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }
    
    /**
     * @return the shortTerm
     */
    public String getShortTerm()
    {
        return shortTerm;
    }
    
    /**
     * @param shortTerm the shortTerm to set
     */
    public void setShortTerm(String shortTerm)
    {
        this.shortTerm = shortTerm;
    }
    
    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    /**
     * @return the beginDate
     */
    public LocalDateTime getBeginDate()
    {
        return beginDate;
    }
    
    /**
     * @param beginDate the beginDate to set
     */
    public void setBeginDate(LocalDateTime beginDate)
    {
        this.beginDate = beginDate;
    }
    
    /**
     * @return the endDate
     */
    public LocalDateTime getEndDate()
    {
        return endDate;
    }
    
    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDateTime endDate)
    {
        this.endDate = endDate;
    }
        
    /**
     * @return the notifications
     */
    public Boolean getNotifications()
    {
        return notifications;
    }
    
    /**
     * @param notifications the notifications to set
     */
    public void setNotifications(Boolean notifications)
    {
        this.notifications = notifications;
    }    
}
