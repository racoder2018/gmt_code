/**
 * 
 */
package org.gmt.persistence.model.util;

import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.dao.UserDoDAO;


/**
 * Utilities for users functionality:
 *  - builds the users transfer object
 * 
 * @author Ralf Mehle
 */
public class UsersUtil
{
    public static final Integer RET_CODE_OK 	= new Integer( 0 );
    public static final Integer RET_CODE_ERROR 	= new Integer( -1 );
    
    
    /**
     * Gets a list of all users defined
     * 
     * @param	em	the EntityManager object
     * @return 	a UsersTO object
     */
    public static UsersTO getAllUsers(EntityManager em)
    {
	UsersTO usersTO = new UsersTO();
	List<UserDO> goalsList = UserDoDAO.getInstance().getAllUsers(em);
	     	
	if(goalsList == null) return errorUsersTO();
	usersTO.setMessage(goalsList.size() + " Users");
	
	usersTO.getResults().addAll(goalsList);
	usersTO.setReturncode(RET_CODE_OK);
	
	return usersTO;
    }
    
    /**
     * Creates a users transfer object
     * 
     * @param message	the message
     * @param user	a UserDO object
     * @return a UsersTO object
     */
    public static UsersTO createUsersTO(String message, UserDO user)
    {
	if(user == null) return errorUsersTO();

	UsersTO usersTO = new UsersTO();
	usersTO.getResults().add(user);
	usersTO.setMessage(message);
	usersTO.setReturncode(RET_CODE_OK);

	return usersTO;
    }
    
    /**
     * Returns an error message and error code as a UsersTO object.
     * @return
     */
    private static UsersTO errorUsersTO()
    {
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Parameter error");
	usersTO.setReturncode(RET_CODE_ERROR);
        
	return usersTO;
    }
}
