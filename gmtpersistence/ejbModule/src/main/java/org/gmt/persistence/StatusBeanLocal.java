package org.gmt.persistence;

import java.io.Serializable;

import javax.ejb.Local;

import org.gmt.persistence.model.util.StatusTO;


@Local
public interface StatusBeanLocal extends Serializable
{
    public StatusTO getAllStatus();
}
