package org.gmt.persistence;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.dao.GoalDoDAO;
import org.gmt.persistence.model.util.GoalsTO;
import org.gmt.persistence.model.util.GoalsUtil;


/**
 * Session Bean implementation class GoalsBean
 * 
 * RF:	- check parameter values for null
 * 
 * @author Ralf Mehle
 */
@Stateless
@LocalBean
public class GoalsBean implements GoalsBeanLocal
{ 
    private static final long serialVersionUID = 1L;
    private GoalDoDAO goalDao = GoalDoDAO.getInstance();
    
    @PersistenceContext(unitName = "gmtpersistence")
    private EntityManager em;

   
    /**
     * Gets a list of all goals 
     */
    @Override
    public GoalsTO getAllGoals()
    {	
	return GoalsUtil.getAllGoals(em);
    }
    
    /**
     * Gets a list of goals identified by their status
     */
    @Override
    public GoalsTO getAllGoalsByStatus(Status status)
    {
	return GoalsUtil.getAllGoalsByStatus(status, em);
    }
    
    /**
     * Gets a goal by its ID
     */
    @Override
    public GoalsTO getGoalById(Long id)
    {
	GoalDO goal = goalDao.getGoalById(id, em);
	return GoalsUtil.createGoalsTO("Goal with ID " + id, goal);
    }
    
    /**
     * Creates a new goal
     */
    @Override
    public GoalsTO createGoal(GoalDO goal)
    {
	GoalDO goalDO = goalDao.createGoal(goal, em);
	
	// We have a StackOverFlowError here at the JSON serialization of the GoalsTO object!
	// So, for the moment goalsTO stays empty - no JSON return value.
	GoalsTO goalsTO = GoalsUtil.createGoalsTO("Goal added", goalDO);
	return goalsTO;
    }
       
    /**
     * Updates data of an existing goal
     */
    @Override
    public GoalsTO updateGoal(GoalDO goal)
    {
	GoalDO goalDO = goalDao.updateGoal(goal, em);
	GoalsTO goalsTO = GoalsUtil.createGoalsTO("Data of goal " + goal.getId() + " changed.", goalDO);
	return goalsTO;	
    }
    
    /**
     * Deletes a goal identified by its ID
     */
    @Override
    public GoalsTO deleteGoalById(Long id)
    {
	GoalDO goal = goalDao.deleteGoalById(id, em);
	GoalsTO goalsTO = GoalsUtil.createGoalsTO("Goal deleted with ID " + id, goal);
	return goalsTO;
    }    
}
