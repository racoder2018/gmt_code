/**
 * 
 */
package org.gmt.persistence.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The class represents the domain object for a category.
 * "categories"	: contains a list of all categories defined in enum Category
 * 
 * @author Ralf Mehle
 */
@Entity
@Table(schema="GMT", name="CATEGORY")
public class CategoryDO implements Serializable
{
    private static final long serialVersionUID 	= 1L;

    @Id
    private Long id 		  		= 0L;    
    
    @Enumerated(EnumType.STRING)
    private Category category;

    
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }
 
    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    } 
    
    /**
     * @return the categories
     */
    public Category getCategory()
    {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(Category category)
    {
        this.category = category;
    }
}
