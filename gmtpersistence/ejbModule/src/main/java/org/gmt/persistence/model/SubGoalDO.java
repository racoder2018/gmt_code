/**
 * 
 */
package org.gmt.persistence.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The class represents the domain object for a SubGoal.
 * 
 * Additionally, to the definitions in the superclass we need for a subgoal:
 * "status"	: defined|running|achieved	- A status value is set by caller
 * "milestones"	: a list containing all milestones domain objects associated to a subgoal
 * 
 * RF:	- @ManyToOne bidirectional association for GoalDO necessary? 
 * 
 *  @author Ralf Mehle
 */
@Entity
@Table(schema="GMT", name="SUBGOAL")
public class SubGoalDO extends AbstractGoalDO implements Serializable
{
    private static final long serialVersionUID 	= 1L;
     
    /*
     * We use a bidirectional One-to-many-many-to-one association here which means: 
     * A goal has many subgoals and a subgoal has one goal. 
     * A subgoal consists of many milestones and a milestone belongs to one subgoal.
     * Classes: GoalDO, SubGoalDO, MilestoneDO
     */
    @ManyToOne(optional = false, targetEntity=GoalDO.class, cascade=CascadeType.ALL)
    @JoinColumn(referencedColumnName="ID", name = "GOAL_ID")
    @JsonbTransient
    private GoalDO goal;
    
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private Status status;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subGoal")
    private List<MilestoneDO> milestones 	= new ArrayList<MilestoneDO>();

     
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * @return the goal
     */
    @JsonbTransient
    public GoalDO getGoal()
    {
        return goal;
    }

    /**
     * @param goal the goal to set
     */
    @JsonbTransient
    public void setGoal(GoalDO goal)
    {
        this.goal = goal;
    }
    
    /**
     * @return the status
     */
    public Status getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }
    
    /**
     * @return the milestones
     */
    public List<MilestoneDO> getMilestones()
    {
        return milestones;
    }

    /**
     * @param milestones	the milestones to set
     */
    public void setMilestones(List<MilestoneDO> milestones)
    {
        this.milestones = milestones;
    }
}
