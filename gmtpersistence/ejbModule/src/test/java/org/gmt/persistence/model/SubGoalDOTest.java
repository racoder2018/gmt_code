/**
 * 
 */
package org.gmt.persistence.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.SubGoalDO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.SubGoalDO")
class SubGoalDOTest
{
    private static SubGoalDO subGoalDOTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.SubGoalDO");
        subGoalDOTest = new SubGoalDO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.SubGoalDO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.SubGoalDO#getId()/setId()}.
     */
    @Test
    @DisplayName("Long = getId()/setId()")
    final void testGetSetId()
    {
	subGoalDOTest.setId(10L);
	assertEquals(new Long(10L), subGoalDOTest.getId());

	subGoalDOTest.setId(-1L);
	assertEquals(new Long(-1L), subGoalDOTest.getId());

	subGoalDOTest.setId(0L);
	assertEquals(new Long(0L), subGoalDOTest.getId());   
    }

    /**
     * Test method for {@link org.gmt.persistence.model.SubGoalDO#getGoal()/setGoal()}.
     */
    @Test
    @DisplayName("GoalDO = getGoal()/setGoal()")
    final void testGetSetGoal()
    {
	GoalDO goal = new GoalDO();
	goal.setDescription("Goal for testing");
	
	subGoalDOTest.setGoal(goal);
	assertEquals(goal, subGoalDOTest.getGoal());
	
	subGoalDOTest.setGoal(null);
	assertNull(subGoalDOTest.getGoal());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.SubGoalDO#getStatus()/setStatus()}.
     */
    @Test
    @DisplayName("Status = getStatus()/setStatus()")
    final void testGetSetStatus()
    {
	subGoalDOTest.setStatus(Status.RUNNING);
	assertEquals(Status.RUNNING, subGoalDOTest.getStatus());
	
	subGoalDOTest.setStatus(null);
	assertNull(subGoalDOTest.getStatus());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.SubGoalDO#getMilestones()/setMilestones()}.
     */
    @Test
    @DisplayName("MilestoneDO = getMilestones()/setMilestones()")
    final void testGetSetMilestones()
    {

	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(200L);
	
	List<MilestoneDO> milestones = new ArrayList<>();
	milestones.add(milestone);
	
	subGoalDOTest.setMilestones(milestones);
	assertEquals(new Long(200L), subGoalDOTest.getMilestones().get(0).getId());
	
	subGoalDOTest.setMilestones(null);
	assertNull(subGoalDOTest.getMilestones());
	
	subGoalDOTest.setMilestones(milestones);
    }
}
