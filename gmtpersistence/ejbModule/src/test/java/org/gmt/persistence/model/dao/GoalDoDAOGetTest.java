/**
 * 
 */
package org.gmt.persistence.model.dao;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.dao.GoalDoDAO;
import org.gmt.persistence.model.util.GoalsTO;
import org.gmt.persistence.model.util.GoalsUtil;
import org.gmt.persistence.model.util.MilestonesTO;
import org.gmt.persistence.model.util.MilestonesUtil;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.gmt.persistence.model.util.SubGoalsUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.dao.GoalDoDAO
 * 
 * Note: Because of the 3/4 parameters of some methods and the resulting test 
 * 	 combinations for the tests 2 values for the 1st and 2nd parameter are 
 * 	 used and 2 for 3rd and 4th parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       milestoneId: -1L, 10L 
 *       EntityManager: null, EntityManager
 *       
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.dao.GoalDoDAO")
class GoalDoDAOGetTest
{
    @Mock 
    private EntityManager em;
    private static GoalDoDAO goalDoDAOTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.GoalDoDAO, get methods");
        goalDoDAOTest = mock(GoalDoDAO.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.GoalDoDAO, get methods completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getInstance()}.
     */
    @Test
    @DisplayName("GoalDoDAO = getInstance()")
    final void testGetInstance()
    {
	assertNotNull(GoalDoDAO.getInstance());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllGoals(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<GoalDO> = getAllGoals(null)")
    final void testGetAllGoalsNull()
    {
	List<GoalDO> goalsList = new ArrayList<>();
	
	when(goalDoDAOTest.getAllGoals(null)).thenReturn(goalsList);
	
	assertNotNull(goalsList);
	assertTrue(goalsList.isEmpty());
    }
       
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllGoals(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<GoalDO> = getAllGoals(EntityManager)")
    final void testGetAllGoalsEntityManager()
    {
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	goal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	goal.setEndDate(LocalDateTime.parse("2019-02-01T08:00"));
	goal.setCategory(Category.HOBBY);
	goal.setDescription("Make a car from wood.");
	goal.setShortTerm("Woodcar");
	goal.setNotifications(true);
	goal.setStatus(Status.DEFINED);
	goal.setSubGoals(new ArrayList<SubGoalDO>());

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal null test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().addAll(goalsList);
		
	when(goalDoDAOTest.getAllGoals(em)).thenReturn(goalsList);
	
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), goalsTO.getResults().get(0).getBeginDate());
	assertEquals(LocalDateTime.parse("2019-02-01T08:00"), goalsTO.getResults().get(0).getEndDate());
	assertEquals(Category.HOBBY, goalsTO.getResults().get(0).getCategory());
	assertEquals("Make a car from wood.", goalsTO.getResults().get(0).getDescription());
	assertEquals("Woodcar", goalsTO.getResults().get(0).getShortTerm());
	assertEquals(true, goalsTO.getResults().get(0).getNotifications());
	assertEquals(Status.DEFINED, goalsTO.getResults().get(0).getStatus());
	assertTrue(goalsTO.getResults().get(0).getSubGoals().isEmpty());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllGoalsByStatus(org.gmt.persistence.model.Status, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<GoalDO> = getAllGoalsByStatus(null, null)")
    final void testGetAllGoalsByStatusNullNull()
    {
	List<GoalDO> goalsList = new ArrayList<>();
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get goals by status test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
		
	when(goalDoDAOTest.getAllGoalsByStatus(null, null)).thenReturn(null);
	
	assertTrue(goalsList.isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllGoalsByStatus(org.gmt.persistence.model.Status, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<GoalDO> = getAllGoalsByStatus(null, EntityManager)")
    final void testGetAllGoalsByStatusNullEntityManager()
    {
	List<GoalDO> goalsList = new ArrayList<>();
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get goals by status test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
		
	when(goalDoDAOTest.getAllGoalsByStatus(null, em)).thenReturn(null);
	
	assertTrue(goalsList.isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllGoalsByStatus(org.gmt.persistence.model.Status, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<GoalDO> = getAllGoalsByStatus(Status.DEFINED, null)")
    final void testGetAllGoalsByStatusDefNull()
    {
	List<GoalDO> goalsList = new ArrayList<>();
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get goals by status test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
		
	when(goalDoDAOTest.getAllGoalsByStatus(Status.DEFINED, null)).thenReturn(null);
	
	assertTrue(goalsList.isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllGoalsByStatus(org.gmt.persistence.model.Status, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<GoalDO> = getAllGoalsByStatus(Status.DEFINED, EntityManager)")
    final void testGetAllGoalsByStatusDefEntityManager()
    {
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	goal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	goal.setEndDate(LocalDateTime.parse("2019-02-01T08:00"));
	goal.setCategory(Category.HOBBY);
	goal.setDescription("Make a car from wood.");
	goal.setShortTerm("Woodcar");
	goal.setNotifications(true);
	goal.setStatus(Status.DEFINED);
	goal.setSubGoals(new ArrayList<SubGoalDO>());

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get goals by status test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().addAll(goalsList);
		
	when(goalDoDAOTest.getAllGoalsByStatus(Status.DEFINED, em)).thenReturn(goalsList);
	
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), goalsTO.getResults().get(0).getBeginDate());
	assertEquals(LocalDateTime.parse("2019-02-01T08:00"), goalsTO.getResults().get(0).getEndDate());
	assertEquals(Category.HOBBY, goalsTO.getResults().get(0).getCategory());
	assertEquals("Make a car from wood.", goalsTO.getResults().get(0).getDescription());
	assertEquals("Woodcar", goalsTO.getResults().get(0).getShortTerm());
	assertEquals(true, goalsTO.getResults().get(0).getNotifications());
	assertEquals(Status.DEFINED, goalsTO.getResults().get(0).getStatus());
	assertTrue(goalsTO.getResults().get(0).getSubGoals().isEmpty());
    }    

    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getGoalById(null, null)")
    final void testGetGoalByIdNullNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getGoalById(null, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getGoalById(null, EntityManager)")
    final void testGetGoalByIdNullEntityManager()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getGoalById(null, em)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = getGoalById(-1L, null)")
    final void testGetGoalByIdMinusOneNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getGoalById(-1L, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = getGoalById(-1L, EntityManager)")
    final void testGetGoalByIdMinusOneEntityManager()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getGoalById(-1L, em)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getGoalById(0L, null)")
    final void testGetGoalByIdZeroNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getGoalById(0L, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = getGoalById(0L, EntityManager)")
    final void testGetGoalByIdZeroEntityManager()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getGoalById(0L, em)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = getGoalById(1L, null)")
    final void testGetGoalByIdOneNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 1L, null test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getGoalById(1L, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = getGoalById(1L, EntityManager)")
    final void testGetGoalByIdOneEntityManager()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 1L, EntityManager test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalDoDAOTest.getGoalById(1L, em)).thenReturn(goal);

	// Then
	assertEquals(goalDoDAOTest.getGoalById(1L, em), goal);
	assertEquals("Get a goal by id 1L, EntityManager test", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getGoalById(10L, null)")
    final void testGetGoalByIdTenNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getGoalById(10L, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = getGoalById(10L, EntityManager)")
    final void testGetGoalByIdTenEntityManager()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 10L, EntityManager test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalDoDAOTest.getGoalById(10L, em)).thenReturn(goal);

	// Then
	assertEquals(goalDoDAOTest.getGoalById(10L, em), goal);
	assertEquals("Get a goal by id 10L, EntityManager test", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());
	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllSubGoals(null, null)")
    final void testGetAllSubGoalsNullNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id null, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllSubGoals(null, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllSubGoals(null, EntityManager)")
    final void testGetAllSubGoalsNullEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id null, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllSubGoals(null, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllSubGoals(-1L, null)")
    final void testGetAllSubGoalsMinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id -1L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllSubGoals(-1L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllSubGoals(-1L, EntityManager)")
    final void testGetAllSubGoalsMinusOneEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id -1L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllSubGoals(-1L, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllSubGoals(0L, null)")
    final void testGetAllSubGoalsZeroNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id 0L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllSubGoals(0L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllSubGoals(0L, EntityManager)")
    final void testGetAllSubGoalsZeroEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id 0L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllSubGoals(0L, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllSubGoals(1L, null)")
    final void testGetAllSubGoalsOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id 1L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllSubGoals(1L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<SubGoalDO> = getAllSubGoals(1L, EntityManager)")
    final void testGetAllSubGoalsOneEntityManager()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id 1L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_OK);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(goalDoDAOTest.getAllSubGoals(1L, em)).thenReturn(subGoalsList);

	// Then
	assertEquals(goalDoDAOTest.getAllSubGoals(1L, em), subGoalsList);
	assertEquals("Get all subgoals id 1L, EntityManager test", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_OK, subGoalsTO.getReturncode());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllSubGoals(10L, null)")
    final void testGetAllSubGoalsTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id 10L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllSubGoals(10L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<SubGoalDO> = getAllSubGoals(10L, EntityManager)")
    final void testGetAllSubGoalsTenEntityManager()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals id 10L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_OK);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(goalDoDAOTest.getAllSubGoals(10L, em)).thenReturn(subGoalsList);

	// Then
	assertEquals(goalDoDAOTest.getAllSubGoals(10L, em), subGoalsList);
	assertEquals("Get all subgoals id 10L, EntityManager test", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_OK, subGoalsTO.getReturncode());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }      

    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(null, null, null)")
    final void testGetAllMilestones3Null()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id null, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(null, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(null, null, EntityManager)")
    final void testGetAllMilestonesNullNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id null, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(null, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(-1L, -1L, null)")
    final void testGetAllMilestonesMinusOneMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id null, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(-1L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(-1L, -1L, EntityManager)")
    final void testGetAllMilestonesMinusOneMinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id null, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(-1L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(-1L, 10L, null)")
    final void testGetAllMilestonesMinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id null, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(-1L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(-1L, 10L, EntityManager)")
    final void testGetAllMilestonesMinusOneTenEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id null, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(-1L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(10L, -1L, null)")
    final void testGetAllMilestonesTenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(10L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(10L, -1L, EntityManager)")
    final void testGetAllMilestonesTenMinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(10L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(10L, 10L, null)")
    final void testGetAllMilestonesTenTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getAllMilestones(10L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getAllMilestones(10L, 10L, EntityManager)")
    final void testGetAllMilestonesTenTenEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(10L);

	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_OK);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(goalDoDAOTest.getAllMilestones(10L, 10L, em)).thenReturn(milestonesList);

	// Then
	assertEquals(goalDoDAOTest.getAllMilestones(10L, 10L, em), milestonesList);
	assertEquals("Get all milestones id 10L, 10L, EntityManager test", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_OK, milestonesTO.getReturncode());
	assertEquals(new Long(10L), milestonesTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getSubGoalById(-1L, -1L, null)")
    final void testGetSubGoalById2MinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id -1L, -1L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getSubGoalById(-1L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getSubGoalById(-1L, -1L, EntityManager)")
    final void testGetSubGoalById2MinusOneEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id -1L, -1L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getSubGoalById(-1L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getSubGoalById(-1L, 10L, null)")
    final void testGetSubGoalByIdMinusOneTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id -1L, 10L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getSubGoalById(-1L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getSubGoalById(-1L, 10L, EntityManager)")
    final void testGetSubGoalByIdMinusOneTenEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id -1L, 10L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getSubGoalById(-1L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getSubGoalById(10L, -1L, null)")
    final void testGetSubGoalByIdTenMinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, -1L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getSubGoalById(10L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getSubGoalById(10L, -1L, EntityManager)")
    final void testGetSubGoalByIdTenMinusOneEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, -1L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getSubGoalById(10L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getSubGoalById(10L, 10L, null)")
    final void testGetSubGoalByIdTenTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, 10L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getSubGoalById(10L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("SubGoalDO = getSubGoalById(10L, 10L, EntityManager)")
    final void testGetSubGoalByIdTenTenEntityManager()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, 10L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_OK);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(goalDoDAOTest.getSubGoalById(10L, 10L, em)).thenReturn(subGoal);

	// Then
	assertEquals(goalDoDAOTest.getSubGoalById(10L, 10L, em), subGoal);
	assertEquals("Get a subgoal by id 10L, 10L, EntityManager test", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_OK, subGoalsTO.getReturncode());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }
   
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(-1L, -1L, -1L, null)")
    final void testGetMilestoneById3MinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, -1L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(-1L, -1L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(-1L, -1L, -1L, EntityManager)")
    final void testGetMilestoneById3MinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, -1L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(-1L, -1L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(-1L, -1L, 10L, null)")
    final void testGetMilestoneById2MinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, -1L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(-1L, -1L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(-1L, -1L, 10L, EntityManager)")
    final void testGetMilestoneById2MinusOneTenEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, -1L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(-1L, -1L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(-1L, 10L, -1L, null)")
    final void testGetMilestoneByIdMinusOneTenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, 10L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(-1L, 10L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(-1L, 10L, -1L, EntityManager)")
    final void testGetMilestoneByIdMinusOneTenMinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, 10L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(-1L, 10L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(-1L, 10L, 10L, null)")
    final void testGetMilestoneByIdMinusOneTenTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, 10L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(-1L, 10L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(-1L, 10L, 10L, EntityManager)")
    final void testGetMilestoneByIdMinusOneTenTenEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, 10L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(-1L, 10L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(10L, -1L, -1L, null)")
    final void testGetMilestoneByIdTen2MinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(10L, -1L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(10L, -1L, -1L, EntityManager)")
    final void testGetMilestoneByIdTen2MinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(10L, -1L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(10L, -1L, 10L, null)")
    final void testGetMilestoneByIdTenMinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(10L, -1L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(10L, -1L, 10L, EntityManager)")
    final void testGetMilestoneByIdTenMinusOneTenEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(10L, -1L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(10L, 10L, -1L, null)")
    final void testGetMilestoneById2TenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(10L, 10L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(10L, 10L, -1L, EntityManager)")
    final void testGetMilestoneById2TenMinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(10L, 10L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getMilestoneById(10L, 10L, 10L, null)")
    final void testGetMilestoneById3TenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(10L, 10L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("MilestoneDO = getMilestoneById(10L, 10L, 10L, EntityManager)")
    final void testGetMilestoneById3TenEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.getMilestoneById(10L, 10L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
}
