/**
 * 
 */
package org.gmt.persistence.model.dao;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.dao.GoalDoDAO;
import org.gmt.persistence.model.util.GoalsTO;
import org.gmt.persistence.model.util.GoalsUtil;
import org.gmt.persistence.model.util.MilestonesTO;
import org.gmt.persistence.model.util.MilestonesUtil;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.gmt.persistence.model.util.SubGoalsUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.dao.GoalDoDAO, 
 * delete methods
 * 
 * Note: Because of the 3/4 parameters of some methods and the resulting test 
 * 	 combinations for the tests 2 values for the 1st and 2nd parameter are 
 * 	 used and 2 for 3rd and 4th parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       milestoneId: -1L, 10L 
 *       EntityManager: null, EntityManager
 *       
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.dao.GoalDoDAO, delete methods")
class GoalDoDAODeleteTest
{
    @Mock 
    private EntityManager em;
    private static GoalDoDAO goalDoDAOTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.GoalDoDAO, delete methods");
        goalDoDAOTest = mock(GoalDoDAO.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.GoalDoDAO, delete methods completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteGoalById(null, null)")
    final void testDeleteGoalByIdNullNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteGoalById(null, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteGoalById(null, EntityManager)")
    final void testDeleteGoalByIdNullEntityManager()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteGoalById(null, em)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = deleteGoalById(-1L, null)")
    final void testDeleteGoalByIdMinusOneNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteGoalById(-1L, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = deleteGoalById(-1L, EntityManager)")
    final void testDeleteGoalByIdMinusOneEntityManager()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteGoalById(-1L, em)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteGoalById(0L, null)")
    final void testDeleteGoalByIdZeroNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteGoalById(0L, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = deleteGoalById(0L, EntityManager)")
    final void testDeleteGoalByIdZeroEntityManager()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteGoalById(0L, em)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = deleteGoalById(1L, null)")
    final void testDeleteGoalByIdOneNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 1L, null test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteGoalById(1L, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = deleteGoalById(1L, EntityManager)")
    final void testDeleteGoalByIdOneEntityManager()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 1L, EntityManager test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalDoDAOTest.deleteGoalById(1L, em)).thenReturn(goal);

	// Then
	assertEquals(goalDoDAOTest.deleteGoalById(1L, em), goal);
	assertEquals("Get a goal by id 1L, EntityManager test", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteGoalById(10L, null)")
    final void testDeleteGoalByIdTenNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteGoalById(10L, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteGoalById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = deleteGoalById(10L, EntityManager)")
    final void testDeleteGoalByIdTenEntityManager()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 10L, EntityManager test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalDoDAOTest.deleteGoalById(10L, em)).thenReturn(goal);

	// Then
	assertEquals(goalDoDAOTest.deleteGoalById(10L, em), goal);
	assertEquals("Get a goal by id 10L, EntityManager test", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());
	
    }

    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteSubGoalById(-1L, -1L, null)")
    final void testDeleteSubGoalById2MinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id -1L, -1L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteSubGoalById(-1L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteSubGoalById(-1L, -1L, EntityManager)")
    final void testDeleteSubGoalById2MinusOneEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id -1L, -1L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteSubGoalById(-1L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteSubGoalById(-1L, 10L, null)")
    final void testDeleteSubGoalByIdMinusOneTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id -1L, 10L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteSubGoalById(-1L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteSubGoalById(-1L, 10L, EntityManager)")
    final void testDeleteSubGoalByIdMinusOneTenEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id -1L, 10L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteSubGoalById(-1L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteSubGoalById(10L, -1L, null)")
    final void testDeleteSubGoalByIdTenMinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, -1L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteSubGoalById(10L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteSubGoalById(10L, -1L, EntityManager)")
    final void testDeleteSubGoalByIdTenMinusOneEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, -1L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteSubGoalById(10L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteSubGoalById(10L, 10L, null)")
    final void testDeleteSubGoalByIdTenTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, 10L, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteSubGoalById(10L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteSubGoalById(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("SubGoalDO = deleteSubGoalById(10L, 10L, EntityManager)")
    final void testDeleteSubGoalByIdTenTenEntityManager()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, 10L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_OK);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(goalDoDAOTest.deleteSubGoalById(10L, 10L, em)).thenReturn(subGoal);

	// Then
	assertEquals(goalDoDAOTest.deleteSubGoalById(10L, 10L, em), subGoal);
	assertEquals("Get a subgoal by id 10L, 10L, EntityManager test", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_OK, subGoalsTO.getReturncode());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(-1L, -1L, -1L, null)")
    final void testDeleteMilestoneById3MinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id -1L, -1L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(-1L, -1L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(-1L, -1L, -1L, EntityManager)")
    final void testDeleteMilestoneById3MinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id -1L, -1L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(-1L, -1L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(-1L, -1L, 10L, null)")
    final void testDeleteMilestoneById2MinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id -1L, -1L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(-1L, -1L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(-1L, -1L, 10L, EntityManager)")
    final void testDeleteMilestoneById2MinusOneTenEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id -1L, -1L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(-1L, -1L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(-1L, 10L, -1L, null)")
    final void testDeleteMilestoneByIdMinusOneTenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id -1L, 10L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(-1L, 10L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(-1L, 10L, -1L, EntityManager)")
    final void testDeleteMilestoneByIdMinusOneTenMinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id -1L, 10L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(-1L, 10L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(-1L, 10L, 10L, null)")
    final void testDeleteMilestoneByIdMinusOneTenTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id -1L, 10L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(-1L, 10L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(-1L, 10L, 10L, EntityManager)")
    final void testDeleteMilestoneByIdMinusOneTenTenEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id -1L, 10L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(-1L, 10L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(10L, -1L, -1L, null)")
    final void testDeleteMilestoneByIdTen2MinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id 10L, -1L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(10L, -1L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(10L, -1L, -1L, EntityManager)")
    final void testDeleteMilestoneByIdTen2MinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id 10L, -1L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(10L, -1L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(10L, -1L, 10L, null)")
    final void testDeleteMilestoneByIdTenMinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id 10L, -1L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(10L, -1L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(10L, -1L, 10L, EntityManager)")
    final void testDeleteMilestoneByIdTenMinusOneTenEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id 10L, -1L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(10L, -1L, 10L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(10L, 10L, -1L, null)")
    final void testDeleteMilestoneById2TenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id 10L, 10L, -1L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(10L, 10L, -1L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(10L, 10L, -1L, EntityManager)")
    final void testDeleteMilestoneById2TenMinusOneEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id 10L, 10L, -1L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(10L, 10L, -1L, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteMilestoneById(10L, 10L, 10L, null)")
    final void testDeleteMilestoneById3TenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id 10L, 10L, 10L, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.deleteMilestoneById(10L, 10L, 10L, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("MilestoneDO = deleteMilestoneById(10L, 10L, 10L, EntityManager)")
    final void testDeleteMilestoneById3TenEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(10L);

	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone by id 10L, 10L, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_OK);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(goalDoDAOTest.deleteMilestoneById(10L, 10L, 10L, em)).thenReturn(milestone);

	// Then
	assertEquals(goalDoDAOTest.deleteMilestoneById(10L, 10L, 10L, em), milestone);
	assertEquals("Delete a milestone by id 10L, 10L, EntityManager test", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_OK, milestonesTO.getReturncode());
	assertEquals(new Long(10L), milestonesTO.getResults().get(0).getId());
    }
}
