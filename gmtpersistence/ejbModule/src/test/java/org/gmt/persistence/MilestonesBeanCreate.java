/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.MilestonesBean;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.MilestonesBean
 * Method: createMilestone()
 * 
 * Note: Because of the 3 method parameters and the resulting test combinations 
 *       for the tests 2 values for the first and second parameter are used and 5 
 *       for third parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       milestone: null, MilestoneDO
 * 
 * RF: -for all tests with Long parameters we could write some Integer param tests
 *  
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.persistence.MilestonesBean, "
	+ "method createMilestone()")
class MilestonesBeanCreate
{
    private static MilestonesBean milestonesBeanCreateTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.MilestonesBean, "
        	+ "method createMilestone()");
        milestonesBeanCreateTest = mock(MilestonesBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.MilestonesBean, "
        	+ "method createMilestone() completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(-1L, -1L, null)")
    final void testCreateMilestoneMinusOneMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone -1L, -1L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanCreateTest.createMilestone(-1L, -1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanCreateTest.createMilestone(-1L, -1L, null), milestonesTO);
	assertEquals("Create a milestone -1L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(-1L, -1L, MilestoneDO)")
    final void testCreateMilestoneMinusOneMinusOneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone -1L, -1L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanCreateTest.createMilestone(-1L, -1L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanCreateTest.createMilestone(-1L, -1L, milestone), milestonesTO);
	assertEquals("Create a milestone -1L, -1L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(-1L, 10L, null)")
    final void testCreateMilestoneMinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone -1L, 10L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanCreateTest.createMilestone(-1L, 10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanCreateTest.createMilestone(-1L, 10L, null), milestonesTO);
	assertEquals("Create a milestone -1L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(-1L, 10L, MilestoneDO)")
    final void testCreateMilestoneMinusOneMinusTenMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone -1L, 10L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanCreateTest.createMilestone(-1L, 10L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanCreateTest.createMilestone(-1L, 10L, milestone), milestonesTO);
	assertEquals("Create a milestone -1L, 10L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(10L, -1L, null)")
    final void testCreateMilestoneTenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone 10L, -1L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanCreateTest.createMilestone(10L, -1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanCreateTest.createMilestone(10L, -1L, null), milestonesTO);
	assertEquals("Create a milestone 10L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(10L, -1L, MilestoneDO)")
    final void testCreateMilestoneTenMinusOneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone 10L, -1L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanCreateTest.createMilestone(10L, -1L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanCreateTest.createMilestone(10L, -1L, milestone), milestonesTO);
	assertEquals("Create a milestone 10L, -1L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(10L, 10L, null)")
    final void testCreateMilestoneTenTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone 10L, 10L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanCreateTest.createMilestone(10L, 10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanCreateTest.createMilestone(10L, 10L, null), milestonesTO);
	assertEquals("Create a milestone 10L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(10L, 10L, MilestoneDO)")
    final void testCreateMilestoneTenTenMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);
	milestone.setDescription("The plan is ready.");
	milestone.setNotifications(true);
	milestone.setName("MS1");
	milestone.setTodosFinished(new ArrayList<String>());
	
	
	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone 10L, 10L, MilestoneDO test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanCreateTest.createMilestone(10L, 10L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanCreateTest.createMilestone(10L, 10L, milestone), milestonesTO);
	assertEquals("Create a milestone 10L, 10L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	
	assertEquals(new Long(1L), milestonesTO.getResults().get(0).getId());
	assertEquals("The plan is ready.", milestonesTO.getResults().get(0).getDescription());
	assertEquals(true, milestonesTO.getResults().get(0).getNotifications());
	assertEquals("MS1", milestonesTO.getResults().get(0).getName());
	assertTrue(milestonesTO.getResults().get(0).getTodosFinished().isEmpty());
    }
}
