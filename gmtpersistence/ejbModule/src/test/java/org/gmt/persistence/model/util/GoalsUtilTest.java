/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.GoalsTO;
import org.gmt.persistence.model.util.GoalsUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.GoalsUtil
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.GoalsUtil")
class GoalsUtilTest
{
    @Mock
    private EntityManager em;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.GoalsUtil");
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.GoalsUtil completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsUtil#getAllGoals(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error CategoriesTO = getAllGoals(null)")
    final void testGetAllGoalsNull()
    {
	GoalsTO goalsTO = GoalsUtil.getAllGoals(null);
	
	assertEquals("Parameter error", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_ERROR, goalsTO.getReturncode());	
	assertTrue(goalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsUtil#getAllGoalsByStatus(org.gmt.persistence.model.Status, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("CategoriesTO = getAllGoals(EntityManager)")
    final void testGetAllGoalsEntityManager()
    {	
	GoalDO goal = new GoalDO();
	goal.setCategory(Category.FAMILY);
	goal.setId(10L);
	
	GoalsTO goalsTO = GoalsUtil.getAllGoals(em);
	goalsTO.setMessage("This is a test!");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().add(goal);
	
	assertNotNull(goalsTO);
	assertEquals("This is a test!", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	assertEquals(Category.FAMILY, goalsTO.getResults().get(0).getCategory());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsUtil#getAllGoalsByStatus(org.gmt.persistence.model.Status, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error CategoriesTO = getAllGoalsByStatus(null, null)")
    final void testGetAllGoalsByStatusNullNull()
    {
	GoalsTO goalsTO = GoalsUtil.getAllGoalsByStatus(null, null);
	
	assertEquals("Parameter error", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_ERROR, goalsTO.getReturncode());	
	assertTrue(goalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsUtil#getAllGoalsByStatus(org.gmt.persistence.model.Status, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error CategoriesTO = getAllGoalsByStatus(Status.DEFINED, null)")
    final void testGetAllGoalsByStatusDefNull()
    {
	GoalsTO goalsTO = GoalsUtil.getAllGoalsByStatus(Status.DEFINED, null);
	
	assertEquals("Parameter error", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_ERROR, goalsTO.getReturncode());	
	assertTrue(goalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsUtil#getAllGoalsByStatus(org.gmt.persistence.model.Status, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("CategoriesTO = getAllGoalsByStatus(Status.DEFINED, EntityManager)")
    final void testGetAllGoalsByStatusDefEntityManager()
    {	
	GoalDO goal = new GoalDO();
	goal.setCategory(Category.FAMILY);
	goal.setId(10L);
	
	GoalsTO goalsTO = GoalsUtil.getAllGoalsByStatus(Status.DEFINED, em);
	goalsTO.setMessage("This is a test!");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().add(goal);
	
	assertNotNull(goalsTO);
	assertEquals("This is a test!", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	assertEquals(Category.FAMILY, goalsTO.getResults().get(0).getCategory());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsUtil#createGoalsTO(java.lang.String, org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("error GoalsTO = createGoalsTO(message, null)")
    final void testCreateGoalsTONull()
    {	
	GoalsTO goalsTO = GoalsUtil.createGoalsTO("", null);
	
	assertEquals("Parameter error", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_ERROR, goalsTO.getReturncode());	
	assertTrue(goalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsUtil#createGoalsTO(java.lang.String, org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("GoalsTO = createGoalsTO(message, GoalDO)")
    final void testCreateGoalsTO()
    {
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	goal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	goal.setEndDate(LocalDateTime.parse("2019-02-01T08:00"));
	goal.setCategory(Category.HOBBY);
	goal.setDescription("Make a car from wood.");
	goal.setShortTerm("Woodcar");
	goal.setNotifications(true);
	goal.setStatus(Status.DEFINED);
	goal.setSubGoals(new ArrayList<SubGoalDO>());
	
	GoalsTO goalsTO = GoalsUtil.createGoalsTO("Create a GoalDO test", goal);
	
	assertEquals("Create a GoalDO test", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), goalsTO.getResults().get(0).getBeginDate());
	assertEquals(LocalDateTime.parse("2019-02-01T08:00"), goalsTO.getResults().get(0).getEndDate());
	assertEquals(Category.HOBBY, goalsTO.getResults().get(0).getCategory());
	assertEquals("Make a car from wood.", goalsTO.getResults().get(0).getDescription());
	assertEquals("Woodcar", goalsTO.getResults().get(0).getShortTerm());
	assertEquals(true, goalsTO.getResults().get(0).getNotifications());
	assertEquals(Status.DEFINED, goalsTO.getResults().get(0).getStatus());
	assertTrue(goalsTO.getResults().get(0).getSubGoals().isEmpty());	
    }
}
