/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.GoalsBean;
import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.GoalsTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.GoalsBean
 * 
 * RF: -for all tests with Long parameters we could write some Integer param tests 
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.persistence.GoalsBean")
class GoalsBeanTest
{
    private static GoalsBean goalsBeanTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.GoalsBean");
        goalsBeanTest = mock(GoalsBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.GoalsBean completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#GoalsBean()}.
     */
    @Test
    @DisplayName("not null = getClass()")
    final void testGoalsBean()
    {
	assertNotNull(goalsBeanTest.getClass());
    }

    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#getAllGoals()}.
     */
    @Test
    @DisplayName("empty GoalsTO = getAllGoals()")
    final void testGetAllGoalsRetEmptyGoalsTO()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();

	// When
	when(goalsBeanTest.getAllGoals()).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getAllGoals(), goalsTO);
	assertNull(goalsTO.getMessage());
	assertNull(goalsTO.getReturncode());
	assertEquals(goalsList, goalsTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#getAllGoals()}.
     */
    @Test
    @DisplayName("GoalsTO = getAllGoals()")
    final void testGetAllGoalsRetGoalsTO()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get all goals test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);
	
	// When
	when(goalsBeanTest.getAllGoals()).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getAllGoals(), goalsTO);
	assertEquals("Get all goals test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(goalsList, goalsTO.getResults());
    }    

    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#getAllGoalsByStatus(org.gmt.persistence.model.Status)}.
     */
    @Test
    @DisplayName("empty GoalsTO = getAllGoalsByStatus(null)")
    final void testGetAllGoalsByStatusNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get all goals with status null test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsBeanTest.getAllGoalsByStatus(null)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getAllGoalsByStatus(null), goalsTO);
	assertEquals("Get all goals with status null test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());    
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#getAllGoalsByStatus(org.gmt.persistence.model.Status)}.
     */
    @Test
    @DisplayName("GoalsTO = getAllGoalsByStatus(\"DEFINED\")")
    final void testGetAllGoalsByStatus()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get all goals with status=DEFINED test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsBeanTest.getAllGoalsByStatus(Status.DEFINED)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getAllGoalsByStatus(Status.DEFINED), goalsTO);
	assertEquals("Get all goals with status=DEFINED test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(goalsList, goalsTO.getResults());    
    }

    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#getGoalById(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty GoalsTO = getGoalById(-1L)")
    final void testGetGoalByIdMinusOne()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by negative id test");
	goalsTO.setReturncode(-1);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsBeanTest.getGoalById(-1L)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getGoalById(-1L), goalsTO);
	assertEquals("Get a goal by negative id test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#getGoalById(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty GoalsTO = getGoalById(0L)")
    final void testGetGoalByIdZero()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 0 test");
	goalsTO.setReturncode(-1);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsBeanTest.getGoalById(0L)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getGoalById(0L), goalsTO);
	assertEquals("Get a goal by id 0 test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#getGoalById(java.lang.Long)}.
     */
    @Test
    @DisplayName("GoalsTO = getGoalById(1L)")
    final void testGetGoalByIdOne()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 1 test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsBeanTest.getGoalById(1L)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getGoalById(1L), goalsTO);
	assertEquals("Get a goal by id 1 test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#getGoalById(java.lang.Long)}.
     */
    @Test
    @DisplayName("GoalsTO = getGoalById(10L)")
    final void testGetGoalByIdTen()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 10 test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsBeanTest.getGoalById(10L)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getGoalById(10L), goalsTO);
	assertEquals("Get a goal by id 10 test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#createGoal(org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("empty GoalsTO = createGoal(null)")
    final void testCreateGoalNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Create a goal null test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsBeanTest.createGoal(null)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.createGoal(null), goalsTO);
	assertEquals("Create a goal null test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#createGoal(org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("GoalsTO = createGoal(GoalDO)")
    final void testCreateGoalGoalDO()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	goal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	goal.setEndDate(LocalDateTime.parse("2019-02-01T08:00"));
	goal.setCategory(Category.HOBBY);
	goal.setDescription("Make a car from wood.");
	goal.setShortTerm("Woodcar");
	goal.setNotifications(true);
	goal.setStatus(Status.DEFINED);
	goal.setSubGoals(new ArrayList<SubGoalDO>());
	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Create a goal null test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);


	// When
	when(goalsBeanTest.createGoal(goal)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.createGoal(goal), goalsTO);
	assertEquals("Create a goal null test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), goalsTO.getResults().get(0).getBeginDate());
	assertEquals(LocalDateTime.parse("2019-02-01T08:00"), goalsTO.getResults().get(0).getEndDate());
	assertEquals(Category.HOBBY, goalsTO.getResults().get(0).getCategory());
	assertEquals("Make a car from wood.", goalsTO.getResults().get(0).getDescription());
	assertEquals("Woodcar", goalsTO.getResults().get(0).getShortTerm());
	assertEquals(true, goalsTO.getResults().get(0).getNotifications());
	assertEquals(Status.DEFINED, goalsTO.getResults().get(0).getStatus());
	assertTrue(goalsTO.getResults().get(0).getSubGoals().isEmpty());
    }

    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#updateGoal(org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("empty GoalsTO = updateGoal(null)")
    final void testUpdateGoalNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Update a goal null test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsBeanTest.updateGoal(null)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.updateGoal(null), goalsTO);
	assertEquals("Update a goal null test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#updateGoal(org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("GoalsTO = updateGoal(GoalDO)")
    final void testUpdateGoalGoalDO()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	goal.setDescription("Make a better car from wood.");
	goal.setStatus(Status.RUNNING);

	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Update a goal null test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);


	// When
	when(goalsBeanTest.updateGoal(goal)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.updateGoal(goal), goalsTO);
	assertEquals("Update a goal null test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
	assertEquals("Make a better car from wood.", goalsTO.getResults().get(0).getDescription());
	assertEquals(Status.RUNNING, goalsTO.getResults().get(0).getStatus());
    }

    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#deleteGoalById(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty GoalsTO = deleteGoalById(-1L)")
    final void testDeleteGoalByIdMinusOne()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Delete a goal id -1 test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsBeanTest.deleteGoalById(-1L)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.deleteGoalById(-1L), goalsTO);
	assertEquals("Delete a goal id -1 test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#deleteGoalById(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty GoalsTO = deleteGoalById(0L)")
    final void testDeleteGoalByIdZero()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Delete a goal id 0 test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsBeanTest.deleteGoalById(0L)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.deleteGoalById(0L), goalsTO);
	assertEquals("Delete a goal id 0 test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#deleteGoalById(java.lang.Long)}.
     */
    @Test
    @DisplayName("GoalsTO = deleteGoalById(1L)")
    final void testDeleteGoalByIdOne()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 1 test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsBeanTest.getGoalById(1L)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.getGoalById(1L), goalsTO);
	assertEquals("Get a goal by id 1 test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.GoalsBean#deleteGoalById(java.lang.Long)}.
     */
    @Test
    @DisplayName("GoalsTO = deleteGoalById(10L)")
    final void testDeleteGoalByIdTen()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 10 test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsBeanTest.deleteGoalById(10L)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsBeanTest.deleteGoalById(10L), goalsTO);
	assertEquals("Get a goal by id 10 test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());
    }
}
