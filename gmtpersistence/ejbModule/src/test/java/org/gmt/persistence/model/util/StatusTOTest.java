/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.StatusDO;
import org.gmt.persistence.model.util.StatusTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.StatusTO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.StatusTO")
class StatusTOTest
{
    private static StatusTO statusToTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.StatusTO");
        statusToTest = new StatusTO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.StatusTO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getMessage()}.
     */
    @Test
    @DisplayName("String = getMessage()/setMessage()")
    final void testGetSetMessage()
    {
        statusToTest.setMessage("This is a test!");
	assertEquals("This is a test!", statusToTest.getMessage());
	
	statusToTest.setMessage("");
	assertEquals("", statusToTest.getMessage());
	
	statusToTest.setMessage("ÄöÜ");
	assertEquals("ÄöÜ", statusToTest.getMessage());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getReturncode()}.
     */
    @Test
    @DisplayName("Integer = getReturncode()/setReturncode()")
    final void testGetSetReturncode()
    {
        statusToTest.setReturncode(-1);
        assertEquals(-1, statusToTest.getReturncode().intValue());
        
        statusToTest.setReturncode(0);
        assertEquals(0, statusToTest.getReturncode().intValue());
        
        statusToTest.setReturncode(1);
        assertEquals(1, statusToTest.getReturncode().intValue());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getResults()}.
     */
    @Test
    @DisplayName("List<GoalDO> = getResults()")
    final void testGetResults()
    {
	assertTrue(statusToTest.getResults().isEmpty());
	
	StatusDO status = new StatusDO();
	status.setId(1L);
	status.setStatus(Status.RUNNING);
	
	statusToTest.getResults().add(status);
	
	StatusDO resultStatus = statusToTest.getResults().get(0);
	assertEquals(new Long(1L), resultStatus.getId());
	assertEquals(Status.RUNNING, resultStatus.getStatus());
    }
}
