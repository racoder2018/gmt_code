/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.util.UsersTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.UsersTO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.UsersTO")
class UsersTOTest
{
    private static UsersTO usersToTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.UsersTO");
        usersToTest = new UsersTO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.UsersTO completed.");
    }
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getMessage()}.
     */
    @Test
    @DisplayName("String = getMessage()/setMessage()")
    final void testGetSetMessage()
    {
        usersToTest.setMessage("This is a test!");
	assertEquals("This is a test!", usersToTest.getMessage());
	
	usersToTest.setMessage("");
	assertEquals("", usersToTest.getMessage());
	
	usersToTest.setMessage("ÄöÜ");
	assertEquals("ÄöÜ", usersToTest.getMessage());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getReturncode()}.
     */
    @Test
    @DisplayName("Integer = getReturncode()/setReturncode()")
    final void testGetSetReturncode()
    {
        usersToTest.setReturncode(-1);
        assertEquals(-1, usersToTest.getReturncode().intValue());
        
        usersToTest.setReturncode(0);
        assertEquals(0, usersToTest.getReturncode().intValue());
        
        usersToTest.setReturncode(1);
        assertEquals(1, usersToTest.getReturncode().intValue());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getResults()}.
     */
    @Test
    @DisplayName("List<GoalDO> = getResults()")
    final void testGetResults()
    {
	assertTrue(usersToTest.getResults().isEmpty());
	
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Blankenstein");
	user.setLoginName("hblankenstein");
	user.setPassword("abcdef1234");
	
	usersToTest.getResults().add(user);
	
	UserDO resultUser = usersToTest.getResults().get(0);
	assertEquals(new Long(1L), resultUser.getId());
	assertEquals("Herbert", resultUser.getFirstName());
	assertEquals("Blankenstein", resultUser.getLastName());
	assertEquals("hblankenstein", resultUser.getLoginName());
	assertEquals("abcdef1234", resultUser.getPassword());    
    }
}
