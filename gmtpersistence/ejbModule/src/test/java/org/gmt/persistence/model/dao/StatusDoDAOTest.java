/**
 * 
 */
package org.gmt.persistence.model.dao;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.StatusDO;
import org.gmt.persistence.model.dao.StatusDoDAO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.dao.StatusDoDAO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.dao.StatusDoDAO")
class StatusDoDAOTest
{
    @Mock
    private EntityManager em;
    private static StatusDoDAO statusDoDAOTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.StatusDoDAO");
        statusDoDAOTest = mock(StatusDoDAO.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.StatusDoDAO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.StatusDoDAO#getInstance()}.
     */
    @Test
    @DisplayName("StatusDoDAO = getInstance()")
    final void testGetInstance()
    {
	assertNotNull(StatusDoDAO.getInstance());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.dao.StatusDoDAO#getAllStatus(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<StatusDO> = getAllStatus(null)")
    final void testGetAllStatusNull()
    {
	List<StatusDO> statusList = new ArrayList<>();
	
	when(statusDoDAOTest.getAllStatus(null)).thenReturn(statusList);
	
	assertNotNull(statusList);
	assertTrue(statusList.isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.StatusDoDAO#getAllStatus(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<StatusDO> = getAllStatus(EntityManager)")
    final void testGetAllStatusEntityManager()
    {
	StatusDO status = new StatusDO();
	status.setId(1L);
	status.setStatus(Status.DEFINED);
		
	List<StatusDO> statusList = new ArrayList<StatusDO>();
	statusList.add(status);
	
	when(statusDoDAOTest.getAllStatus(em)).thenReturn(statusList);

	assertNotNull(statusList);
	assertFalse(statusList.isEmpty());
	assertEquals(Status.DEFINED, statusList.get(0).getStatus());
	assertEquals(new Long(1L), statusList.get(0).getId());
    }
}
