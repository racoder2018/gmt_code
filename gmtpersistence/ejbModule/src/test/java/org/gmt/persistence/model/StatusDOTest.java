/**
 * 
 */
package org.gmt.persistence.model;

import static org.junit.jupiter.api.Assertions.*;

import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.StatusDO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.StatusDO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.StatusDO")
class StatusDOTest
{
    private static StatusDO statusDOTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.StatusDO");
        statusDOTest = new StatusDO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.StatusDO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.StatusDO#getId()/setId()}.
     */
    @Test
    @DisplayName("Long = getId()/setId()")
    final void testGetSetId()
    {
	statusDOTest.setId(10L);
	assertEquals(new Long(10L), statusDOTest.getId());

	statusDOTest.setId(-1L);
	assertEquals(new Long(-1L), statusDOTest.getId());

	statusDOTest.setId(0L);
	assertEquals(new Long(0L), statusDOTest.getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.StatusDO#getStatus()/setStatus()}.
     */
    @Test
    @DisplayName("Status = getStatus()/setStatus()")
    final void testGetSetStatus()
    {
	statusDOTest.setStatus(Status.ACHIEVED);
	assertEquals(Status.ACHIEVED, statusDOTest.getStatus());
	
	statusDOTest.setStatus(null);
	assertNull(statusDOTest.getStatus());
    }
}
