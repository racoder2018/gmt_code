/**
 * 
 */
package org.gmt.persistence.model;

import static org.junit.jupiter.api.Assertions.*;

import org.gmt.persistence.model.UserDO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.UserDO
 * 
 * RF: 	- It should not be allowed to set a firstname or lastname like "-12 abc" 
 * 	- a string without numbers should be required here: "abcABC" ... 
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.UserDO")
class UserDOTest
{
    private static UserDO userDOTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.UserDO");
        userDOTest = new UserDO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.UserDO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.UserDO#getId()/setId()}.
     */
    @Test
    @DisplayName("Long = getId()/setId()")
    final void testGetSetId()
    {
	userDOTest.setId(10L);
	assertEquals(new Long(10L), userDOTest.getId());

	userDOTest.setId(-1L);
	assertEquals(new Long(-1L), userDOTest.getId());

	userDOTest.setId(0L);
	assertEquals(new Long(0L), userDOTest.getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.UserDO#getFirstName()/setFirstName()}.
     */
    @Test
    @DisplayName("String = getFirstName()/setFirstName()")
    final void testGetSetFirstName()
    {
	userDOTest.setFirstName("Herbert");
	assertEquals("Herbert", userDOTest.getFirstName());

	userDOTest.setFirstName("");
	assertEquals("", userDOTest.getFirstName());
	
	userDOTest.setFirstName(null);
	assertNull(userDOTest.getFirstName());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.UserDO#getLastName()/setLastName()}.
     */
    @Test
    @DisplayName("String = getLastName()/setLastName()")
    final void testGetSetLastName()
    {
	userDOTest.setLastName("Feuerstein");
	assertEquals("Feuerstein", userDOTest.getLastName());

	userDOTest.setLastName("");
	assertEquals("", userDOTest.getLastName());
	
	userDOTest.setLastName(null);
	assertNull(userDOTest.getLastName());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.UserDO#getLoginName()/setLoginName()}.
     */
    @Test
    @DisplayName("String = getLoginName()/setLoginName()")
    final void testGetSetLoginName()
    {
	userDOTest.setLoginName("hfeuerstein");
	assertEquals("hfeuerstein", userDOTest.getLoginName());

	userDOTest.setLoginName("");
	assertEquals("", userDOTest.getLoginName());
	
	userDOTest.setLoginName(null);
	assertNull(userDOTest.getLoginName());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.UserDO#getPassword()/setPassword()}.
     */
    @Test
    @DisplayName("String = getPassword()/setPassword()")
    final void testGetSetPassword()
    {
	userDOTest.setPassword("123abc");
	assertEquals("123abc", userDOTest.getPassword());

	userDOTest.setPassword("");
	assertEquals("", userDOTest.getPassword());
	
	userDOTest.setPassword(null);
	assertNull(userDOTest.getPassword());
    }
}
