/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.gmt.persistence.MilestonesBean;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.MilestonesBean
 * Method: updateMilestoneById()
 * 
 * Note: Because of the 4 method parameters and the resulting test combinations 
 *       for the tests 2 values for the 1st, 2nd and 3rd parameter are used and 
 *       2 for 4th parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       id:	    -1L, 10L
 *       milestone: null, MilestoneDO
 *       
 * RF: -for all tests with Long parameters we could write some Integer param tests
 *  
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.persistence.MilestonesBean, "
	+ "method updateMilestoneById()")
class MilestonesBeanUpdate
{
    private static MilestonesBean milestonesBeanUpdateTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.MilestonesBean, "
        	+ "method updateMilestoneById()");
        milestonesBeanUpdateTest = mock(MilestonesBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.MilestonesBean, "
        	+ "method updateMilestoneById() completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(-1L, -1L, -1L, null)")
    final void testUpdateMilestoneById3MinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1L, -1L, -1L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(-1L, -1L, -1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(-1L, -1L, -1L, null), milestonesTO);
	assertEquals("Update a milestone -1L, -1L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(-1L, -1L, -1L, MilestoneDO)")
    final void testUpdateMilestoneById3MinusOneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1L, -1L, -1L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(-1L, -1L, -1L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(-1L, -1L, -1L, milestone), milestonesTO);
	assertEquals("Update a milestone -1L, -1L, -1L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(-1L, -1L, 10L, null)")
    final void testUpdateMilestoneById2MinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1L, -1L, 10L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(-1L, -1L, 10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(-1L, -1L, 10L, null), milestonesTO);
	assertEquals("Update a milestone -1L, -1L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(-1L, -1L, 10L, MilestoneDO)")
    final void testUpdateMilestoneById2MinusOneTenMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1L, -1L, 10L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(-1L, -1L, 10L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(-1L, -1L, 10L, milestone), milestonesTO);
	assertEquals("Update a milestone -1L, -1L, 10L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(-1L, 10L, -1L, null)")
    final void testUpdateMilestoneByIdMinusOneTenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1L, 10L, -1L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(-1L, 10L, -1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(-1L, 10L, -1L, null), milestonesTO);
	assertEquals("Update a milestone -1L, 10L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(-1L, 10L, -1L, MilestoneDO)")
    final void testUpdateMilestoneByIdMinusOneTenMinusOneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1L, 10L, -1L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(-1L, 10L, -1L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(-1L, 10L, -1L, milestone), milestonesTO);
	assertEquals("Update a milestone -1L, 10L, -1L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(-1L, 10L, 10L, null)")
    final void testUpdateMilestoneByIdMinusOneTenTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1L, 10L, 10L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(-1L, 10L, 10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(-1L, 10L, 10L, null), milestonesTO);
	assertEquals("Update a milestone -1L, 10L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(-1L, 10L, 10L, MilestoneDO)")
    final void testUpdateMilestoneByIdMinusOneTenTenMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1L, 10L, 10L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(-1L, 10L, 10L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(-1L, 10L, 10L, milestone), milestonesTO);
	assertEquals("Update a milestone -1L, 10L, 10L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(10L, -1L, -1L, null)")
    final void testUpdateMilestoneByIdTen2MinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10L, -1L, -1L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(10L, -1L, -1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(10L, -1L, -1L, null), milestonesTO);
	assertEquals("Update a milestone 10L, -1L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(10L, -1L, -1L, MilestoneDO)")
    final void testUpdateMilestoneByIdTen2MinusOneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10L, -1L, -1L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(10L, -1L, -1L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(10L, -1L, -1L, milestone), milestonesTO);
	assertEquals("Update a milestone 10L, -1L, -1L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(10L, -1L, 10L, null)")
    final void testUpdateMilestoneByIdTenMinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10L, -1L, 10L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(10L, -1L, 10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(10L, -1L, 10L, null), milestonesTO);
	assertEquals("Update a milestone 10L, -1L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(10L, -1L, 10L, MilestoneDO)")
    final void testUpdateMilestoneByIdTenMinusOneTenMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10L, -1L, 10L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(10L, -1L, 10L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(10L, -1L, 10L, milestone), milestonesTO);
	assertEquals("Update a milestone 10L, -1L, 10L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(10L, 10L, -1L, null)")
    final void testUpdateMilestoneByIdTenTenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10L, 10L, -1L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(10L, 10L, -1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(10L, 10L, -1L, null), milestonesTO);
	assertEquals("Update a milestone 10L, 10L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(10L, 10L, -1L, MilestoneDO)")
    final void testUpdateMilestoneByIdTenTenMinusOneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10L, 10L, -1L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(10L, 10L, -1L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(10L, 10L, -1L, milestone), milestonesTO);
	assertEquals("Update a milestone 10L, 10L, -1L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(10L, 10L, 10L, null)")
    final void testUpdateMilestoneById3TenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10L, 10L, 10L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(10L, 10L, 10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(10L, 10L, 10L, null), milestonesTO);
	assertEquals("Update a milestone 10L, 10L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#updateMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(10L, 10L, 10L, MilestoneDO)")
    final void testUpdateMilestoneById3TenMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10L, 10L, 10L, MilestoneDO test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanUpdateTest.updateMilestoneById(10L, 10L, 10L, milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanUpdateTest.updateMilestoneById(10L, 10L, 10L, milestone), milestonesTO);
	assertEquals("Update a milestone 10L, 10L, 10L, MilestoneDO test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
}
