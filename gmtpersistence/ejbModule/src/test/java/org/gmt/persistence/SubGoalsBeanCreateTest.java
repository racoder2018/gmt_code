/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.SubGoalsBean;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.SubGoalsBean
 * Method: createSubGoal()
 *  
 * RF: -for all tests with Long parameters we could write some Integer param tests
 *
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.SubGoalsBean, method createSubGoal()")
class SubGoalsBeanCreateTest
{
    private static SubGoalsBean subGoalsBeanCreateTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.SubGoalsBean, "
        	+ "method createSubGoal()");
        subGoalsBeanCreateTest = mock(SubGoalsBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.SubGoalsBean, "
        	+ "method createSubGoal() completed.");
    }  
    
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(null, null)")
    final void testCreateSubGoalNullNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal null, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(null, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(null, null), subGoalsTO);
	assertEquals("Create a subgoal null, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(-1L, null)")
    final void testCreateSubGoalMuinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal -1L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(-1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(-1L, null), subGoalsTO);
	assertEquals("Create a subgoal -1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(0L, null)")
    final void testCreateSubGoalZeroNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 0L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(0L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(0L, null), subGoalsTO);
	assertEquals("Create a subgoal 0L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(1L, null)")
    final void testCreateSubGoalOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 1L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(1L, null), subGoalsTO);
	assertEquals("Create a subgoal 1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(10L, null)")
    final void testCreateSubGoalTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 10L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(10L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(10L, null), subGoalsTO);
	assertEquals("Create a subgoal 10L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(null, SubGoalDO)")
    final void testCreateSubGoalNullSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal null, subGoal test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(null, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(null, subGoal), subGoalsTO);
	assertEquals("Create a subgoal null, subGoal test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(-1L, SubGoalDO)")
    final void testCreateSubGoalMinusOneSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal -1L, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(-1L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(-1L, subGoal), subGoalsTO);
	assertEquals("Create a subgoal -1L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(0L, SubGoalDO)")
    final void testCreateSubGoalZeroSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 0L, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(0L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(0L, subGoal), subGoalsTO);
	assertEquals("Create a subgoal 0L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("SubGoalsTO = createSubGoal(1L, SubGoalDO)")
    final void testCreateSubGoalOneSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	subGoal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	subGoal.setEndDate(LocalDateTime.parse("2019-01-01T09:00"));
	subGoal.setDescription("Get a plan.");
	subGoal.setShortTerm("Planning");
	subGoal.setNotifications(true);
	subGoal.setMilestones(new ArrayList<MilestoneDO>());

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 1L, SubGoalDO test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(1L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(1L, subGoal), subGoalsTO);
	assertEquals("Create a subgoal 1L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());

	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), subGoalsTO.getResults().get(0).getBeginDate());
	assertEquals(LocalDateTime.parse("2019-01-01T09:00"), subGoalsTO.getResults().get(0).getEndDate());
	assertEquals("Get a plan.", subGoalsTO.getResults().get(0).getDescription());
	assertEquals("Planning", subGoalsTO.getResults().get(0).getShortTerm());
	assertEquals(true, subGoalsTO.getResults().get(0).getNotifications());
	assertTrue(subGoalsTO.getResults().get(0).getMilestones().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("SubGoalsTO = createSubGoal(10L, SubGoalDO)")
    final void testCreateSubGoalTenSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	subGoal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	subGoal.setEndDate(LocalDateTime.parse("2019-01-01T09:00"));
	subGoal.setDescription("Get a plan.");
	subGoal.setShortTerm("Planning");
	subGoal.setNotifications(true);
	subGoal.setMilestones(new ArrayList<MilestoneDO>());

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 10L, SubGoalDO test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanCreateTest.createSubGoal(10L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanCreateTest.createSubGoal(10L, subGoal), subGoalsTO);
	assertEquals("Create a subgoal 10L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());

	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), subGoalsTO.getResults().get(0).getBeginDate());
	assertEquals(LocalDateTime.parse("2019-01-01T09:00"), subGoalsTO.getResults().get(0).getEndDate());
	assertEquals("Get a plan.", subGoalsTO.getResults().get(0).getDescription());
	assertEquals("Planning", subGoalsTO.getResults().get(0).getShortTerm());
	assertEquals(true, subGoalsTO.getResults().get(0).getNotifications());
	assertTrue(subGoalsTO.getResults().get(0).getMilestones().isEmpty());
    }
}
