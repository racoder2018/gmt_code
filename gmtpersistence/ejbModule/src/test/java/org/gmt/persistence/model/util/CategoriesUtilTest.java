/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.CategoryDO;
import org.gmt.persistence.model.util.CategoriesTO;
import org.gmt.persistence.model.util.CategoriesUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.CategoriesUtil
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.CategoriesUtil")
class CategoriesUtilTest
{       
    @Mock
    private EntityManager em;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.CategoriesUtil");
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.CategoriesUtil completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.CategoriesUtil#getAllCategories(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error CategoriesTO = getAllCategories(null)")
    final void testGetAllCategoriesNull()
    {
	CategoriesTO categories = CategoriesUtil.getAllCategories(null);
	
	assertNotNull(categories);
	assertEquals("Parameter error", categories.getMessage());
	assertEquals(CategoriesUtil.RET_CODE_ERROR, categories.getReturncode());
	assertTrue(categories.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.CategoriesUtil#getAllCategories(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("CategoriesTO = getAllCategories(EntityManager)")
    final void testGetAllCategoriesEntityManager()
    {
	CategoryDO category = new CategoryDO();
	category.setCategory(Category.FAMILY);
	category.setId(10L);
	
	CategoriesTO categories = CategoriesUtil.getAllCategories(em);
	categories.setMessage("This is a test!");
	categories.setReturncode(CategoriesUtil.RET_CODE_OK);
	categories.getResults().add(category);
	
	assertNotNull(categories);
	assertEquals("This is a test!", categories.getMessage());
	assertEquals(CategoriesUtil.RET_CODE_OK, categories.getReturncode());
	assertEquals(Category.FAMILY, categories.getResults().get(0).getCategory());
	assertEquals(new Long(10L), categories.getResults().get(0).getId());
    }
}
