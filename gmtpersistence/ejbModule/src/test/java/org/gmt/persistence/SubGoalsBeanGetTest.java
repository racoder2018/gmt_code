/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.SubGoalsBean;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.SubGoalsBean
 * Methods: getAllSubGoals(), getSubGoalById()
 *  
 * RF: -for all tests with Long parameters we could write some Integer param tests
 *
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.SubGoalsBean, "
	+ "methods getAllSubGoals(), getSubGoalById()")
class SubGoalsBeanGetTest
{
    private static SubGoalsBean subGoalsBeanGetTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.SubGoalsBean, "
        	+ "methods getAllSubGoals(), getSubGoalById()");
        subGoalsBeanGetTest = mock(SubGoalsBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.SubGoalsBean,"
        	+ "methods getAllSubGoals(), getSubGoalById() completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#SubGoalsBean()}.
     */
    @Test
    @DisplayName("not null = getClass()")
    final void testSubGoalsBean()
    {
	assertNotNull(subGoalsBeanGetTest.getClass());
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getAllSubGoals(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getAllSubGoals(null)")
    final void testGetAllSubGoalsNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals with id null test");
	subGoalsTO.setReturncode(-1);
	
	// When
	when(subGoalsBeanGetTest.getAllSubGoals(null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getAllSubGoals(null), subGoalsTO);
	assertEquals("Get all subgoals with id null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getAllSubGoals(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getAllSubGoals(-1L)")
    final void testGetAllSubGoalsMinusOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals with negative id test");
	subGoalsTO.setReturncode(-1);
	
	// When
	when(subGoalsBeanGetTest.getAllSubGoals(-1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getAllSubGoals(-1L), subGoalsTO);
	assertEquals("Get all subgoals with negative id test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getAllSubGoals(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getAllSubGoals(0L)")
    final void testGetAllSubGoalsZero()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals with id 0 test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanGetTest.getAllSubGoals(0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getAllSubGoals(0L), subGoalsTO);
	assertEquals("Get all subgoals with id 0 test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getAllSubGoals(java.lang.Long)}.
     */
    @Test
    @DisplayName("SubGoalsTO = getAllSubGoals(1L)")
    final void testGetAllSubGoalsOne()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);

	List<SubGoalDO> subGgoalsList = new ArrayList<>();
	subGgoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a goal by id 1 test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGgoalsList);

	// When
	when(subGoalsBeanGetTest.getAllSubGoals(1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getAllSubGoals(1L), subGoalsTO);
	assertEquals("Get a goal by id 1 test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getAllSubGoals(java.lang.Long)}.
     */
    @Test
    @DisplayName("SubGoalsTO = getAllSubGoals(10L)")
    final void testGetAllSubGoalsTen()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGgoalsList = new ArrayList<>();
	subGgoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a goal by id 10 test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGgoalsList);

	// When
	when(subGoalsBeanGetTest.getAllSubGoals(10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getAllSubGoals(10L), subGoalsTO);
	assertEquals("Get a goal by id 10 test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(null, null)")
    final void testGetSubGoalByIdNullNull()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by null, null test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(null, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(null, null), subGoalsTO);
	assertEquals("Get a subgoal by null, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(null, -1L)")
    final void testGetSubGoalByIdNullMinusOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by null, -1L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(null, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(null, -1L), subGoalsTO);
	assertEquals("Get a subgoal by null, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(null, 0L)")
    final void testGetSubGoalByIdNullZero()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by null, 0L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(null, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(null, 0L), subGoalsTO);
	assertEquals("Get a subgoal by null, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(null, 1L)")
    final void testGetSubGoalByIdNullOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by null, 1L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(null, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(null, 1L), subGoalsTO);
	assertEquals("Get a subgoal by null, 1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(null, 10L)")
    final void testGetSubGoalByIdNullTen()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by null, 10L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(null, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(null, 10L), subGoalsTO);
	assertEquals("Get a subgoal by null, 10L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(-1L, null)")
    final void testGetSubGoalByIdMinusOneNull()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by -1L, null test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(-1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(-1L, null), subGoalsTO);
	assertEquals("Get a subgoal by -1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(-1L, -1L)")
    final void testGetSubGoalByIdMinusOneMinusOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by -1L, -1L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(-1L, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(-1L, -1L), subGoalsTO);
	assertEquals("Get a subgoal by -1L, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(-1L, 0L)")
    final void testGetSubGoalByIdMinusOneZero()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by -1L, 0L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(-1L, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(-1L, 0L), subGoalsTO);
	assertEquals("Get a subgoal by -1L, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(-1L, 1L)")
    final void testGetSubGoalByIdMinusOneOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by -1L, 1L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(-1L, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(-1L, 1L), subGoalsTO);
	assertEquals("Get a subgoal by -1L, 1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(-1L, 10L)")
    final void testGetSubGoalByIdMinusOneTen()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by -1L, 10L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(-1L, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(-1L, 10L), subGoalsTO);
	assertEquals("Get a subgoal by -1L, 10L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(0L, null)")
    final void testGetSubGoalByIdZeroNull()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 0L, null test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(0L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(0L, null), subGoalsTO);
	assertEquals("Get a subgoal by 0L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(0L, -1L)")
    final void testGetSubGoalByIdZeroMinusOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 0L, -1L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(0L, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(0L, -1L), subGoalsTO);
	assertEquals("Get a subgoal by 0L, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(0L, 0L)")
    final void testGetSubGoalByIdZeroZero()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 0L, 0L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(0L, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(0L, 0L), subGoalsTO);
	assertEquals("Get a subgoal by 0L, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(0L, 1L)")
    final void testGetSubGoalByIdZeroOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 0L, 1L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(0L, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(0L, 1L), subGoalsTO);
	assertEquals("Get a subgoal by 0L, 1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(0L, 10L)")
    final void testGetSubGoalByIdZeroTen()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 0L, 10L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(0L, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(0L, 10L), subGoalsTO);
	assertEquals("Get a subgoal by 0L, 10L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(1L, null)")
    final void testGetSubGoalByIdOneNull()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 1L, null test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(1L, null), subGoalsTO);
	assertEquals("Get a subgoal by 1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(1L, -1L)")
    final void testGetSubGoalByIdOneMinusOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 1L, -1L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(1L, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(1L, -1L), subGoalsTO);
	assertEquals("Get a subgoal by 1L, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(1L, 0L)")
    final void testGetSubGoalByIdOneZero()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 1L, 0L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(1L, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(1L, 0L), subGoalsTO);
	assertEquals("Get a subgoal by 1L, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("SubGoalsTO = getSubGoalById(1L, 1L)")
    final void testGetSubGoalByIdOneOne()
    {	
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 1L, 1L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(1L, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(1L, 1L), subGoalsTO);
	assertEquals("Get a subgoal by 1L, 1L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("SubGoalsTO = getSubGoalById(1L, 10L)")
    final void testGetSubGoalByIdOneTen()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 1L, 10L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(1L, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(1L, 10L), subGoalsTO);
	assertEquals("Get a subgoal by 1L, 10L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(10L, null)")
    final void testGetSubGoalByIdTenNull()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 10L, null test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(10L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(10L, null), subGoalsTO);
	assertEquals("Get a subgoal by 10L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(10L, -1L)")
    final void testGetSubGoalByIdTenMinusOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 10L, -1L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(10L, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(10L, -1L), subGoalsTO);
	assertEquals("Get a subgoal by 10L, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(10L, 0L)")
    final void testGetSubGoalByIdTenZero()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 10L, 0L test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(10L, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(10L, 0L), subGoalsTO);
	assertEquals("Get a subgoal by 10L, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("SubGoalsTO = getSubGoalById(10L, 1L)")
    final void testGetSubGoalByIdTenOne()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 10L, 1L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(10L, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(10L, 1L), subGoalsTO);
	assertEquals("Get a subgoal by 10L, 1L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#getSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("SubGoalsTO = getSubGoalById(10L, 10L)")
    final void testGetSubGoalByIdTenTen()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by 10L, 10L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanGetTest.getSubGoalById(10L, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanGetTest.getSubGoalById(10L, 10L), subGoalsTO);
	assertEquals("Get a subgoal by 10L, 10L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }
}
