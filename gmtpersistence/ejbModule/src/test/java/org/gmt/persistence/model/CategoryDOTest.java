/**
 * 
 */
package org.gmt.persistence.model;

import static org.junit.jupiter.api.Assertions.*;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.CategoryDO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.CategoryDO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.CategoryDO")
class CategoryDOTest
{
    private static CategoryDO categoryDOTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.CategoryDO");
        categoryDOTest = new CategoryDO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.CategoryDO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.GoalsDO#getId()/setId()}.
     */
    @Test
    @DisplayName("Long = getId()/setId()")
    final void testGetSetId()
    {
	categoryDOTest.setId(10L);
	assertEquals(new Long(10L), categoryDOTest.getId());

	categoryDOTest.setId(-1L);
	assertEquals(new Long(-1L), categoryDOTest.getId());

	categoryDOTest.setId(0L);
	assertEquals(new Long(0L), categoryDOTest.getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.GoalsDO#getCategory()/setCategory()}.
     */
    @Test
    @DisplayName("Category = getCategory()/setCategory()")
    final void testGetSetCategory()
    {
	categoryDOTest.setCategory(Category.FAMILY);
	assertEquals(Category.FAMILY, categoryDOTest.getCategory());
	
	categoryDOTest.setCategory(null);
	assertNull(categoryDOTest.getCategory());
    }
}
