/**
 * 
 */
package org.gmt.persistence.model.dao;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.dao.GoalDoDAO;
import org.gmt.persistence.model.util.GoalsTO;
import org.gmt.persistence.model.util.GoalsUtil;
import org.gmt.persistence.model.util.MilestonesTO;
import org.gmt.persistence.model.util.MilestonesUtil;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.gmt.persistence.model.util.SubGoalsUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.dao.GoalDoDAO, create methods
 * 
 * Note: Because of the 3/4 parameters of some methods and the resulting test 
 * 	 combinations for the tests 2 values for the 1st and 2nd parameter are 
 * 	 used and 2 for 3rd and 4th parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       milestoneId: -1L, 10L 
 *       EntityManager: null, EntityManager
 *       
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.dao.GoalDoDAO, create methods")
class GoalDoDAOCreateTest
{
    @Mock 
    private EntityManager em;
    private static GoalDoDAO goalDoDAOTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.GoalDoDAO, create methods");
        goalDoDAOTest = mock(GoalDoDAO.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.GoalDoDAO, create methods completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createGoal(org.gmt.persistence.model.GoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createGoal(null, null)")
    final void testCreateGoalNullNull()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Create a goal null, null test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createGoal(null, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createGoal(org.gmt.persistence.model.GoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createGoal(null, EntityManager)")
    final void testCreateGoalNullEntityManager()
    {
	// Given	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Create a goal null, EntityManager test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createGoal(null, em)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createGoal(org.gmt.persistence.model.GoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createGoal(GoalDO, null)")
    final void testCreateGoalGoalDONull()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Create a goal GoalDO, null test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createGoal(goal, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createGoal(org.gmt.persistence.model.GoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = createGoal(GoalDO, EntityManager)")
    final void testCreateGoalGoalDOEntityManager()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Create a goal GoalDO, EntityManager test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().addAll(goalsList);
	
	// When
	when(goalDoDAOTest.createGoal(goal, em)).thenReturn(goal);

	// Then
	assertEquals(goalDoDAOTest.createGoal(goal, em), goal);
	assertEquals("Create a goal GoalDO, EntityManager test", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createSubGoal(-1L, null, null)")
    final void testCreateSubGoalMinusOneNullNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal -1L, null, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createSubGoal(-1L, null, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createSubGoal(-1L, null, EntityManager)")
    final void testCreateSubGoalMinusOneNullEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal -1L, null, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createSubGoal(-1L, null, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createSubGoal(-1L, SubGoalDO, null)")
    final void testCreateSubGoalMinusOneSubGoalDONull()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal -1L, subGoal, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createSubGoal(-1L, subGoal, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createSubGoal(-1L, SubGoalDO, EntityManager)")
    final void testCreateSubGoalMinusOneSubGoalDOEntityManager()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal -1L, subGoal, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createSubGoal(-1L, subGoal, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createSubGoal(10L, null, null)")
    final void testCreateSubGoalTenNullNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 10L, null, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createSubGoal(10L, null, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createSubGoal(10L, null, EntityManager)")
    final void testCreateSubGoalTenNullEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 10L, null, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createSubGoal(10L, null, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createSubGoal(10L, SubGoalDO, null)")
    final void testCreateSubGoalTenSubGoalDONull()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal 10L, subGoal, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createSubGoal(10L, subGoal, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createSubGoal(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("SubGoalDO = createSubGoal(10L, SubGoalDO, EntityManager)")
    final void testCreateSubGoal()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10L, 10L, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_OK);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(goalDoDAOTest.createSubGoal(10L, subGoal, em)).thenReturn(subGoal);

	// Then
	assertEquals(goalDoDAOTest.createSubGoal(10L, subGoal, em), subGoal);
	assertEquals("Get a subgoal by id 10L, 10L, EntityManager test", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_OK, subGoalsTO.getReturncode());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }
        
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(-1L, -1L, null, null)")
    final void testCreateMilestoneById2MinusOneNullNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id -1L, -1L, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(-1L, -1L, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(-1L, -1L, null, EntityManager)")
    final void testCreateMilestoneById2MinusOneNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id -1L, -1L, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(-1L, -1L, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(-1L, -1L, MilestoneDO, null)")
    final void testCreateMilestoneById2MinusOneMilestoneDONull()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id -1L, -1L, MilestoneDO, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(-1L, -1L, milestone, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(-1L, -1L, MilestoneDO, EntityManager)")
    final void testCreateMilestoneById2MinusOneMilestoneDOEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id -1L, -1L, MilestoneDO, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(-1L, -1L, milestone, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(-1L, 10L, null, null)")
    final void testCreateMilestoneByIdMinusOneTenNullNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id -1L, 10L, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(-1L, 10L, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(-1L, 10L, null, EntityManager)")
    final void testCreateMilestoneByIdMinusOneTenNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id -1L, 10L, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(-1L, 10L, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(-1L, 10L, MilestoneDO, null)")
    final void testCreateMilestoneByIdMinusOneTenTenNull()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id -1L, 10L, MilestoneDO, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(-1L, 10L, milestone, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(-1L, 10L, MilestoneDO, EntityManager)")
    final void testCreateMilestoneByIdMinusOneTenMilestoneDOEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id -1L, 10L, MilestoneDO, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(-1L, 10L, milestone, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(10L, -1L, null, null)")
    final void testCreateMilestoneByIdTenMinusOneNullNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id 10L, -1L, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(10L, -1L, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(10L, -1L, null, EntityManager)")
    final void testCreateMilestoneByIdTenMinusOneNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id 10L, -1L, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(10L, -1L, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(10L, -1L, MilestoneDO, null)")
    final void testCreateMilestoneByIdTenMinusOneMilestoneDONull()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id 10L, -1L, MilestoneDO, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(10L, -1L, milestone, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(10L, -1L, MilestoneDO, EntityManager)")
    final void testCreateMilestoneByIdTenMinusOneMilestoneDOEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id 10L, -1L, MilestoneDO, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(10L, -1L, milestone, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(10L, 10L, null, null)")
    final void testCreateMilestoneById2TenNullNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id 10L, 10L, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(10L, 10L, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(10L, 10L, null, EntityManager)")
    final void testCreateMilestoneById2TenNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id 10L, 10L, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(10L, 10L, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createMilestone(10L, 10L, MilestoneDO, null)")
    final void testCreateMilestoneById2TenMilestoneDONull()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id 10L, 10L, MilestoneDO, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(10L, 10L, milestone, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#createMilestone(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("MilestoneDO = createMilestone(10L, 10L, MilestoneDO, EntityManager)")
    final void testCreateMilestoneById2TenMilestoneDOEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone id 10L, 10L, MilestoneDO, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.createMilestone(10L, 10L, milestone, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }  
}
