/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.StatusDO;
import org.gmt.persistence.model.util.StatusTO;
import org.gmt.persistence.model.util.StatusUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.StatusUtil
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.StatusUtil")
class StatusUtilTest
{
    @Mock
    private EntityManager em;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.StatusUtil");
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.StatusUtil completed.");
    }   
    
       
    /**
     * Test method for {@link org.gmt.persistence.model.util.StatusUtil#getAllStatus(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error StatusTO = testGetAllStatus(null)")
    final void testGetAllStatusNull()
    {
	StatusTO status = StatusUtil.getAllStatus(null);
	
	assertNotNull(status);
	assertEquals("Parameter error", status.getMessage());
	assertEquals(StatusUtil.RET_CODE_ERROR, status.getReturncode());
	assertTrue(status.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.StatusUtil#getAllStatus(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("StatusTO = getAllStatus(EntityManager)")
    final void testGetAllStatusEntityManager()
    {
	StatusDO status = new StatusDO();
	status.setStatus(Status.DEFINED);
	status.setId(10L);
	
	StatusTO statusTO = StatusUtil.getAllStatus(em);
	statusTO.setMessage("This is a test!");
	statusTO.setReturncode(StatusUtil.RET_CODE_OK);
	statusTO.getResults().add(status);
	
	assertNotNull(statusTO);
	assertEquals("This is a test!", statusTO.getMessage());
	assertEquals(StatusUtil.RET_CODE_OK, statusTO.getReturncode());
	assertEquals(Status.DEFINED, statusTO.getResults().get(0).getStatus());
	assertEquals(new Long(10L), statusTO.getResults().get(0).getId());
    }
}
