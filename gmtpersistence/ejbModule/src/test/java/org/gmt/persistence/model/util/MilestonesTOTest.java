/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.MilestonesTO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.MilestonesTO")
class MilestonesTOTest
{
    private static MilestonesTO milestonesToTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.MilestonesTO");
        milestonesToTest = new MilestonesTO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.MilestonesTO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getMessage()}.
     */
    @Test
    @DisplayName("String = getMessage()/setMessage()")
    final void testGetSetMessage()
    {
        milestonesToTest.setMessage("This is a test!");
	assertEquals("This is a test!", milestonesToTest.getMessage());
	
	milestonesToTest.setMessage("");
	assertEquals("", milestonesToTest.getMessage());
	
	milestonesToTest.setMessage("ÄöÜ");
	assertEquals("ÄöÜ", milestonesToTest.getMessage());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getReturncode()}.
     */
    @Test
    @DisplayName("Integer = getReturncode()/setReturncode()")
    final void testGetSetReturncode()
    {
        milestonesToTest.setReturncode(-1);
        assertEquals(-1, milestonesToTest.getReturncode().intValue());
        
        milestonesToTest.setReturncode(0);
        assertEquals(0, milestonesToTest.getReturncode().intValue());
        
        milestonesToTest.setReturncode(1);
        assertEquals(1, milestonesToTest.getReturncode().intValue());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getResults()}.
     */
    @Test
    @DisplayName("List<MilestoneDO> = getResults()")
    final void testGetResults()
    {
	assertTrue(milestonesToTest.getResults().isEmpty());
	
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);
	milestone.setDescription("The plan is ready.");
	milestone.setNotifications(true);
	milestone.setName("MS1");
	milestone.setTodosFinished(new ArrayList<String>());
	
	milestonesToTest.getResults().add(milestone);
	
	MilestoneDO resultMilestone = milestonesToTest.getResults().get(0);
	assertEquals(new Long(1L), resultMilestone.getId());
	assertEquals("The plan is ready.", resultMilestone.getDescription());
	assertEquals(true, resultMilestone.getNotifications());
	assertEquals("MS1", resultMilestone.getName());
	assertTrue(resultMilestone.getTodosFinished().isEmpty());
    }
}
