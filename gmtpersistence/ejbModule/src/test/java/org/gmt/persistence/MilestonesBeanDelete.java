/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.MilestonesBean;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.MilestonesBean
 * Method: deleteMilestoneById()
 * 
 * Note: Because of the 3 method parameters of the method deleteMilestoneById() 
 * 	 and the resulting test combinations for the tests 2 values for the 
 * 	 first and second parameter are used and 5 for the third parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       id:        null, -1L, 0L, 1L, 10L 
 *       
 * RF: -for all tests with Long parameters we could write some Integer param tests
 *  
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.persistence.MilestonesBean, "
	+ "method deleteMilestoneById()")
class MilestonesBeanDelete
{
    private static MilestonesBean milestonesBeanDeleteTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.MilestonesBean, "
        	+ "method deleteMilestoneById()");
        milestonesBeanDeleteTest = mock(MilestonesBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.MilestonesBean, "
        	+ "method deleteMilestoneById() completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, -1L, null)")
    final void testDeleteMilestoneByIdMinusOneMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, -1L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, null), milestonesTO);
	assertEquals("Delete a milestones id -1L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, -1L, -1L)")
    final void testDeleteMilestoneByIdMinusOneMinusOneMinusOne()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, -1L, -1L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, -1L), milestonesTO);
	assertEquals("Delete a milestones id -1L, -1L, -1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, -1L, 0L)")
    final void testDeleteMilestoneByIdMinusOneMinusOneZero()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, -1L, 0L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, 0L), milestonesTO);
	assertEquals("Delete a milestones id -1L, -1L, 0L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, -1L, 1L)")
    final void testDeleteMilestoneByIdMinusOneMinusOneOne()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, -1L, 1L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, 1L), milestonesTO);
	assertEquals("Delete a milestones id -1L, -1L, 1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, -1L, 10L)")
    final void testDeleteMilestoneByIdMinusOneMinusOneTen()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, -1L, 10L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, -1L, 10L), milestonesTO);
	assertEquals("Delete a milestones id -1L, -1L, 10L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, 10L, null)")
    final void testDeleteMilestoneByIdMinusOneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, 10L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, null), milestonesTO);
	assertEquals("Delete a milestones id -1L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, 10L, -1L)")
    final void testDeleteMilestoneByIdMinusOneTenMinusOne()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, 10L, -1L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, -1L), milestonesTO);
	assertEquals("Delete a milestones id -1L, 10L, -1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, 10L, 0L)")
    final void testDeleteMilestoneByIdMinusOneTenZero()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, 10L, 0L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, 0L), milestonesTO);
	assertEquals("Delete a milestones id -1L, 10L, 0L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, 10L, 1L)")
    final void testDeleteMilestoneByIdMinusOneTenOne()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, 10L, 1L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, 1L), milestonesTO);
	assertEquals("Delete a milestones id -1L, 10L, 1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(-1L, 10L, 10L)")
    final void testDeleteMilestoneByIdMinusOneTenTen()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1L, 10L, 10L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(-1L, 10L, 10L), milestonesTO);
	assertEquals("Delete a milestones id -1L, 10L, 10L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(10L, -1L, null)")
    final void testDeleteMilestoneByIdTenMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id 10L, -1L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, null), milestonesTO);
	assertEquals("Delete a milestones id 10L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(10L, -1L, -1L)")
    final void testDeleteMilestoneByIdTenMinusOneMinusOne()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id 10L, -1L, -1L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, -1L), milestonesTO);
	assertEquals("Delete a milestones id 10L, -1L, -1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(10L, -1L, 0L)")
    final void testDeleteMilestoneByIdTenMinusOneZero()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id 10L, -1L, 0L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, 0L), milestonesTO);
	assertEquals("Delete a milestones id 10L, -1L, 0L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(10L, -1L, 1L)")
    final void testDeleteMilestoneByIdTenMinusOneOne()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id 10L, -1L, 1L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, 1L), milestonesTO);
	assertEquals("Delete a milestones id 10L, -1L, 1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(10L, -1L, 10L)")
    final void testDeleteMilestoneByIdTenMinusOneTen()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id 10L, -1L, 10L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, -1L, 10L), milestonesTO);
	assertEquals("Delete a milestones id 10L, -1L, 10L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(10L, 10L, null)")
    final void testDeleteMilestoneByIdTenTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id 10L, 10L, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, null), milestonesTO);
	assertEquals("Delete a milestones id 10L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
        
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(10L, 10L, -1L)")
    final void testDeleteMilestoneByIdTenTenMinusOne()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id 10L, 10L, -1L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, -1L), milestonesTO);
	assertEquals("Delete a milestones id 10L, 10L, -1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(10L, 10L, 0L)")
    final void testDeleteMilestoneByIdTenTenZero()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id 10L, 10L, 0L test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, 0L), milestonesTO);
	assertEquals("Delete a milestones id 10L, 10L, 0L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("MilestonesTO = deleteMilestoneById(10L, 10L, 1L)")
    final void testDeleteMilestoneByIdTenTenOne()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);

	List<MilestoneDO> miletonesList = new ArrayList<>();
	miletonesList.add(milestone);

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, 10L, 1L test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(miletonesList);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, 1L), milestonesTO);
	assertEquals("Get a milestone by id 10L, 10L, 1L test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(1L), milestonesTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#deleteMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("MilestonesTO = deleteMilestoneById(10L, 10L, 10L)")
    final void testDeleteMilestoneByIdTenTenTen()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(10L);

	List<MilestoneDO> miletonesList = new ArrayList<>();
	miletonesList.add(milestone);

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, 10L, 10L test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(miletonesList);

	// When
	when(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanDeleteTest.deleteMilestoneById(10L, 10L, 10L), milestonesTO);
	assertEquals("Get a milestone by id 10L, 10L, 10L test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(10L), milestonesTO.getResults().get(0).getId());
    }
}
