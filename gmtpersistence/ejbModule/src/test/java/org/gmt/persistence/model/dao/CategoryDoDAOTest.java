/**
 * 
 */
package org.gmt.persistence.model.dao;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.CategoryDO;
import org.gmt.persistence.model.dao.CategoryDoDAO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.dao.CategoryDoDAO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.dao.CategoryDoDAO")
class CategoryDoDAOTest
{
    @Mock
    private EntityManager em;
    private static CategoryDoDAO categoryDoDAOTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.CategoryDoDAO");
        categoryDoDAOTest = mock(CategoryDoDAO.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.CategoryDoDAO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.CategoryDoDAO#getInstance()}.
     */
    @Test
    @DisplayName("CategoryDoDAO = getInstance()")
    final void testGetInstance()
    {
	assertNotNull(CategoryDoDAO.getInstance());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.dao.CategoryDoDAO#getAllCategories(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<CategoryDO> = getAllCategories(null)")
    final void testGetAllCategoriesNull()
    {
	List<CategoryDO> categoriesList = new ArrayList<>();
	
	when(categoryDoDAOTest.getAllCategories(null)).thenReturn(categoriesList);
	
	assertNotNull(categoriesList);
	assertTrue(categoriesList.isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.CategoryDoDAO#getAllCategories(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<CategoryDO> = getAllCategories(EntityManager)")
    final void testGetAllCategoriesEntityManager()
    {	
	CategoryDO category = new CategoryDO();
	category.setId(1L);
	category.setCategory(Category.HOBBY);
	
	List<CategoryDO> categoriesList = new ArrayList<CategoryDO>();
	categoriesList.add(category);
	
	when(categoryDoDAOTest.getAllCategories(em)).thenReturn(categoriesList);

	assertNotNull(categoriesList);
	assertFalse(categoriesList.isEmpty());
	assertEquals(Category.HOBBY, categoriesList.get(0).getCategory());
	assertEquals(new Long(1L), categoriesList.get(0).getId());
    }
}
