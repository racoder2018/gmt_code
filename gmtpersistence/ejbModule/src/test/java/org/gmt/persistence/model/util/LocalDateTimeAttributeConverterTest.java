/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.gmt.persistence.model.util.LocalDateTimeAttributeConverter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.LocalDateTimeAttributeConverter
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.LocalDateTimeAttributeConverter")
class LocalDateTimeAttributeConverterTest
{
    private static LocalDateTimeAttributeConverter ldtac;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.LocalDateTimeAttributeConverter");
        ldtac = new LocalDateTimeAttributeConverter();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.LocalDateTimeAttributeConverter completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.LocalDateTimeAttributeConverter#convertToDatabaseColumn(java.time.LocalDateTime)}.
     */
    @Test
    @DisplayName("null = convertToDatabaseColumn(null)")
    final void testConvertToDatabaseColumnNull()
    {
	assertNull(ldtac.convertToDatabaseColumn(null));
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.LocalDateTimeAttributeConverter#convertToDatabaseColumn(java.time.LocalDateTime)}.
     */
    @Test
    @DisplayName("Timestamp = convertToDatabaseColumn(LocalDateTime)")
    final void testConvertToDatabaseColumnLocalDateTime()
    {
	assertEquals(Timestamp.valueOf("2019-01-01 10:10:00"), 
		ldtac.convertToDatabaseColumn(LocalDateTime.parse("2019-01-01T10:10:00")));
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.LocalDateTimeAttributeConverter#convertToEntityAttribute(java.sql.Timestamp)}.
     */
    @Test
    @DisplayName("null = convertToEntityAttribute(null)")
    final void testConvertToEntityAttributeNull()
    {
	assertNull(ldtac.convertToEntityAttribute(null));
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.LocalDateTimeAttributeConverter#convertToEntityAttribute(java.sql.Timestamp)}.
     */
    @Test
    @DisplayName("LocalDateTime = convertToEntityAttribute(Timestamp)")
    final void testConvertToEntityAttributeTimestamp()
    {
	assertEquals(LocalDateTime.parse("2019-01-01T10:10:00"), 
		ldtac.convertToEntityAttribute(Timestamp.valueOf("2019-01-01 10:10:00")));
    }
}
