/**
 * 
 */
package org.gmt.persistence.model.dao;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.dao.GoalDoDAO;
import org.gmt.persistence.model.util.GoalsTO;
import org.gmt.persistence.model.util.GoalsUtil;
import org.gmt.persistence.model.util.MilestonesTO;
import org.gmt.persistence.model.util.MilestonesUtil;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.gmt.persistence.model.util.SubGoalsUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.dao.GoalDoDAO,
 * update methods
 * 
 * Note: Because of the 3/4 parameters of some methods and the resulting test 
 * 	 combinations for the tests 2 values for the 1st and 2nd parameter are 
 * 	 used and 2 for 3rd and 4th parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       milestoneId: -1L, 10L 
 *       EntityManager: null, EntityManager
 *       
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.dao.GoalDoDAO, update methods")
class GoalDoDAOUpdateTest
{
    @Mock 
    private EntityManager em;
    private static GoalDoDAO goalDoDAOTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.GoalDoDAO, update methods");
        goalDoDAOTest = mock(GoalDoDAO.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.GoalDoDAO, update methods completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateGoal(org.gmt.persistence.model.GoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateGoal(null, null)")
    final void testUpdateGoalNullNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Update a goal null, null test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateGoal(null, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
       
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateGoal(org.gmt.persistence.model.GoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateGoal(null, EntityManager)")
    final void testUpdateGoalNullEntityManager()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Update a goal null, EntityManager test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateGoal(null, em)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateGoal(org.gmt.persistence.model.GoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateGoal(GoalDO, null)")
    final void testUpdateGoalGoalDONull()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Update a goal GoalDO, null test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateGoal(goal, null)).thenReturn(null);

	// Then
	assertTrue(goalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateGoal(org.gmt.persistence.model.GoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("GoalDO = updateGoal(GoalDO, EntityManager)")
    final void testUpdateGoalGoalDOEntityManager()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);
	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Update a goal GoalDO, EntityManager test");
	goalsTO.setReturncode(GoalsUtil.RET_CODE_OK);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalDoDAOTest.updateGoal(goal, em)).thenReturn(goal);

	// Then
	assertEquals(goalDoDAOTest.updateGoal(goal, em), goal);
	assertEquals("Update a goal GoalDO, EntityManager test", goalsTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, goalsTO.getReturncode());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateSubGoalById(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateSubGoalById(-1L, null, null)")
    final void testUpdateSubGoalByIdMinusOneNullNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal by id -1L, null, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateSubGoalById(-1L, null, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateSubGoalById(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateSubGoalById(-1L, null, EntityManager)")
    final void testUpdateSubGoalByIdMinusOneNullEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal by id -1L, null, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateSubGoalById(-1L, null, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateSubGoalById(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateSubGoalById(-1L, SubGoalDO, null)")
    final void testUpdateSubGoalByIdMinusOneSubGoalDONull()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal by id -1L, SubGoalDO, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateSubGoalById(-1L, subGoal, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateSubGoalById(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateSubGoalById(-1L, SubGoalDO, EntityManager)")
    final void testUpdateSubGoalByIdMinusOneSubGoalDOEntityManager()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal by id -1L, SubGoalDO, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateSubGoalById(-1L, subGoal, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateSubGoalById(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateSubGoalById(10L, null, null)")
    final void testUpdateSubGoalByIdTenMinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal by id 10L, null, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateSubGoalById(10L, null, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateSubGoalById(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateSubGoalById(10L, null, EntityManager)")
    final void testUpdateSubGoalByIdTenMinusOneEntityManager()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal by id 10L, null, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateSubGoalById(10L, null, em)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateSubGoalById(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateSubGoalById(10L, SubGoalDO, null)")
    final void testUpdateSubGoalByIdTenSubGoalDONull()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal by id 10L, SubGoalDO, null test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateSubGoalById(10L, subGoal, null)).thenReturn(null);

	// Then
	assertTrue(subGoalsTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateSubGoalById(java.lang.Long, org.gmt.persistence.model.SubGoalDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("SubGoalDO = updateSubGoalById(10L, SubGoalDO, EntityManager)")
    final void testUpdateSubGoalByIdTenSubGoalDOEntityManager()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal by id 10L, SubGoalDO, EntityManager test");
	subGoalsTO.setReturncode(SubGoalsUtil.RET_CODE_OK);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(goalDoDAOTest.updateSubGoalById(10L, subGoal, em)).thenReturn(subGoal);

	// Then
	assertEquals(goalDoDAOTest.updateSubGoalById(10L, subGoal, em), subGoal);
	assertEquals("Update a subgoal by id 10L, SubGoalDO, EntityManager test", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_OK, subGoalsTO.getReturncode());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }   
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(-1L, -1L, null, null)")
    final void testUpdateMilestoneById2MinusOneNullNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, -1L, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(-1L, -1L, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(-1L, -1L, null, EntityManager)")
    final void testUpdateMilestoneById2MinusOneNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, -1L, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(-1L, -1L, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(-1L, -1L, MilestoneDO, null)")
    final void testUpdateMilestoneById2MinusOneMilestoneDONull()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, -1L, MilestoneDO, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(-1L, -1L, milestone, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(-1L, -1L, MilestoneDO, EntityManager)")
    final void testUpdateMilestoneById2MinusOneMilestoneDOEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, -1L, MilestoneDO, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(-1L, -1L, milestone, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(-1L, 10L, null, null)")
    final void testUpdateMilestoneByIdMinusOneTenNullNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, 10L, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(-1L, 10L, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(-1L, 10L, null, EntityManager)")
    final void testUpdateMilestoneByIdMinusOneTenNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, 10L, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(-1L, 10L, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(-1L, 10L, MilestoneDO, null)")
    final void testUpdateMilestoneByIdMinusOneTenTenNull()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, 10L, MilestoneDO, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(-1L, 10L, milestone, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(-1L, 10L, MilestoneDO, EntityManager)")
    final void testUpdateMilestoneByIdMinusOneTenMilestoneDOEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id -1L, 10L, MilestoneDO, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(-1L, 10L, milestone, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(10L, -1L, null, null)")
    final void testUpdateMilestoneByIdTenMinusOneNullNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(10L, -1L, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(10L, -1L, null, EntityManager)")
    final void testUpdateMilestoneByIdTenMinusOneNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(10L, -1L, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(10L, -1L, MilestoneDO, null)")
    final void testUpdateMilestoneByIdTenMinusOneMilestoneDONull()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, MilestoneDO, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(10L, -1L, milestone, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(10L, -1L, MilestoneDO, EntityManager)")
    final void testUpdateMilestoneByIdTenMinusOneMilestoneDOEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, -1L, MilestoneDO, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(10L, -1L, milestone, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(10L, 10L, null, null)")
    final void testUpdateMilestoneById2TenNullNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, null, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(10L, 10L, null, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(10L, 10L, null, EntityManager)")
    final void testUpdateMilestoneById2TenNullEntityManager()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, null, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(10L, 10L, null, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = updateMilestoneById(10L, 10L, MilestoneDO, null)")
    final void testUpdateMilestoneById2TenMilestoneDONull()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, MilestoneDO, null test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(10L, 10L, milestone, null)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.GoalDoDAO#updateMilestoneById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.MilestoneDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("MilestoneDO = updateMilestoneById(10L, 10L, MilestoneDO, EntityManager)")
    final void testUpdateMilestoneById2TenMilestoneDOEntityManager()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones id 10L, 10L, MilestoneDO, EntityManager test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_ERROR);

	// When
	when(goalDoDAOTest.updateMilestoneById(10L, 10L, milestone, em)).thenReturn(null);

	// Then
	assertTrue(milestonesTO.getResults().isEmpty()); 
    }  
}
