/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.util.GoalsUtil;
import org.gmt.persistence.model.util.UsersTO;
import org.gmt.persistence.model.util.UsersUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.UsersUtil
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.UsersUtil")
class UsersUtilTest
{
    @Mock
    private EntityManager em;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.UsersUtil");
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.UsersUtil completed.");
    }   
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.UsersUtil#getAllUsers(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error UsersTO = getAllUsers(null)")
    final void testGetAllUsersNull()
    {
	UsersTO usersTO = UsersUtil.getAllUsers(null);
	
	assertEquals("Parameter error", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_ERROR, usersTO.getReturncode());	
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.UsersUtil#getAllUsers(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error UsersTO = getAllUsers(EntityManager)")
    final void testGetAllUsersEntityManager()
    {
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Feuerstein");
	user.setLoginName("hfeuerstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);
	
	UsersTO usersTO = UsersUtil.getAllUsers(em);
	usersTO.setMessage("Get all Users test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);
	
	assertEquals("Get all Users test", usersTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, usersTO.getReturncode());
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Feuerstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hfeuerstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.UsersUtil#createUsersTO(java.lang.String, org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("error UsersTO = createUsersTO(message, null)")
    final void testCreateUsersTONull()
    {
	UsersTO usersTO = UsersUtil.createUsersTO("", null);
	
	assertEquals("Parameter error", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_ERROR, usersTO.getReturncode());	
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.UsersUtil#createUsersTO(java.lang.String, org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("error UsersTO = createUsersTO(message, UserDO)")
    final void testCreateUsersTO()
    {
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Feuerstein");
	user.setLoginName("hfeuerstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);
	
	UsersTO usersTO = UsersUtil.createUsersTO("Create a UsersTO test", user);
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);
	
	assertEquals("Create a UsersTO test", usersTO.getMessage());
	assertEquals(GoalsUtil.RET_CODE_OK, usersTO.getReturncode());
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Feuerstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hfeuerstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword());
    }
}
