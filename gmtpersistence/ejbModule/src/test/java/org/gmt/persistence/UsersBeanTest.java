/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.UsersBean;
import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.util.UsersTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.UsersBean
 * 
 * RF: -for all tests with Long parameters we could write some Integer param tests
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.persistence.UsersBean")
class UsersBeanTest
{
    private static UsersBean usersBeanTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.StatusBean");
        usersBeanTest = mock(UsersBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.StatusBean completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#UsersBean()}.
     */
    @Test
    @DisplayName("not null = getClass()")
    final void testUsersBean()
    {
	assertNotNull(usersBeanTest.getClass());
    }

    /**
     * Test method for {@link org.gmt.persistence.UsersBean#getAllUsers()}.
     */
    @Test
    @DisplayName("empty UsersTO = getAllUsers()")
    final void testGetAllUsersRetEmptyUsersTO()
    {
	// Given
	List<UserDO> usersList = new ArrayList<>();
	UsersTO usersTO = new UsersTO();

	// When
	when(usersBeanTest.getAllUsers()).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.getAllUsers(), usersTO);
	assertNull(usersTO.getMessage());
	assertNull(usersTO.getReturncode());
	assertEquals(usersList, usersTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#getAllUsers()}.
     */
    @Test
    @DisplayName("UsersTO = getAllUsers()")
    final void testGetAllUsersRetUsersTO()
    {
	// Given
	List<UserDO> usersList = new ArrayList<>();
	
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get all users test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersBeanTest.getAllUsers()).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.getAllUsers(), usersTO);
	assertEquals("Get all users test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(usersList, usersTO.getResults());
    }

    /**
     * Test method for {@link org.gmt.persistence.UsersBean#getUserById(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty UsersTO = getUserById(-1L)")
    final void testGetUserByIdMinusOne()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by negative id test");
	usersTO.setReturncode(-1);

	// When
	when(usersBeanTest.getUserById(-1L)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.getUserById(-1L), usersTO);
	assertEquals("Get a user by negative id test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());   
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#getUserById(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty UsersTO = getUserById(0L)")
    final void testGetUserByIdZero()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id 0 test");
	usersTO.setReturncode(-1);

	// When
	when(usersBeanTest.getUserById(0L)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.getUserById(0L), usersTO);
	assertEquals("Get a user by id 0 test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#getUserById(java.lang.Long)}.
     */
    @Test
    @DisplayName("UsersTO = getUserById(1L)")
    final void testGetUserByIdOne()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);

	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id=1 test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersBeanTest.getUserById(1L)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.getUserById(1L), usersTO);
	assertEquals("Get a user by id=1 test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(usersList, usersTO.getResults());
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#getUserById(java.lang.Long)}.
     */
    @Test
    @DisplayName("UsersTO = getUserById(10L)")
    final void testGetUserByIdTen()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(10L);

	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id 10 test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersBeanTest.getUserById(10L)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.getUserById(10L), usersTO);
	assertEquals("Get a user by id 10 test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(usersList, usersTO.getResults());
	assertEquals(new Long(10L), usersTO.getResults().get(0).getId());
    }
      
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#createUser(org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("empty UsersTO = createUser(null)")
    final void testCreateUserNull()
    {
	// Given			
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a user null test");
	usersTO.setReturncode(-1);	

	// When
	when(usersBeanTest.createUser(null)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.createUser(null), usersTO);
	assertEquals("Create a user null test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#createUser(org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("UsersTO = createUser(UserDO)")
    final void testCreateUserUserDO()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Feuerstein");
	user.setLoginName("hfeuerstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a user test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersBeanTest.createUser(user)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.createUser(user), usersTO);
	assertEquals("Create a user test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	
	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Feuerstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hfeuerstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword());
    }

    /**
     * Test method for {@link org.gmt.persistence.UsersBean#updateUser(org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("empty UsersTO = updateUser(null)")
    final void testUpdateUserNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Update a user null test");
	usersTO.setReturncode(-1);

	// When
	when(usersBeanTest.updateUser(null)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.updateUser(null), usersTO);
	assertEquals("Update a user null test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#updateUser(org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("UsersTO = updateUser(UserDO)")
    final void testUpdateUserUsersDO()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Blankenstein");
	user.setLoginName("hblankenstein");
	user.setPassword("abcdef1234");

	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Update a user test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersBeanTest.updateUser(user)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.updateUser(user), usersTO);
	assertEquals("Update a user test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());

	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Blankenstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hblankenstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword());    
    }

    /**
     * Test method for {@link org.gmt.persistence.UsersBean#deleteUserById(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty UsersTO = deleteUserById(-1L)")
    final void testDeleteUserByIdMinusOne()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id -1 test");
	usersTO.setReturncode(-1);

	// When
	when(usersBeanTest.deleteUserById(-1L)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.deleteUserById(-1L), usersTO);
	assertEquals("Delete a user id -1 test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#deleteUserById(java.lang.Long)}.
     */
    @Test
    @DisplayName("empty UsersTO = deleteUserById(0L)")
    final void testDeleteUserByIdZero()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id 0 test");
	usersTO.setReturncode(-1);

	// When
	when(usersBeanTest.deleteUserById(0L)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.deleteUserById(0L), usersTO);
	assertEquals("Delete a user id 0 test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());    
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#deleteUserById(java.lang.Long)}.
     */
    @Test
    @DisplayName("UsersTO = deleteUserById(1L)")
    final void testDeleteUserByIdOne()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);

	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id 1 test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersBeanTest.deleteUserById(1L)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.deleteUserById(1L), usersTO);
	assertEquals("Delete a user id 1 test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());   
    }
    
    /**
     * Test method for {@link org.gmt.persistence.UsersBean#deleteUserById(java.lang.Long)}.
     */
    @Test
    @DisplayName("UsersTO = deleteUserById(10L)")
    final void testDeleteUserById()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(10L);
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);
	
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id 10 test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersBeanTest.deleteUserById(10L)).thenReturn(usersTO);

	// Then
	assertEquals(usersBeanTest.deleteUserById(10L), usersTO);
	assertEquals("Delete a user id 10 test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(new Long(10L), usersTO.getResults().get(0).getId());   
    }    
}
