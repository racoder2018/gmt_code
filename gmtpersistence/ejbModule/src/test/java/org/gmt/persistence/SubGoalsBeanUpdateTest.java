/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.SubGoalsBean;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.SubGoalsBean
 * Method: updateSubGoalById()
 * 
 * Note: Because of the 3 method parameters and the resulting test combinations 
 *       for the tests 2 values for the first parameter (goalId) are used and 5 
 *       for the second and third parameter:
 *       goalId:  -1L, 10L
 *       id:      null, -1L, 0L, 1L, 10L 
 *       subGoal: null, SubGoalDO
 * 
 * RF: -for all tests with Long parameters we could write some Integer param tests
 *
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.SubGoalsBean, method updateSubGoalById()")
class SubGoalsBeanUpdateTest
{
    private static SubGoalsBean subGoalsBeanUpdateTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.SubGoalsBean, "
        	+ "method updateSubGoalById()");
        subGoalsBeanUpdateTest = mock(SubGoalsBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.SubGoalsBean, "
        	+ "method updateSubGoalById() completed.");
    }  
    
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, null, null)")
    final void testUpdateSubGoalByIdMinusOneNullNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, null, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, null, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, null, null), subGoalsTO);
	assertEquals("Update a subgoal -1L, null, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, -1L, null)")
    final void testUpdateSubGoalByIdMinusOneMinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, -1L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, -1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, -1L, null), subGoalsTO);
	assertEquals("Update a subgoal -1L, -1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, 0L, null)")
    final void testUpdateSubGoalByIdMinusOneZeroNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, 0L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 0L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 0L, null), subGoalsTO);
	assertEquals("Update a subgoal -1L, 0L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, 1L, null)")
    final void testUpdateSubGoalByIdMinusOneOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, 1L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 1L, null), subGoalsTO);
	assertEquals("Update a subgoal -1L, 1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, 10L, null)")
    final void testUpdateSubGoalByIdMinusOneTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, 10L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 10L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 10L, null), subGoalsTO);
	assertEquals("Update a subgoal -1L, 10L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, null, SubGoalDO)")
    final void testUpdateSubGoalByIdMinusOneNullSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, null, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, null, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, null, subGoal), subGoalsTO);
	assertEquals("Update a subgoal -1L, null, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, -1L, SubGoalDO)")
    final void testUpdateSubGoalByIdMinusOneMinusOneSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, -1L, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, -1L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, -1L, subGoal), subGoalsTO);
	assertEquals("Update a subgoal -1L, -1L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, 0L, SubGoalDO)")
    final void testUpdateSubGoalByIdMinusOneZeroMinusSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, 0L, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 0L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 0L, subGoal), subGoalsTO);
	assertEquals("Update a subgoal -1L, 0L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, 1L, SubGoalDO)")
    final void testUpdateSubGoalByIdMinusOneOneMinusSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, 1L, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 1L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 1L, subGoal), subGoalsTO);
	assertEquals("Update a subgoal -1L, 1L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(-1L, 10L, SubGoalDO)")
    final void testUpdateSubGoalByIdMinusOneTenMinusSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1L, 10L, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 10L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(-1L, 10L, subGoal), subGoalsTO);
	assertEquals("Update a subgoal -1L, 10L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, null, null)")
    final void testUpdateSubGoalByIdTenNullNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, null, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, null, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, null, null), subGoalsTO);
	assertEquals("Update a subgoal 10L, null, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, -1L, null)")
    final void testUpdateSubGoalByIdTenMinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, -1L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, -1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, -1L, null), subGoalsTO);
	assertEquals("Update a subgoal 10L, -1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, 0L, null)")
    final void testUpdateSubGoalByIdTenZeroNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, 0L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, 0L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, 0L, null), subGoalsTO);
	assertEquals("Update a subgoal 10L, 0L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, 1L, null)")
    final void testUpdateSubGoalByIdTenOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, 1L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, 1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, 1L, null), subGoalsTO);
	assertEquals("Update a subgoal 10L, 1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, 10L, null)")
    final void testUpdateSubGoalByIdTenTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, 10L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, 10L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, 10L, null), subGoalsTO);
	assertEquals("Update a subgoal 10L, 10L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, null, SubGoalDO)")
    final void testUpdateSubGoalByIdTenNullSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, null, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, null, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, null, subGoal), subGoalsTO);
	assertEquals("Update a subgoal 10L, null, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, -1L, SubGoalDO)")
    final void testUpdateSubGoalByIdTenMinusOneSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, -1L, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, -1L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, -1L, subGoal), subGoalsTO);
	assertEquals("Update a subgoal 10L, -1L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, 0L, SubGoalDO)")
    final void testUpdateSubGoalByIdTenZeroSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, 0L, SubGoalDO test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, 0L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, 0L, subGoal), subGoalsTO);
	assertEquals("Update a subgoal 10L, 0L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, 1L, SubGoalDO)")
    final void testUpdateSubGoalByIdTenOneSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	subGoal.setDescription("Make a better car from wood.");
	subGoal.setStatus(Status.RUNNING);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, 1L, SubGoalDO test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, 1L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, 1L, subGoal), subGoalsTO);
	assertEquals("Update a subgoal 10L, 1L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
	assertEquals("Make a better car from wood.", subGoalsTO.getResults().get(0).getDescription());
	assertEquals(Status.RUNNING, subGoalsTO.getResults().get(0).getStatus());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#updateSubGoalById(java.lang.Long, java.lang.Long, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(10L, 10L, SubGoalDO)")
    final void testUpdateSubGoalByIdTenTenSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	subGoal.setDescription("Make a better car from wood.");
	subGoal.setStatus(Status.RUNNING);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10L, 10L, SubGoalDO test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanUpdateTest.updateSubGoalById(10L, 10L, subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanUpdateTest.updateSubGoalById(10L, 10L, subGoal), subGoalsTO);
	assertEquals("Update a subgoal 10L, 10L, SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
	assertEquals("Make a better car from wood.", subGoalsTO.getResults().get(0).getDescription());
	assertEquals(Status.RUNNING, subGoalsTO.getResults().get(0).getStatus());	
    }   
}
