/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.MilestonesUtil;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.gmt.persistence.model.util.SubGoalsUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.SubGoalsUtil
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.SubGoalsUtil")
class SubGoalsUtilTest
{
    @Mock
    private EntityManager em;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.SubGoalsUtil");
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.SubGoalsUtil completed.");
    }   
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(null, null)")
    final void testGetAllSubGoalsNullNull()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.getAllSubGoals(null, null);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(null, EntityManager)")
    final void testGetAllSubGoalsNullEntityManager()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.getAllSubGoals(null, em);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(-1L, null)")
    final void testGetAllSubGoalsMinusOneNull()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.getAllSubGoals(-1L, null);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(-1L, EntityManager)")
    final void testGetAllSubGoalsMinusOneEntityManager()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.getAllSubGoals(-1L, em);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(0L, null)")
    final void testGetAllSubGoalsZeroNull()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.getAllSubGoals(0L, null);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(0L, EntityManager)")
    final void testGetAllSubGoalsZeroEntityManager()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.getAllSubGoals(0L, em);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(1L, null)")
    final void testGetAllSubGoalsOneNull()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.getAllSubGoals(1L, null);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(1L, EntityManager)")
    final void testGetAllSubGoalsOneEntityManager()
    {
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
		
	SubGoalsTO resultSubGoals = SubGoalsUtil.getAllSubGoals(1L, em);	
	resultSubGoals.setMessage("Get a subgoal by 1L, EntityManager test");
	resultSubGoals.setReturncode(SubGoalsUtil.RET_CODE_OK);
	resultSubGoals.getResults().addAll(subGoalsList);
	
	assertEquals("Get a subgoal by 1L, EntityManager test", resultSubGoals.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_OK, resultSubGoals.getReturncode());	
	assertEquals(new Long(1L), resultSubGoals.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(10L, null)")
    final void testGetAllSubGoalsTenNull()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.getAllSubGoals(10L, null);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#getAllSubGoals(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error SubGoalsTO = getAllSubGoals(10L, EntityManager)")
    final void testGetAllSubGoalsTenEntityManager()
    {
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
		
	SubGoalsTO resultSubGoals = SubGoalsUtil.getAllSubGoals(10L, em);	
	resultSubGoals.setMessage("Get a subgoal by 10L, EntityManager test");
	resultSubGoals.setReturncode(SubGoalsUtil.RET_CODE_OK);
	resultSubGoals.getResults().addAll(subGoalsList);
	
	assertEquals("Get a subgoal by 10L, EntityManager test", resultSubGoals.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_OK, resultSubGoals.getReturncode());	
	assertEquals(new Long(10L), resultSubGoals.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#createSubGoalsTO(java.lang.String, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("error MilestonesTO = createSubGoalsTO(message, null)")
    final void testCreateSubGoalsTONull()
    {
	SubGoalsTO subGoalsTO = SubGoalsUtil.createSubGoalsTO("", null);
	
	assertEquals("Parameter error", subGoalsTO.getMessage());
	assertEquals(SubGoalsUtil.RET_CODE_ERROR, subGoalsTO.getReturncode());	
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.SubGoalsUtil#createSubGoalsTO(java.lang.String, org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("error MilestonesTO = createSubGoalsTO(message, SubGoalDO)")
    final void testCreateSubGoalsTO()
    {
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	subGoal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	subGoal.setEndDate(LocalDateTime.parse("2019-01-01T09:00"));
	subGoal.setDescription("Get a plan.");
	subGoal.setShortTerm("Planning");
	subGoal.setNotifications(true);
	subGoal.setMilestones(new ArrayList<MilestoneDO>());

	SubGoalsTO subGoalsTO = SubGoalsUtil.createSubGoalsTO("Create a SubGoalDO test", subGoal);
	
	assertEquals("Create a SubGoalDO test", subGoalsTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_OK, subGoalsTO.getReturncode());
	
	SubGoalDO resultSubGoal = subGoalsTO.getResults().get(0);	
	assertEquals(new Long(10L), resultSubGoal.getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), resultSubGoal.getBeginDate());
	assertEquals(LocalDateTime.parse("2019-01-01T09:00"), resultSubGoal.getEndDate());
	assertEquals("Get a plan.", resultSubGoal.getDescription());
	assertEquals("Planning", resultSubGoal.getShortTerm());
	assertEquals(true, resultSubGoal.getNotifications());
	assertTrue(resultSubGoal.getMilestones().isEmpty());
    }
}
