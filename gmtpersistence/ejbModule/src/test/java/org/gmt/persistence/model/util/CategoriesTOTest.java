/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.CategoryDO;
import org.gmt.persistence.model.util.CategoriesTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.CategoriesTO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.CategoriesTO")
class CategoriesTOTest
{
    private static CategoriesTO categoriesToTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.CategoriesTO");
        categoriesToTest = new CategoriesTO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.CategoriesTO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.CategoriesTO#getMessage()}.
     */
    @Test
    @DisplayName("String = getMessage()/setMessage()")
    final void testGetSetMessage()
    {
        categoriesToTest.setMessage("This is a test!");
	assertEquals("This is a test!", categoriesToTest.getMessage());
	
	categoriesToTest.setMessage("");
	assertEquals("", categoriesToTest.getMessage());
	
	categoriesToTest.setMessage("ÄöÜ");
	assertEquals("ÄöÜ", categoriesToTest.getMessage());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.CategoriesTO#getReturncode()}.
     */
    @Test
    @DisplayName("Integer = getReturncode()/setReturncode()")
    final void testGetSetReturncode()
    {
        categoriesToTest.setReturncode(-1);
        assertEquals(-1, categoriesToTest.getReturncode().intValue());
        
        categoriesToTest.setReturncode(0);
        assertEquals(0, categoriesToTest.getReturncode().intValue());
        
        categoriesToTest.setReturncode(1);
        assertEquals(1, categoriesToTest.getReturncode().intValue());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.CategoriesTO#getResults()}.
     */
    @Test
    @DisplayName("List<CategoryDO> = getResults()")
    final void testGetResults()
    {
	assertTrue(categoriesToTest.getResults().isEmpty());
	
	CategoryDO category = new CategoryDO();
	category.setCategory(Category.FAMILY);
	category.setId(10L);
	categoriesToTest.getResults().add(category);
	
	CategoryDO resultCat = categoriesToTest.getResults().get(0);
	assertEquals(Category.FAMILY, resultCat.getCategory());
	assertEquals(new Long(10L), resultCat.getId());
    }
}
