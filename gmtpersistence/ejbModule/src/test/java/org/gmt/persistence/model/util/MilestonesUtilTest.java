/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.gmt.persistence.model.util.MilestonesUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.MilestonesUtil
 *  
 * Note: Because of the 3 method parameters and the resulting test combinations 
 *       for the tests 2 values for the first and second parameter are used and 2 
 *       for third parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       EntityManager: null, EntityManager
 *       
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.MilestonesUtil")
class MilestonesUtilTest
{
    @Mock
    private EntityManager em;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.MilestonesUtil");
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.MilestonesUtil completed.");
    }   
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error MilestonesTO = getAllMilestones(-1L, -1L, null)")
    final void testGetAllMilestonesMinusOneMinusOneNull()
    {
	MilestonesTO milestonesTO = MilestonesUtil.getAllMilestones(-1L, -1L, null);
	
	assertEquals("Parameter error", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_ERROR, milestonesTO.getReturncode());	
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error MilestonesTO = getAllMilestones(-1L, -1L, EntityManager)")
    final void testGetAllMilestonesMinusOneMinusOneEntityManager()
    {
	MilestonesTO milestonesTO = MilestonesUtil.getAllMilestones(-1L, -1L, em);
	
	assertEquals("Parameter error", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_ERROR, milestonesTO.getReturncode());	
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error MilestonesTO = getAllMilestones(-1L, 10L, null)")
    final void testGetAllMilestonesMinusOneTenNull()
    {
	MilestonesTO milestonesTO = MilestonesUtil.getAllMilestones(-1L, 10L, null);
	
	assertEquals("Parameter error", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_ERROR, milestonesTO.getReturncode());	
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error MilestonesTO = getAllMilestones(-1L, 10L, EntityManager)")
    final void testGetAllMilestonesMinusOneTenEntityManager()
    {
	MilestonesTO milestonesTO = MilestonesUtil.getAllMilestones(-1L, 10L, em);
	
	assertEquals("Parameter error", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_ERROR, milestonesTO.getReturncode());	
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error MilestonesTO = getAllMilestones(10L, -1L, null)")
    final void testGetAllMilestonesTenMinusOneNull()
    {
	MilestonesTO milestonesTO = MilestonesUtil.getAllMilestones(10L, -1L, null);
	
	assertEquals("Parameter error", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_ERROR, milestonesTO.getReturncode());	
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error MilestonesTO = getAllMilestones(10L, -1L, EntityManager)")
    final void testGetAllMilestonesTenMinusOneEntityManager()
    {
	MilestonesTO milestonesTO = MilestonesUtil.getAllMilestones(10L, -1L, em);
	
	assertEquals("Parameter error", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_ERROR, milestonesTO.getReturncode());	
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error MilestonesTO = getAllMilestones(10L, 10L, null)")
    final void testGetAllMilestonesTenTenNull()
    {
	MilestonesTO milestonesTO = MilestonesUtil.getAllMilestones(10L, 10L, null);
	
	assertEquals("Parameter error", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_ERROR, milestonesTO.getReturncode());	
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#getAllMilestones(java.lang.Long, java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("error MilestonesTO = getAllMilestones(10L, 10L, EntityManager)")
    final void testGetAllMilestonesTenTenEntityManager()
    {
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);
	milestone.setDescription("The plan is ready.");
	milestone.setNotifications(true);
	milestone.setName("MS1");
	milestone.setTodosFinished(new ArrayList<String>());

	MilestonesTO milestonesTO = MilestonesUtil.getAllMilestones(10L, 10L, em);
	milestonesTO.setMessage("Get a MilestoneDO test");
	milestonesTO.setReturncode(MilestonesUtil.RET_CODE_OK);
	milestonesTO.getResults().add(milestone);
	
	assertEquals("Get a MilestoneDO test", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_OK, milestonesTO.getReturncode());
	
	MilestoneDO resultMilestone = milestonesTO.getResults().get(0);
	assertEquals(new Long(1L), resultMilestone.getId());
	assertEquals("The plan is ready.", resultMilestone.getDescription());
	assertEquals(true, resultMilestone.getNotifications());
	assertEquals("MS1", resultMilestone.getName());
	assertTrue(resultMilestone.getTodosFinished().isEmpty());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#createMilestonesTO(java.lang.String, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("error MilestonesTO = createMilestonesTO(message, null)")
    final void testCreateMilestonesTONull()
    {
	MilestonesTO milestonesTO = MilestonesUtil.createMilestonesTO("", null);
	
	assertEquals("Parameter error", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_ERROR, milestonesTO.getReturncode());	
	assertTrue(milestonesTO.getResults().isEmpty());	
    }    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.MilestonesUtil#createMilestonesTO(java.lang.String, org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("MilestonesTO = createMilestonesTO(message, MilestoneDO)")
    final void testCreateMilestonesTO()
    {
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);
	milestone.setDescription("The plan is ready.");
	milestone.setNotifications(true);
	milestone.setName("MS1");
	milestone.setTodosFinished(new ArrayList<String>());

	MilestonesTO milestonesTO = MilestonesUtil.createMilestonesTO("Create a MilestoneDO test", milestone);
	
	assertEquals("Create a MilestoneDO test", milestonesTO.getMessage());
	assertEquals(MilestonesUtil.RET_CODE_OK, milestonesTO.getReturncode());
	
	MilestoneDO resultMilestone = milestonesTO.getResults().get(0);
	assertEquals(new Long(1L), resultMilestone.getId());
	assertEquals("The plan is ready.", resultMilestone.getDescription());
	assertEquals(true, resultMilestone.getNotifications());
	assertEquals("MS1", resultMilestone.getName());
	assertTrue(resultMilestone.getTodosFinished().isEmpty());
    }    
}
