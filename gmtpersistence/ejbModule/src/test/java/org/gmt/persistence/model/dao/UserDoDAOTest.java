/**
 * 
 */
package org.gmt.persistence.model.dao;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.dao.UserDoDAO;
import org.gmt.persistence.model.util.UsersTO;
import org.gmt.persistence.model.util.UsersUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;


/**
 * Unit test cases for the class org.gmt.persistence.model.dao.UserDoDAO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.dao.UserDoDAO")
class UserDoDAOTest
{
    @Mock
    private EntityManager em;
    private static UserDoDAO userDoDAOTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.UserDoDAO");
        userDoDAOTest = mock(UserDoDAO.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.dao.UserDoDAO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getInstance()}.
     */
    @Test
    @DisplayName("UserDoDAO = getInstance()")
    final void testGetInstance()
    {
	assertNotNull(UserDoDAO.getInstance());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getAllUsers(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<UserDO> = getAllUsers(null)")
    final void testGetAllUsersNull()
    {
	List<UserDO> usersList = new ArrayList<>();
	
	when(userDoDAOTest.getAllUsers(null)).thenReturn(usersList);
	
	assertNotNull(usersList);
	assertTrue(usersList.isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getAllUsers(javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<UserDO> = getAllUsers(EntityManager)")
    final void testGetAllUsersEntityManager()
    {
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Feuerstein");
	user.setLoginName("hfeuerstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);
	
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get all users test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);
		
	when(userDoDAOTest.getAllUsers(em)).thenReturn(usersList);

	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Feuerstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hfeuerstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<UserDO> = getUserId(null, null)")
    final void testGetUserByIdNullNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.getUserById(null, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getUserId(null, EntityManager)")
    final void testGetUserByIdNullEntityManager()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.getUserById(null, em)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getUserId(-1L, null)")
    final void testGetUserByIdMinusOneNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.getUserById(-1L, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<UserDO> = getUserId(-1L, EntityManager)")
    final void testGetUserByIdMinusOneEntityManager()
    {
	// Given
	UserDO user = new UserDO();	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.getUserById(-1L, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.getUserById(-1L, em), user);
	assertEquals("Get a user by id test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_ERROR, usersTO.getReturncode());
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getUserId(0L, null)")
    final void testGetUserByIdZeroNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.getUserById(0L, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<UserDO> = getUserId(0L, EntityManager)")
    final void testGetUserByIdZeroEntityManager()
    {
	// Given
	UserDO user = new UserDO();	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.getUserById(0L, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.getUserById(0L, em), user);
	assertEquals("Get a user by id test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_ERROR, usersTO.getReturncode());
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getUserId(1L, null)")
    final void testGetUserByIdOneNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.getUserById(1L, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<UserDO> = getUserId(1L, EntityManager)")
    final void testGetUserByIdOneEntityManager()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Blankenstein");
	user.setLoginName("hblankenstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);

	// When
	when(userDoDAOTest.getUserById(1L, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.getUserById(1L, em), user);
	assertEquals("Get a user by id test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_OK, usersTO.getReturncode());
	
	assertEquals(usersList, usersTO.getResults());	
	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Blankenstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hblankenstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = getUserId(10L, null)")
    final void testGetUserByIdTenNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.getUserById(10L, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#getUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("List<UserDO> = getUserId(10L, EntityManager)")
    final void testGetUserByIdTenEntityManager()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(10L);
	user.setFirstName("Herbert");
	user.setLastName("Blankenstein");
	user.setLoginName("hblankenstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);

	// When
	when(userDoDAOTest.getUserById(10L, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.getUserById(10L, em), user);
	assertEquals("Get a user by id test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_OK, usersTO.getReturncode());
	
	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(10L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Blankenstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hblankenstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#createUser(org.gmt.persistence.model.UserDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = createUser(null, null)")
    final void testCreateUserNullNull()
    {
	// Given			
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a user null, null test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);	

	// When
	when(userDoDAOTest.createUser(null, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#createUser(org.gmt.persistence.model.UserDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("UserDO = createUser(UserDO, null)")
    final void testCreateUserUserDONull()
    {
	// Given		
	UserDO user = new UserDO();
		
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a UserDO, null test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);

	// When
	when(userDoDAOTest.createUser(user, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#createUser(org.gmt.persistence.model.UserDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("UserDO = createUser(null, EntityManager)")
    final void testCreateUserNullEntityManager()
    {
	// Given			
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a UserDO, null test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);

	// When
	when(userDoDAOTest.createUser(null, em)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#createUser(org.gmt.persistence.model.UserDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("UserDO = createUser(UserDO, EntityManager)")
    final void testCreateUserUserDOEntityManager()
    {
	// Given		
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Feuerstein");
	user.setLoginName("hfeuerstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);
	
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a UserDO, null test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);	

	// When
	when(userDoDAOTest.createUser(user, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.createUser(user, em), user);
	assertEquals("Create a UserDO, null test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_OK, usersTO.getReturncode());
	assertFalse(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#updateUser(org.gmt.persistence.model.UserDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("UserDO = updateUser(Null, Null)")
    final void testUpdateUserNullNull()
    {
	// Given			
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a user null, null test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);	

	// When
	when(userDoDAOTest.updateUser(null, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#updateUser(org.gmt.persistence.model.UserDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("UserDO = updateUser(UserDO, null)")
    final void testUpdateUserUserDONull()
    {
	// Given		
	UserDO user = new UserDO();

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a UserDO, null test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);

	// When
	when(userDoDAOTest.updateUser(user, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#updateUser(org.gmt.persistence.model.UserDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("UserDO = updateUser(null, EntityManager)")
    final void testUpdateUserNullEntityManager()
    {
	// Given			
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a UserDO, null test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);

	// When
	when(userDoDAOTest.updateUser(null, em)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#updateUser(org.gmt.persistence.model.UserDO, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("UserDO = updateUser(UserDO, EntityManager)")
    final void testUpdateUserUserDOEntityManager()
    {	
	// Given		
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Feuerstein");
	user.setLoginName("hfeuerstein");
	user.setPassword("abcdef1234");

	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a UserDO, null test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);	

	// When
	when(userDoDAOTest.updateUser(user, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.updateUser(user, em), user);
	assertEquals("Create a UserDO, null test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_OK, usersTO.getReturncode());
	assertFalse(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(null, null)")
    final void testDeleteUserByIdNullNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.deleteUserById(null, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(null, EntityManager)")
    final void testDeleteUserByIdNullEntityManager()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.deleteUserById(null, em)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(-1L, null)")
    final void testDeleteUserByIdMinusOneNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.deleteUserById(-1L, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(-1L, EntityManager)")
    final void testDeleteUserByIdMinusOneEntityManager()
    {
	// Given
	UserDO user = new UserDO();	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.deleteUserById(-1L, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.deleteUserById(-1L, em), user);
	assertEquals("Get a user by id test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_ERROR, usersTO.getReturncode());
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(0L, null)")
    final void testDeleteUserByIdZeroNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.deleteUserById(0L, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(0L, EntityManager)")
    final void testDeleteUserByIdZeroEntityManager()
    {
	// Given
	UserDO user = new UserDO();	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.deleteUserById(0L, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.deleteUserById(0L, em), user);
	assertEquals("Get a user by id test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_ERROR, usersTO.getReturncode());
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(1L, null)")
    final void testDeleteUserByIdOneNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.deleteUserById(1L, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(1L, EntityManager)")
    final void testDeleteUserByIdOneEntityManager()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Blankenstein");
	user.setLoginName("hblankenstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);

	// When
	when(userDoDAOTest.deleteUserById(1L, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.deleteUserById(1L, em), user);
	assertEquals("Get a user by id test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_OK, usersTO.getReturncode());
	
	assertEquals(usersList, usersTO.getResults());	
	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Blankenstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hblankenstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(10L, null)")
    final void testDeleteUserByIdTenNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_ERROR);

	// When
	when(userDoDAOTest.deleteUserById(10L, null)).thenReturn(null);

	// Then
	assertTrue(usersTO.getResults().isEmpty()); 
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.dao.UserDoDAO#deleteUserById(java.lang.Long, javax.persistence.EntityManager)}.
     */
    @Test
    @DisplayName("null = deleteUserById(10L, EntityManager)")
    final void testDeleteUserByIdTenEntityManager()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(10L);
	user.setFirstName("Herbert");
	user.setLastName("Blankenstein");
	user.setLoginName("hblankenstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id test");
	usersTO.setReturncode(UsersUtil.RET_CODE_OK);
	usersTO.getResults().addAll(usersList);

	// When
	when(userDoDAOTest.deleteUserById(10L, em)).thenReturn(user);

	// Then
	assertEquals(userDoDAOTest.deleteUserById(10L, em), user);
	assertEquals("Get a user by id test", usersTO.getMessage());
	assertEquals(UsersUtil.RET_CODE_OK, usersTO.getReturncode());
	
	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(10L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Blankenstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hblankenstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword()); 
    }
}
