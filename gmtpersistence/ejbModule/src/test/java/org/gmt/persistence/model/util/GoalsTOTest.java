/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.GoalsTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.GoalsTO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.GoalsTO")
class GoalsTOTest
{
    private static GoalsTO goalsToTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.GoalsTO");
        goalsToTest = new GoalsTO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.GoalsTO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getMessage()}.
     */
    @Test
    @DisplayName("String = getMessage()/setMessage()")
    final void testGetSetMessage()
    {
        goalsToTest.setMessage("This is a test!");
	assertEquals("This is a test!", goalsToTest.getMessage());
	
	goalsToTest.setMessage("");
	assertEquals("", goalsToTest.getMessage());
	
	goalsToTest.setMessage("ÄöÜ");
	assertEquals("ÄöÜ", goalsToTest.getMessage());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getReturncode()}.
     */
    @Test
    @DisplayName("Integer = getReturncode()/setReturncode()")
    final void testGetSetReturncode()
    {
        goalsToTest.setReturncode(-1);
        assertEquals(-1, goalsToTest.getReturncode().intValue());
        
        goalsToTest.setReturncode(0);
        assertEquals(0, goalsToTest.getReturncode().intValue());
        
        goalsToTest.setReturncode(1);
        assertEquals(1, goalsToTest.getReturncode().intValue());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getResults()}.
     */
    @Test
    @DisplayName("List<GoalDO> = getResults()")
    final void testGetResults()
    {
	assertTrue(goalsToTest.getResults().isEmpty());
	
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	goal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	goal.setEndDate(LocalDateTime.parse("2019-02-01T08:00"));
	goal.setCategory(Category.HOBBY);
	goal.setDescription("Make a car from wood.");
	goal.setShortTerm("Woodcar");
	goal.setNotifications(true);
	goal.setStatus(Status.DEFINED);
	goal.setSubGoals(new ArrayList<SubGoalDO>());
	
	goalsToTest.getResults().add(goal);
	
	GoalDO resultGoal = goalsToTest.getResults().get(0);
	assertEquals(new Long(1L), resultGoal.getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), resultGoal.getBeginDate());
	assertEquals(LocalDateTime.parse("2019-02-01T08:00"), resultGoal.getEndDate());
	assertEquals(Category.HOBBY, resultGoal.getCategory());
	assertEquals("Make a car from wood.", resultGoal.getDescription());
	assertEquals("Woodcar", resultGoal.getShortTerm());
	assertEquals(true, resultGoal.getNotifications());
	assertEquals(Status.DEFINED, resultGoal.getStatus());
	assertTrue(resultGoal.getSubGoals().isEmpty());
    }
}
