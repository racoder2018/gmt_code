/**
 * 
 */
package org.gmt.persistence.model.util;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.util.SubGoalsTO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.util.SubGoalsTO")
class SubGoalsTOTest
{
    private static SubGoalsTO subGoalsToTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.SubGoalsTO");
        subGoalsToTest = new SubGoalsTO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.util.SubGoalsTO completed.");
    }
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getMessage()}.
     */
    @Test
    @DisplayName("String = getMessage()/setMessage()")
    final void testGetSetMessage()
    {
        subGoalsToTest.setMessage("This is a test!");
	assertEquals("This is a test!", subGoalsToTest.getMessage());
	
	subGoalsToTest.setMessage("");
	assertEquals("", subGoalsToTest.getMessage());
	
	subGoalsToTest.setMessage("ÄöÜ");
	assertEquals("ÄöÜ", subGoalsToTest.getMessage());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getReturncode()}.
     */
    @Test
    @DisplayName("Integer = getReturncode()/setReturncode()")
    final void testGetSetReturncode()
    {
        subGoalsToTest.setReturncode(-1);
        assertEquals(-1, subGoalsToTest.getReturncode().intValue());
        
        subGoalsToTest.setReturncode(0);
        assertEquals(0, subGoalsToTest.getReturncode().intValue());
        
        subGoalsToTest.setReturncode(1);
        assertEquals(1, subGoalsToTest.getReturncode().intValue());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.util.GoalsTO#getResults()}.
     */
    @Test
    @DisplayName("List<SubGoalDO> = getResults()")
    final void testGetResults()
    {
	assertTrue(subGoalsToTest.getResults().isEmpty());
	
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	subGoal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	subGoal.setEndDate(LocalDateTime.parse("2019-01-01T09:00"));
	subGoal.setDescription("Get a plan.");
	subGoal.setShortTerm("Planning");
	subGoal.setNotifications(true);
	subGoal.setMilestones(new ArrayList<MilestoneDO>());
	
	subGoalsToTest.getResults().add(subGoal);
	
	SubGoalDO resultSubGoal = subGoalsToTest.getResults().get(0);
	assertEquals(new Long(1L), resultSubGoal.getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), resultSubGoal.getBeginDate());
	assertEquals(LocalDateTime.parse("2019-01-01T09:00"), resultSubGoal.getEndDate());
	assertEquals("Get a plan.", resultSubGoal.getDescription());
	assertEquals("Planning", resultSubGoal.getShortTerm());
	assertEquals(true, resultSubGoal.getNotifications());
	assertTrue(resultSubGoal.getMilestones().isEmpty());	
    }
}
