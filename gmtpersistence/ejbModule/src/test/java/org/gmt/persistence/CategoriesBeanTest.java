/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.CategoriesBean;
import org.gmt.persistence.model.CategoryDO;
import org.gmt.persistence.model.util.CategoriesTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.CategoriesBean
 *    
 * RF: -for all tests with Long parameters we could write some Integer param tests
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.CategoriesBean")
class CategoriesBeanTest
{
    private static CategoriesBean categoriesBeanTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.CategoriesBean");
        categoriesBeanTest = mock(CategoriesBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.CategoriesBean completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.CategoriesBean#CategoriesBean()}.
     */
    @Test
    @DisplayName("not null = getClass()")
    final void testCategoriesBean()
    {
	assertNotNull(categoriesBeanTest.getClass());
    }

    /**
     * Test method for {@link org.gmt.persistence.CategoriesBean#getAllCategories()}.
     */
    @Test
    @DisplayName("empty CategoriesTO = getAllCategories()")
    final void testGetAllCategoriesRetEmptyCategoriesTO()
    {
	// Given
	List<CategoryDO> categoriesList = new ArrayList<>();
	CategoriesTO categoriesTO = new CategoriesTO();

	// When
	when(categoriesBeanTest.getAllCategories()).thenReturn(categoriesTO);

	// Then
	assertEquals(categoriesBeanTest.getAllCategories(), categoriesTO);
	assertNull(categoriesTO.getMessage());
	assertNull(categoriesTO.getReturncode());
	assertEquals(categoriesList, categoriesTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.CategoriesBean#getAllCategories()}.
     */
    @Test
    @DisplayName("CategoriesTO = getAllCategories()")
    final void testGetAllCategoriesRetCategoriesTO()
    {
	// Given
	List<CategoryDO> categoriesList = new ArrayList<>();
	CategoriesTO categoriesTO = new CategoriesTO();

	// When
	when(categoriesBeanTest.getAllCategories()).thenReturn(categoriesTO);

	// Then
	assertEquals(categoriesBeanTest.getAllCategories(), categoriesTO);
	assertNull(categoriesTO.getMessage());
	assertNull(categoriesTO.getReturncode());
	assertEquals(categoriesList, categoriesTO.getResults());
    }
}
