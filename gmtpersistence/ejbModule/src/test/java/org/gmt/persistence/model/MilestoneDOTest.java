/**
 * 
 */
package org.gmt.persistence.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.SubGoalDO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.MilestoneDO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.MilestoneDO")
class MilestoneDOTest
{
    private static MilestoneDO milestoneDOTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.MilestoneDO");
        milestoneDOTest = new MilestoneDO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.MilestoneDO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.MilestoneDO#getSubGoal()/setSubGoal()}.
     */
    @Test
    @DisplayName("SubGoalDO = getSubGoal()/setSubGoal()")
    final void testGetSetSubGoal()
    {
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(200L);
		
	milestoneDOTest.setSubGoal(subGoal);
	assertEquals(subGoal, milestoneDOTest.getSubGoal());
	
	milestoneDOTest.setSubGoal(null);
	assertNull(milestoneDOTest.getSubGoal());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.MilestoneDO#getId()/setId()}.
     */
    @Test
    @DisplayName("Long = getId()/setId()")
    final void testGetSetId()
    {
	milestoneDOTest.setId(10L);
	assertEquals(new Long(10L), milestoneDOTest.getId());

	milestoneDOTest.setId(-1L);
	assertEquals(new Long(-1L), milestoneDOTest.getId());

	milestoneDOTest.setId(0L);
	assertEquals(new Long(0L), milestoneDOTest.getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.MilestoneDO#getName()/setName()}.
     */
    @Test
    @DisplayName("String = getName()/setName()")
    final void testGetSetName()
    {
	milestoneDOTest.setName("Milestone One");
	assertEquals("Milestone One", milestoneDOTest.getName());
	
	milestoneDOTest.setName("Milestone 12345Äß");
	assertEquals("Milestone 12345Äß", milestoneDOTest.getName());
	
	milestoneDOTest.setName(null);
	assertNull(milestoneDOTest.getName());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.MilestoneDO#getDescription()/setDescription()}.
     */
    @Test
    @DisplayName("String = getDescription()/setDescription()")
    final void testGetSetDescription()
    {
	milestoneDOTest.setDescription("This is Milestone One");
	assertEquals("This is Milestone One", milestoneDOTest.getDescription());
	
	milestoneDOTest.setDescription("This is Milestone 12345Äß");
	assertEquals("This is Milestone 12345Äß", milestoneDOTest.getDescription());
	
	milestoneDOTest.setDescription(null);
	assertNull(milestoneDOTest.getDescription());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.MilestoneDO#getTodosFinished()/setTodosFinished()}.
     */
    @Test
    @DisplayName("? = getTodosFinished()/setTodosFinished()")
    final void testGetSetTodosFinished()
    {
	List<String> todosFinished = new ArrayList<String>();
	todosFinished.add("Book read");
	todosFinished.add("Practices done");
	
	milestoneDOTest.setTodosFinished(todosFinished);
	assertEquals("Book read", milestoneDOTest.getTodosFinished().get(0));
	assertEquals("Practices done", milestoneDOTest.getTodosFinished().get(1));
    }

    /**
     * Test method for {@link org.gmt.persistence.model.MilestoneDO#getNotifications()/setNotifications()}.
     */
    @Test
    @DisplayName("Boolean = getNotifications()/setNotifications()")
    final void testGetSetNotifications()
    {
	milestoneDOTest.setNotifications(true);
	assertTrue(milestoneDOTest.getNotifications());
	
	milestoneDOTest.setNotifications(false);
	assertFalse(milestoneDOTest.getNotifications());
    }
}
