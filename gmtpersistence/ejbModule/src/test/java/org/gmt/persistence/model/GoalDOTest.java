/**
 * 
 */
package org.gmt.persistence.model;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.model.GoalDO
 * 
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.model.GoalDO")
class GoalDOTest
{
    private static GoalDO goalDOTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.GoalDO");
        goalDOTest = new GoalDO();
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.model.GoalDO completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.model.GoalDO#getId()/setId()}.
     */
    @Test
    @DisplayName("Long = getId()/setId()")
    final void testGetSetId()
    {
	goalDOTest.setId(10L);
	assertEquals(new Long(10L), goalDOTest.getId());

	goalDOTest.setId(-1L);
	assertEquals(new Long(-1L), goalDOTest.getId());

	goalDOTest.setId(0L);
	assertEquals(new Long(0L), goalDOTest.getId());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.GoalDO#getStatus()/setStatus()()}.
     */
    @Test
    @DisplayName("Status = getStatus()/setStatus()")
    final void testGetSetStatus()
    {
	goalDOTest.setStatus(Status.RUNNING);
	assertEquals(Status.RUNNING, goalDOTest.getStatus());
	
	goalDOTest.setStatus(null);
	assertNull(goalDOTest.getStatus());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.GoalDO#getCategory()/setCategory()}.
     */
    @Test
    @DisplayName("Category = getCategory()/setCategory()")
    final void testGetSetCategory()
    {
	goalDOTest.setCategory(Category.FAMILY);
	assertEquals(Category.FAMILY, goalDOTest.getCategory());
	
	goalDOTest.setCategory(null);
	assertNull(goalDOTest.getCategory());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.GoalDO#getSubGoals()/setSubGoals()}.
     */
    @Test
    @DisplayName("Long = getSubGoals()/setSubGoals()")
    final void testGetSetSubGoals()
    {
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(200L);
	
	List<SubGoalDO> subGoals = new ArrayList<>();
	subGoals.add(subGoal);
	
	goalDOTest.setSubGoals(subGoals);
	assertEquals(new Long(200L), goalDOTest.getSubGoals().get(0).getId());
	
	goalDOTest.setSubGoals(null);
	assertNull(goalDOTest.getSubGoals());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.model.GoalDO#getShortTerm()/setShortTerm}.
     */
    @Test
    @DisplayName("String = getShortTerm()/setShortTerm()")
    final void testGetSetShortTerm()
    {
	goalDOTest.setShortTerm("Testgoal One");
	assertEquals("Testgoal One", goalDOTest.getShortTerm());
	
	goalDOTest.setShortTerm("Testgoal One 12345Äß");
	assertEquals("Testgoal One 12345Äß", goalDOTest.getShortTerm());
	
	goalDOTest.setShortTerm(null);
	assertNull(goalDOTest.getShortTerm());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.GoalDO#getDescription()/setDescription()}.
     */
    @Test
    @DisplayName("String = getDescription()/setDescription()")
    final void testGetSetDescription()
    {
	goalDOTest.setDescription("Testgoal One");
	assertEquals("Testgoal One", goalDOTest.getDescription());
	
	goalDOTest.setDescription("Testgoal One 12345Äß");
	assertEquals("Testgoal One 12345Äß", goalDOTest.getDescription());
	
	goalDOTest.setDescription(null);
	assertNull(goalDOTest.getDescription());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.GoalDO#getBeginDate()/setBeginDate()}.
     */
    @Test
    @DisplayName("String = getBeginDate()/setBeginDate()")
    final void testGetSetBeginDate()
    {
	goalDOTest.setBeginDate(LocalDateTime.parse("2019-04-01T23:00:00"));
	assertEquals(LocalDateTime.parse("2019-04-01T23:00:00"), goalDOTest.getBeginDate());
	
	goalDOTest.setBeginDate(null);
	assertNull(goalDOTest.getBeginDate());
    }

    /**
     * Test method for {@link org.gmt.persistence.model.GoalDO#getEndDate()/setEndDate()}.
     */
    @Test
    @DisplayName("String = getEndDate()/setEndDate()")
    final void testGetSetEndDate()
    {
	goalDOTest.setEndDate(LocalDateTime.parse("2019-04-01T23:00:00"));
	assertEquals(LocalDateTime.parse("2019-04-01T23:00:00"), goalDOTest.getEndDate());
	
	goalDOTest.setEndDate(null);
	assertNull(goalDOTest.getEndDate());
    }
}
