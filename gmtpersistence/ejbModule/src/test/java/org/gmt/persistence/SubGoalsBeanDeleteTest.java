/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.SubGoalsBean;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.SubGoalsBean
 * Method: deleteSubGoalById()
 *  
 * RF: -for all tests with Long parameters we could write some Integer param tests
 *
 * @author Ralf Mehle 
 */
@DisplayName("TestCases for org.gmt.persistence.SubGoalsBean, method deleteSubGoalById()")
class SubGoalsBeanDeleteTest
{
    private static SubGoalsBean subGoalsBeanDeleteTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.SubGoalsBean, "
        	+ "method deleteSubGoalById()");
        subGoalsBeanDeleteTest = mock(SubGoalsBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.SubGoalsBean, "
        	+ "method deleteSubGoalById() completed.");
    }  
    
    
   /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(null, null)")
    final void testDeleteSubGoalByIdNullNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal null, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(null, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(null, null), subGoalsTO);
	assertEquals("Delete a subgoal null, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(null, -1L)")
    final void testDeleteSubGoalByIdNullMinusOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal null, -1L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(null, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(null, -1L), subGoalsTO);
	assertEquals("Delete a subgoal null, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(null, 0L)")
    final void testDeleteSubGoalByIdNullZero()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal null, 0L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(null, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(null, 0L), subGoalsTO);
	assertEquals("Delete a subgoal null, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(null, 1L)")
    final void testDeleteSubGoalByIdNullOne()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal by null, 1L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanDeleteTest.getSubGoalById(null, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.getSubGoalById(null, 1L), subGoalsTO);
	assertEquals("Delete a subgoal by null, 1L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(null, 10L)")
    final void testDeleteSubGoalByIdNullTen()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal by null, 10L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanDeleteTest.getSubGoalById(null, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.getSubGoalById(null, 10L), subGoalsTO);
	assertEquals("Delete a subgoal by null, 10L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(-1L, null)")
    final void testDeleteSubGoalByIdMinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal -1L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, null), subGoalsTO);
	assertEquals("Delete a subgoal -1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(-1L, -1L)")
    final void testDeleteSubGoalByIdMinusOneMinusOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal -1L, -1L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, -1L), subGoalsTO);
	assertEquals("Delete a subgoal -1L, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(-1L, 0L)")
    final void testDeleteSubGoalByIdMinusOneZero()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal -1L, 0L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, 0L), subGoalsTO);
	assertEquals("Delete a subgoal -1L, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(-1L, 1L)")
    final void testDeleteSubGoalByIdMinusOneOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal -1L, 1L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, 1L), subGoalsTO);
	assertEquals("Delete a subgoal -1L, 1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(-1L, 10L)")
    final void testDeleteSubGoalByIdMinusOneTen()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal -1L, 10L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(-1L, 10L), subGoalsTO);
	assertEquals("Delete a subgoal -1L, 10L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(0L, null)")
    final void testDeleteSubGoalByIdZeroNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 0L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(0L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(0L, null), subGoalsTO);
	assertEquals("Delete a subgoal 0L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(0L, -1L)")
    final void testDeleteSubGoalByIdZeroMinusOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 0L, -1L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(0L, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(0L, -1L), subGoalsTO);
	assertEquals("Delete a subgoal 0L, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(0L, 0L)")
    final void testDeleteSubGoalByIdZeroZero()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 0L, 0L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(0L, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(0L, 0L), subGoalsTO);
	assertEquals("Delete a subgoal 0L, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(0L, 1L)")
    final void testDeleteSubGoalByIdZeroOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 0L, 1L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(0L, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(0L, 1L), subGoalsTO);
	assertEquals("Delete a subgoal 0L, 1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(0L, 10L)")
    final void testDeleteSubGoalByIdZeroTen()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 0L, 10L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(0L, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(0L, 10L), subGoalsTO);
	assertEquals("Delete a subgoal 0L, 10L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(1L, null)")
    final void testDeleteSubGoalByIdOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 1L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(1L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(1L, null), subGoalsTO);
	assertEquals("Delete a subgoal 1L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(1L, -1L)")
    final void testDeleteSubGoalByIdOneMinusOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 1L, -1L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(1L, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(1L, -1L), subGoalsTO);
	assertEquals("Delete a subgoal 1L, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(1L, 0L)")
    final void testDeleteSubGoalByIdOneZero()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 1L, 0L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(1L, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(1L, 0L), subGoalsTO);
	assertEquals("Delete a subgoal 1L, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(1L, 1L)")
    final void testDeleteSubGoalByIdOneOne()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal by 1L, 1L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanDeleteTest.getSubGoalById(1L, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.getSubGoalById(1L, 1L), subGoalsTO);
	assertEquals("Delete a subgoal by 1L, 1L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(1L, 10L)")
    final void testDeleteSubGoalByIdOneTen()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal by 1L, 10L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanDeleteTest.getSubGoalById(1L, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.getSubGoalById(1L, 10L), subGoalsTO);
	assertEquals("Delete a subgoal by 1L, 10L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(10L, null)")
    final void testDeleteSubGoalByIdTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 10L, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(10L, null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(10L, null), subGoalsTO);
	assertEquals("Delete a subgoal 10L, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(10L, -1L)")
    final void testDeleteSubGoalByIdTenMinusOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 10L, -1L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(10L, -1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(10L, -1L), subGoalsTO);
	assertEquals("Delete a subgoal 10L, -1L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(10L, 0L)")
    final void testDeleteSubGoalByIdTenZero()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal 10L, 0L test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsBeanDeleteTest.deleteSubGoalById(10L, 0L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.deleteSubGoalById(10L, 0L), subGoalsTO);
	assertEquals("Delete a subgoal 10L, 0L test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());		
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(10L, 1L)")
    final void testDeleteSubGoalByIdTenOne()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal by 10L, 1L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanDeleteTest.getSubGoalById(10L, 1L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.getSubGoalById(10L, 1L), subGoalsTO);
	assertEquals("Delete a subgoal by 10L, 1L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.SubGoalsBean#deleteSubGoalById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(10L, 10L)")
    final void testDeleteSubGoalByIdTenTen()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal by 10L, 10L test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsBeanDeleteTest.getSubGoalById(10L, 10L)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsBeanDeleteTest.getSubGoalById(10L, 10L), subGoalsTO);
	assertEquals("Delete a subgoal by 10L, 10L test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());	
    }
}
