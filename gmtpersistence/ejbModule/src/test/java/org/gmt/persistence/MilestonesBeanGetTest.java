/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.MilestonesBean;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.MilestonesBean
 * Methods: getAllMilestones(), getMilestoneById()
 * 
 * Note: Because of the 3 method parameters of the method getMilestoneById() 
 * 	 and the resulting test combinations for the tests 2 values for the 
 * 	 first and second parameter are used and 5 for the third parameter:
 *       goalId:    -1L, 10L
 *       subGoalId: -1L, 10L 
 *       id:        null, -1L, 0L, 1L, 10L 
 *       
 * RF: -for all tests with Long parameters we could write some Integer param tests
 *  
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.persistence.MilestonesBean, "
	+ "methods getAllMilestones(), getMilestoneById()")
class MilestonesBeanGetTest
{
    private static MilestonesBean milestonesBeanGetTest;

    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.MilestonesBean, "
        	+ "methods getAllMilestones(), getMilestoneById()");
        milestonesBeanGetTest = mock(MilestonesBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.MilestonesBean, "
        	+ "methods getAllMilestones(), getMilestoneById() completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#MilestonesBean()}.
     */
    @Test
    @DisplayName("not null = getClass()")
    final void testMilestonesBean()
    {
	assertNotNull(milestonesBeanGetTest.getClass());
    }

    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(null, null)")
    final void testGetAllMilestonesNullNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(null, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(null, null), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(-1L, null)")
    final void testGetAllMilestonesMinusOneNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(-1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(-1L, null), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(0L, null)")
    final void testGetAllMilestonesZeroNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(0L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(0L, null), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(1L, null)")
    final void testGetAllMilestonesOneNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(1L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(1L, null), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(10L, null)")
    final void testGetAllMilestonesTenNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(10L, null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(10L, null), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(null, -1L)")
    final void testGetAllMilestonesNullMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(null, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(null, -1L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(-1L, -1L)")
    final void testGetAllMilestonesMinusOneMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(-1L, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(-1L, -1L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(0L, -1L)")
    final void testGetAllMilestonesZeroMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(0L, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(0L, -1L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(1L, -1L)")
    final void testGetAllMilestonesOneMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(1L, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(1L, -1L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(10L, -1L)")
    final void testGetAllMilestonesTenMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(10L, -1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(10L, -1L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(null, 0L)")
    final void testGetAllMilestonesNullZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(null, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(null, 0L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(-1L, 0L)")
    final void testGetAllMilestonesMinusOneZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(-1L, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(-1L, 0L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(0L, 0L)")
    final void testGetAllMilestonesZeroZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(0L, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(0L, 0L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(1L, 0L)")
    final void testGetAllMilestonesOneZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(1L, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(1L, 0L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(10L, 0L)")
    final void testGetAllMilestonesTenZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(10L, 0L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(10L, 0L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(null, 1L)")
    final void testGetAllMilestonesNullOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(null, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(null, 1L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(-1L, 1L)")
    final void testGetAllMilestonesMinusOneOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(-1L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(-1L, 1L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(0L, 1L)")
    final void testGetAllMilestonesZeroOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(0L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(0L, 1L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("MilestonesTO = getAllMilestones(1L, 1L)")
    final void testGetAllMilestonesOneOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getAllMilestones(1L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(1L, 1L), milestonesTO);
	assertEquals("Get all milestones test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("MilestonesTO = getAllMilestones(10L, 1L)")
    final void testGetAllMilestonesTenOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getAllMilestones(10L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(10L, 1L), milestonesTO);
	assertEquals("Get all milestones test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(null, 10L)")
    final void testGetAllMilestonesNullTen()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(null, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(null, 10L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(-1L, 10L)")
    final void testGetAllMilestonesMinusOneTen()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(-1L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(-1L, 10L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestones(0L, 10L)")
    final void testGetAllMilestonesZeroTen()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();

	// When
	when(milestonesBeanGetTest.getAllMilestones(0L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(0L, 10L), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("MilestonesTO = getAllMilestones(1L, 10L)")
    final void testGetAllMilestonesOneTen()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getAllMilestones(1L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(1L, 10L), milestonesTO);
	assertEquals("Get all milestones test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(milestonesList, milestonesTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getAllMilestones(java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("MilestonesTO = getAllMilestones(10L, 10L)")
    final void testGetAllMilestonesTenTen()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getAllMilestones(10L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getAllMilestones(10L, 10L), milestonesTO);
	assertEquals("Get all milestones test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(milestonesList, milestonesTO.getResults());	
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, -1L, null)")
    final void testGetMilestoneByIdMinusOneMinusOnelNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, -1L, null test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, -1L, null)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, -1L, null), milestonesTO);
	assertEquals("Get a milestone by id -1L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, -1L, -1L)")
    final void testGetMilestoneByIdMinusOneMinusOneMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, -1L, -1L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, -1L, -1L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, -1L, -1L), milestonesTO);
	assertEquals("Get a milestone by id -1L, -1L, -1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, -1L, 0L)")
    final void testGetMilestoneByIdMinusOneMinusOneZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, -1L, 0L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, -1L, 0L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, -1L, 0L), milestonesTO);
	assertEquals("Get a milestone by id -1L, -1L, 0L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, -1L, 1L)")
    final void testGetMilestoneByIdMinusOneMinusOneOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, -1L, 1L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, -1L, 1L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, -1L, 1L), milestonesTO);
	assertEquals("Get a milestone by id -1L, -1L, 1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, -1L, 10L)")
    final void testGetMilestoneByIdMinusOneMinusOneTen()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, -1L, 10L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, -1L, 10L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, -1L, 10L), milestonesTO);
	assertEquals("Get a milestone by id -1L, -1L, 10L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, 10L, null)")
    final void testGetMilestoneByIdMinusOneTenNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, 10L, null test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, 10L, null)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, 10L, null), milestonesTO);
	assertEquals("Get a milestone by id -1L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, 10L, -1L)")
    final void testGetMilestoneByIdMinusOneTenMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, 10L, -1L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, 10L, -1L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, 10L, -1L), milestonesTO);
	assertEquals("Get a milestone by id -1L, 10L, -1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, 10L, 0L)")
    final void testGetMilestoneByIdMinusOneTenZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, 10L, 0L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, 10L, 0L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, 10L, 0L), milestonesTO);
	assertEquals("Get a milestone by id -1L, 10L, 0L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, 10L, 1L)")
    final void testGetMilestoneByIdMinusOneTenOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, 10L, 1L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, 10L, 1L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, 10L, 1L), milestonesTO);
	assertEquals("Get a milestone by id -1L, 10L, 1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(-1L, 10L, 10L)")
    final void testGetMilestoneByIdMinusOneTenTen()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id -1L, 10L, 10L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(-1L, 10L, 10L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(-1L, 10L, 10L), milestonesTO);
	assertEquals("Get a milestone by id -1L, 10L, 10L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, -1L, null)")
    final void testGetMilestoneByIdTenMinusOneNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, -1L, null test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, -1L, null)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, -1L, null), milestonesTO);
	assertEquals("Get a milestone by id 10L, -1L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, -1L, -1L)")
    final void testGetMilestoneByIdTenMinusOneMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, -1L, -1L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, -1L, -1L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, -1L, -1L), milestonesTO);
	assertEquals("Get a milestone by id 10L, -1L, -1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, -1L, 0L)")
    final void testGetMilestoneByIdTenMinusOneZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, -1L, 0L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, -1L, 0L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, -1L, 0L), milestonesTO);
	assertEquals("Get a milestone by id 10L, -1L, 0L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, -1L, 1L)")
    final void testGetMilestoneByIdTenMinusOneOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, -1L, 1L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, -1L, 1L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, -1L, 1L), milestonesTO);
	assertEquals("Get a milestone by id 10L, -1L, 1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, -1L, 10L)")
    final void testGetMilestoneByIdTenMinusOneTen()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, -1L, 10L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, -1L, 10L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, -1L, 10L), milestonesTO);
	assertEquals("Get a milestone by id 10L, -1L, 10L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, 10L, null)")
    final void testGetMilestoneByIdTenTenNull()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, 10L, null test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, 10L, null)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, 10L, null), milestonesTO);
	assertEquals("Get a milestone by id 10L, 10L, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, 10L, -1L)")
    final void testGetMilestoneByIdTenTenNullMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, 10L, -1L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, 10L, -1L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, 10L, -1L), milestonesTO);
	assertEquals("Get a milestone by id 10L, 10L, -1L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, 10L, 0L)")
    final void testGetMilestoneByIdTenTenZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, 10L, 0L test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, 10L, 0L)
		).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, 10L, 0L), milestonesTO);
	assertEquals("Get a milestone by id 10L, 10L, 0L test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, 10L, 1L)")
    final void testGetMilestoneByIdTenTenOne()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);

	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, 10L, 1L test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, 10L, 1L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, 10L, 1L), milestonesTO);
	assertEquals("Get a milestone by id 10L, 10L, 1L test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(1L), milestonesTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.MilestonesBean#getMilestoneById(java.lang.Long, java.lang.Long, java.lang.Long)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(10L, 10L, 10L)")
    final void testGetMilestoneByIdTenTenTen()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(10L);

	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10L, 10L, 10L test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesBeanGetTest.getMilestoneById(10L, 10L, 10L)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesBeanGetTest.getMilestoneById(10L, 10L, 10L), milestonesTO);
	assertEquals("Get a milestone by id 10L, 10L, 10L test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(10L), milestonesTO.getResults().get(0).getId());
    }
}
