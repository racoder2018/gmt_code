/**
 * 
 */
package org.gmt.persistence;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.StatusBean;
import org.gmt.persistence.model.StatusDO;
import org.gmt.persistence.model.util.StatusTO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.persistence.StatusBean
 * 
 * RF: -for all tests with Long parameters we could write some Integer param tests
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.persistence.StatusBean")
class StatusBeanTest
{
  private static StatusBean statusBeanTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.StatusBean");
        statusBeanTest = mock(StatusBean.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.persistence.StatusBean completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.persistence.StatusBean#StatusBean()}.
     */
    @Test
    @DisplayName("not null = getClass()")
    final void testStatusBean()
    {
	assertNotNull(statusBeanTest.getClass());
    }
   
    /**
     * Test method for {@link org.gmt.persistence.StatusBean#getAllStatus()}.
     */
    @Test
    @DisplayName("empty StatusTO = getAllStatus()")
    final void testGetAllStatusListRetEmptyStatusTO()
    {
	// Given
	List<StatusDO> statusList = new ArrayList<>();
	StatusTO statusTO = new StatusTO();

	// When
	when(statusBeanTest.getAllStatus()).thenReturn(statusTO);

	// Then
	assertEquals(statusBeanTest.getAllStatus(), statusTO);
	assertNull(statusTO.getMessage());
	assertNull(statusTO.getReturncode());
	assertEquals(statusList, statusTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.persistence.StatusBean#getAllStatus()}.
     */
    @Test
    @DisplayName("StatusTO = getAllStatus()")
    final void testGetAllStatusRetStatusTO()
    {
	// Given
	List<StatusDO> statusList = new ArrayList<>();
	StatusTO statusTO = new StatusTO();

	// When
	when(statusBeanTest.getAllStatus()).thenReturn(statusTO);

	// Then
	assertEquals(statusBeanTest.getAllStatus(), statusTO);
	assertNull(statusTO.getMessage());
	assertNull(statusTO.getReturncode());
	assertEquals(statusList, statusTO.getResults());
    }
}
