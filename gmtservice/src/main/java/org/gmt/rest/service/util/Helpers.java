/**
 * 
 */
package org.gmt.rest.service.util;

import org.gmt.persistence.model.Status;


/**
 * Helper class for different methods for conversion, parsing, etc.
 * 
 * @author Ralf Mehle
 */
public class Helpers
{
    /**
     * Converts a ID as String to a Long value. 
     * 
     * @param 	id	the id as string being converted to long
     * @return 	the String id as Long value
     */
    public static Long stringToLong(String id)
    {
	if( !isEmpty( id ) ) 
	{
	    try 
	    { 
		return new Long( id.trim() ); 
	    } 
	    catch( NumberFormatException nfe ) 
	    {
		System.err.println("The string cannot converted correctly to a Long. " + 
			nfe.getMessage());
		nfe.getStackTrace();
	    }
	}
	return null;
    }
    
    /**
     * Converts a ID as String to a Integer value. 
     * 
     * @param 	id	the id as string being converted to int 
     * @return 	the String id as Integer value
     */
    public static Integer stringToInt(String id)
    {
	if( !isEmpty( id ) ) 
	{
	    try 
	    { 
		return new Integer( id.trim() ); 
	    } 
	    catch( NumberFormatException nfe ) 
	    {
		System.err.println("The string cannot converted correctly to a Integer. " + 
			nfe.getMessage());
		nfe.getStackTrace();
	    }
	}
	return null;
    }

    /**
     * Converts a String status to a Status status
     * 
     * @param	statusStr	the status as string being converted to status object
     * @return	status		the status object
     */
    public static Status stringToStatus(String statusStr)
    {
	if (statusStr == null) return null;
	
	Status status = Status.UNKNOWN;
	if (statusStr.equals("DEFINED"))
	    status = Status.DEFINED;
	if (statusStr.equals("RUNNING"))
	    status = Status.RUNNING;
	if (statusStr.equals("ACHIEVED"))
	    status = Status.ACHIEVED;
	
	return status;
    }
    
    /**
     * Checks the String id being empty or not.
     * 
     * @param	id		the id to be checked
     * @return	true|false	indication of empty string
     */
    public static boolean isEmpty(String id)
    {
	return id == null || id.trim().length() <= 0;
    }
}
