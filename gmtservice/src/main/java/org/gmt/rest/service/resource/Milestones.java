/**
 * 
 */
package org.gmt.rest.service.resource;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.gmt.persistence.MilestonesBean;
import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.gmt.rest.service.util.Helpers;


/**
 *  Class handling the REST resource "/goals/{id}/subgoals/{id}/milestones". 
 *  A milestone always belongs to a subgoal and cannot exist alone.
 * 
 * - Implementation of the REST methods GET and POST for
 *   "/goals/{id}/subgoals/{id}/milestones"
 * - Implementation of the REST methods GET, PUT and DELETE for 
 *   "/goals/{id}/subgoals/{id}/milestones/{id}"
 * 
 * @author Ralf Mehle
 */
@Path( "/goals/{goalid}/subgoals/{subgoalid}/milestones" )
@Produces( MediaType.APPLICATION_JSON )
public class Milestones
{
    private @PathParam("goalid") String goalId;   	// get id from goal in path
    private @PathParam("subgoalid") String subGoalId;   // get id from subgoal in path

    @EJB
    private MilestonesBean milestonesBean;
    
    
    /**
     * Gets a subgoal's list of milestones identified by the subgoal's ID
     * 
     * @return a MilestonesTO object
     */
    @GET    
    public MilestonesTO getAllMilestonesList()
    {
	return milestonesBean.getAllMilestones( Helpers.stringToLong( goalId ),  
		Helpers.stringToLong( subGoalId ) );
    }
    
    /**
     * Gets a milestone of a subgoal, identified by its ID
     * 
     * @param id of a milestone
     * @return a certain milestone identified by its ID
     */
    @GET
    @Path("{id}")  
    public MilestonesTO getMilestoneById( @PathParam("id") String id )
    {
	return milestonesBean.getMilestoneById( Helpers.stringToLong( goalId ), 
		Helpers.stringToLong( subGoalId ), Helpers.stringToLong( id ) );
    }
    
    /**
     * Creates a new milestone for a subgoal
     * 
     * @param milestone	the milestone being created
     * @return the created milestones transfer object
     */
    @POST 
    @Consumes( MediaType.APPLICATION_JSON )    
    public MilestonesTO createMilestone( MilestoneDO milestone )
    {	
	return milestonesBean.createMilestone( Helpers.stringToLong( goalId ), 
		Helpers.stringToLong( subGoalId ), milestone );
    }
    
    /**
     * Change data of a subgoal's milestone defined by its ID
     * 
     * @param 	id	the id of a milestone
     * @param 	milestone	a MilestoneDO object
     * @return	the created milestones transfer object
     */
    @PUT 
    @Path("{id}")  
    @Consumes( MediaType.APPLICATION_JSON )
    public MilestonesTO updateMilestoneById( @PathParam("id") String id, MilestoneDO milestone)
    {
	return milestonesBean.updateMilestoneById( Helpers.stringToLong( goalId ), 
		       Helpers.stringToLong( subGoalId ), Helpers.stringToLong( id ), milestone );
    }   
    
    /**
     * Delete a subgoal's milestone identified by its ID
     * 
     * @param id 	the id of a milestone
     * @return the milestone being deleted
     */
    @DELETE 
    @Path("{id}")
    public MilestonesTO deleteMilestoneById( @PathParam("id") String id )
    {
       return milestonesBean.deleteMilestoneById( Helpers.stringToLong( goalId ), 
		       Helpers.stringToLong( subGoalId ), Helpers.stringToLong( id ) );
    }   
}
