/**
 * 
 */
package org.gmt.rest.service.resource;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.gmt.persistence.GoalsBean;
import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.util.GoalsTO;
import org.gmt.rest.service.util.Helpers;


/**
 * This class handles the REST resource "/goals":
 *  -Implementation of the REST methods GET and POST for "/goals"
 *  -Implementation of the REST methods GET, PUT and DELETE for "/goals/{id}"
 *  -Implementation of the REST method GET for "/goals?{status}"
 *   
 * RF: -validation of input via REST: unknown values can lead to empty fields in the database
 *   
 * @author Ralf Mehle
 */
@Path( "/goals" ) 
@Produces( MediaType.APPLICATION_JSON) 
public class Goals
{
    @EJB
    private GoalsBean goalsBean;
       
    
    /**
     * Gets a list of all goals or a list of goals identified by their status
     * 
     * @param	status	the status of a goal
     * @return	a GoalsTO object
     */
    @GET    
    public GoalsTO getAllGoalsList(@QueryParam("status") String status)
    {	
	if (status == null)
	    return goalsBean.getAllGoals();
	return goalsBean.getAllGoalsByStatus(Helpers.stringToStatus(status));
    }
    
    /**
     * Gets a goal by its ID
     * 
     * @param id of a goal
     * @return a certain goal identified by its ID
     */
    @GET
    @Path("{id}")
    public GoalsTO getGoalById(@PathParam("id") String id)
    {
	return goalsBean.getGoalById(Helpers.stringToLong(id));
    }
    
    /**
     * Creates a new goal
     * 
     * @param goal	the goal being created
     * @return the created goals transfer object
     */
    @POST 
    @Consumes( MediaType.APPLICATION_JSON )
    public GoalsTO createGoal(GoalDO goal)
    {	
	return goalsBean.createGoal(goal);
    }
           
    /**
     * Updates data of an existing goal
     * 
     * @param 	goal	the GoalDO object
     * @return	a GoalsTO object
     */
    @PUT 
    @Path("{id}")
    @Consumes( MediaType.APPLICATION_JSON )
    public GoalsTO updateGoal(GoalDO goal)
    {
       return goalsBean.updateGoal(goal);
    }    
    
    /**
     * Deletes a goal identified by its ID
     * 
     * @param id of a goal
     * @return the goal being deleted
     */
    @DELETE 
    @Path("{id}")
    public GoalsTO deleteGoalById(@PathParam("id") String id)
    {
       return goalsBean.deleteGoalById(Helpers.stringToLong(id));
    }
}
