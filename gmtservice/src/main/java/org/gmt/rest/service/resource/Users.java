/**
 * 
 */
package org.gmt.rest.service.resource;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.gmt.persistence.UsersBean;
import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.util.UsersTO;
import org.gmt.rest.service.util.Helpers;


/**
 * This class handles the REST resource "/users":
 *  -Implementation of the REST methods GET and POST for "/users"
 *  -Implementation of the REST methods GET, PUT and DELETE for "/users/{id}"
 * 
 * RF: 	- change ...ById()? and getAllUsersList() to getAllUsers()?
 * 	- check parameters for null values
 * 
 * @author Ralf Mehle
 */
@Path( "/users" ) 
@Produces( MediaType.APPLICATION_JSON) 
public class Users
{
    @EJB
    private UsersBean usersBean;
    
    
    /**
     * Gets a list of all registered users
     * 
     * @return	a UsersTO object
     */
    @GET    
    public UsersTO getAllUsersList()
    {
	return usersBean.getAllUsers();
    }
    
    /**
     * Gets a user by its ID
     * @param id of a user
     * @return a certain user identified by its ID
     */
    @GET
    @Path("{id}")
    public UsersTO getUserById( @PathParam("id") String id )
    {
	return usersBean.getUserById( Helpers.stringToLong( id ) );
    }
    
    /**
     * Creates a new user
     * 
     * @param	user	the user being created
     * @return the created users transfer object
     */
    @POST 
    @Consumes( MediaType.APPLICATION_JSON )
    public UsersTO createUser( UserDO user)
    {	
	return usersBean.createUser( user );
    }
    
    /**
     * Updates data of an existing user
     * 
     * @param user	a UserDO object
     * @return a UsersTO object
     */
    @PUT 
    @Path("{id}")
    @Consumes( MediaType.APPLICATION_JSON )
    public UsersTO updateUser( UserDO user )
    {
       return usersBean.updateUser( user );
    }  
    
    /**
     * Deletes a user identified by its ID
     * 
     * @param id	id of a user
     * @return the user being deleted
     */
    @DELETE 
    @Path("{id}")
    public UsersTO deleteUserById(@PathParam("id") String id )
    {
	return usersBean.deleteUserById( Helpers.stringToLong( id ) );
    }
}
