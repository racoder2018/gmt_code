/**
 * 
 */
package org.gmt.rest.service.resource;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.gmt.persistence.StatusBean;
import org.gmt.persistence.model.util.StatusTO;


/**
 * - Class handling the REST resource "/status"
 * - Implementation of the REST method GET for "/status"
 *
 * @author Ralf Mehle
 */
@Path( "/status" ) 
@Produces( MediaType.APPLICATION_JSON) 
public class Status
{    
    @EJB
    private StatusBean statusBean;
    
    
    /**
     * Gets a list of all defined status
     * 
     * @return a StatusTO object
     */
    @GET    
    public StatusTO getAllStatusList()
    {	
	return statusBean.getAllStatus();
    }
}
