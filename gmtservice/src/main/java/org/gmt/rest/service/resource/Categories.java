/**
 * 
 */
package org.gmt.rest.service.resource;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.gmt.persistence.CategoriesBean;
import org.gmt.persistence.model.util.CategoriesTO;


/**
 * - Class handling the REST resource "/categories"
 * - Implementation of the REST method GET for "/categories"
 *
 * @author Ralf Mehle
 */
@Path( "/categories" ) 
@Produces( MediaType.APPLICATION_JSON) 
public class Categories
{    
    @EJB
    private CategoriesBean categoriesBean;
    
    
    /**
     * Gets a list of all defined categories
     * 
     * @return a CategoriesTO object
     */
    @GET    
    public CategoriesTO getAllCategoriesList()
    {	
	return categoriesBean.getAllCategories();
    }
}
