/**
 * 
 */
package org.gmt.rest.service.resource;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.gmt.persistence.SubGoalsBean;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.gmt.rest.service.util.Helpers;


/**
 * Class handling the REST resource "/goals/{id}/subgoals". 
 * A subgoal always belongs to a goal and cannot exist alone.
 * 
 * - Implementation of the REST methods GET and POST for "/goals/{id}/subgoals"
 * - Implementation of the REST methods GET, PUT and DELETE for "/goals/{id}/subgoals/{id}"
 * 
 * @author Ralf Mehle
 */
@Path( "/goals/{goalid}/subgoals" )
@Produces( MediaType.APPLICATION_JSON )
public class SubGoals
{
    private @PathParam("goalid") String goalId;   // get id from goal in path

    @EJB
    private SubGoalsBean subGoalsBean;
    
    
    /**
     * Gets a goal's list of subgoals identified by the goal's ID
     * 
     * @return	a SubGoalsTO object
     */
    @GET    
    public SubGoalsTO getAllSubGoalsList()
    {
	return subGoalsBean.getAllSubGoals( Helpers.stringToLong( goalId ) );
    }
    
    /**
     * Gets a subgoal of a goal, identified by its ID
     * @param id of a subgoal
     * @return a certain subgoal identified by its ID
     */
    @GET
    @Path("{id}")  
    public SubGoalsTO getSubGoalById( @PathParam("id") String id )
    {
	return subGoalsBean.getSubGoalById( Helpers.stringToLong(goalId), 
		Helpers.stringToLong( id ) );
    }
    
    /**
     * Creates a new subgoal for a goal
     * 
     * @param subGoal	the subgoal being created
     * @return the created subgoals transfer object
     */
    @POST 
    @Consumes( MediaType.APPLICATION_JSON )    
    public SubGoalsTO createSubGoal( SubGoalDO subGoal )
    {	
	return subGoalsBean.createSubGoal(Helpers.stringToLong(goalId), subGoal);
    }
           
    /**
     * Change data of a goal's subgoal defined by its ID
     * 
     * @param id 	the id of the subgoal
     * @param subGoal	the SubGoalDO object
     * @return	a SubGoalsTO object
     */
    @PUT 
    @Path("{id}")  
    @Consumes( MediaType.APPLICATION_JSON )
    public SubGoalsTO updateSubGoalById( @PathParam("id") String id, SubGoalDO subGoal)
    {
	return subGoalsBean.updateSubGoalById( Helpers.stringToLong(goalId), 
		Helpers.stringToLong( id ), subGoal );	
    }    
    
    /**
     * Delete a goal's subgoal identified by its ID
     * @param id of a subgoal
     * @return the subgoal being deleted
     */
    @DELETE 
    @Path("{id}")
    public SubGoalsTO deleteSubGoalById( @PathParam("id") String id )
    {
       return subGoalsBean.deleteSubGoalById(Helpers.stringToLong(goalId), 
		Helpers.stringToLong( id ) );
    }
}
