/**
 * Todo: We should write here smth. general about the application...
 */
package org.gmt.rest.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Initialization class for the Goals Management Tool (GMT) 
 * 
 * - initializes the REST Webservice
 * 
 * FT: -User authentication needed for REST-API (currently 
 *      exist no restrictions for accessing REST routes 
 *      - anyone can see any data)  
 *  
 * @author Ralf Mehle
 */
@ApplicationPath("/resources")
public class GmtServiceApplication extends Application
{
}
