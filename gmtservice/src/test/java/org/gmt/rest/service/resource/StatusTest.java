/**
 * 
 */
package org.gmt.rest.service.resource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.StatusDO;
import org.gmt.persistence.model.util.StatusTO;
import org.gmt.rest.service.resource.Status;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.rest.service.resource.Status
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.rest.service.resource.Status")
class StatusTest
{
    private static Status statusTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Status");
        statusTest = mock(Status.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Status completed.");
    }
   

    /**
     * Test method for {@link org.gmt.rest.service.resource.Status#getAllStatusList()}.
     */
    @Test
    @DisplayName("empty StatusTO = getAllStatusList()")
    final void testGetAllStatusListRetEmptyStatusTO()
    {
	// Given
	List<StatusDO> statusList = new ArrayList<>();
	StatusTO statusTO = new StatusTO();

	// When
	when(statusTest.getAllStatusList()).thenReturn(statusTO);

	// Then
	assertEquals(statusTest.getAllStatusList(), statusTO);
	assertNull(statusTO.getMessage());
	assertNull(statusTO.getReturncode());
	assertEquals(statusList, statusTO.getResults());
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Status#getAllStatusList()}.
     */
    @Test
    @DisplayName("StatusTO = getAllStatusList()")
    final void testGetAllStatusListRetStatusTO()
    {
	// Given
	List<StatusDO> statusList = new ArrayList<>();
	
	StatusTO statusTO = new StatusTO();
	statusTO.setMessage("Status test");
	statusTO.setReturncode(0);
	statusTO.getResults().addAll(statusList);
	
	// When
	when(statusTest.getAllStatusList()).thenReturn(statusTO);

	// Then
	assertEquals(statusTest.getAllStatusList(), statusTO);
	assertEquals("Status test", statusTO.getMessage());
	assertEquals(0, statusTO.getReturncode().intValue());
	assertEquals(statusList, statusTO.getResults());
    }
}
