/**
 * 
 */
package org.gmt.rest.service.util;

import org.gmt.persistence.model.Status;
import org.gmt.rest.service.util.Helpers;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.rest.service.util.Helpers
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.rest.service.util.Helpers")
class HelpersTest
{    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.util.Helpers");       
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.util.Helpers completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToLong(java.lang.String)}.
     */
    @Test
    @DisplayName("null = stringToLong(null)")
    final void testStringToLongNull()
    {
	Assertions.assertNull(Helpers.stringToLong(null));
    }	
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToLong(java.lang.String)}.
     */
    @Test
    @DisplayName("null = stringToLong(\"anystring\")")
    final void testStringToLongAnyString()
    {
	Assertions.assertNull(Helpers.stringToLong("anystring"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToLong(java.lang.String)}.
     */
    @Test
    @DisplayName("null = stringToLong(\"\")")
    final void testStringToLongEmptyString()
    {
	Assertions.assertNull(Helpers.stringToLong(""));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToLong(java.lang.String)}.
     */
    @Test
    @DisplayName("-1L = stringToLong(\"-1\")")
    final void testStringToLongMinusOne()
    {
	Assertions.assertEquals(new Long(-1L), Helpers.stringToLong("-1"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToLong(java.lang.String)}.
     */
    @Test
    @DisplayName("0L = stringToLong(\"0\")")
    final void testStringToLongZero()
    {
	Assertions.assertEquals(new Long(0L), Helpers.stringToLong("0"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToLong(java.lang.String)}.
     */
    @Test
    @DisplayName("1 = stringToLong(\"1\")")
    final void testStringToLongOne()
    {
	Assertions.assertEquals(new Long(1L), Helpers.stringToLong("1"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToLong(java.lang.String)}.
     */
    @Test
    @DisplayName("23L = stringToLong(\"23\")")
    final void testStringToLong23()
    {
	Assertions.assertEquals(new Long(23L), Helpers.stringToLong("23"));	
	Assertions.assertEquals(23L, Helpers.stringToLong("23").longValue());
    }    
        
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToInt(java.lang.String)}.
     */
    @Test
    @DisplayName("null = stringToInt(null)")
    final void testStringToIntNull()
    {
	Assertions.assertNull(Helpers.stringToInt(null));
    }	
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToInt(java.lang.String)}.
     */
    @Test
    @DisplayName("null = stringToInt(\"\")")
    final void testStringToIntEmptyString()
    {
	Assertions.assertNull(Helpers.stringToInt(""));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToInt(java.lang.String)}.
     */
    @Test
    @DisplayName("null = stringToInt(\"anystring\")")
    final void testStringToIntAnyString()
    {
	Assertions.assertNull(Helpers.stringToInt("anystring"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToInt(java.lang.String)}.
     */
    @Test
    @DisplayName("-1 = stringToInt(\"-1\")")
    final void testStringToIntMinusOne()
    {
	Assertions.assertEquals(new Integer(-1), Helpers.stringToInt("-1"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToInt(java.lang.String)}.
     */
    @Test
    @DisplayName("0 = stringToInt(\"0\")")
    final void testStringToIntZero()
    {
	Assertions.assertEquals(new Integer(0), Helpers.stringToInt("0"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToInt(java.lang.String)}.
     */
    @Test
    @DisplayName("1 = stringToInt(\"1\")")
    final void testStringToIntOne()
    {
	Assertions.assertEquals(new Integer(1), Helpers.stringToInt("1"));
    }   
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToInt(java.lang.String)}.
     */
    @Test
    @DisplayName("23 = stringToInt(\"23\")")
    final void testStringToInt()
    {
	Assertions.assertEquals(new Integer(23), Helpers.stringToInt("23"));
	Assertions.assertEquals(23, Helpers.stringToInt("23").intValue());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToStatus(java.lang.String)}.
     */
    @Test
    @DisplayName("null = stringToStatus(null)")
    final void testStringToStatusNull()
    {
	Assertions.assertNull(Helpers.stringToStatus(null));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToStatus(java.lang.String)}.
     */
    @Test
    @DisplayName("Status.UNKNOWN = stringToStatus(null)")
    final void testStringToStatusEmpty()
    {
	Assertions.assertEquals(Status.UNKNOWN, Helpers.stringToStatus(""));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToStatus(java.lang.String)}.
     */
    @Test
    @DisplayName("Status.UNKNOWN = stringToStatus(\"anystring\")")
    final void testStringToStatusAnyString()
    {
	Assertions.assertEquals(Status.UNKNOWN, Helpers.stringToStatus("anystring"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToStatus(java.lang.String)}.
     */
    @Test
    @DisplayName("Status.UNKNOWN = stringToStatus(\"42\")")
    final void testStringToStatusNumber()
    {
	Assertions.assertEquals(Status.UNKNOWN, Helpers.stringToStatus("42"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#stringToStatus(java.lang.String)}.
     */
    @Test
    @DisplayName(
	    "Status.DEFINED/RUNNING/ACHIEVED = stringToStatus(\"DEFINED/RUNNING/ACHIEVED\")")
    final void testStringToStatusDefRunAch()
    {
	Assertions.assertEquals(Status.DEFINED, Helpers.stringToStatus("DEFINED"));
	Assertions.assertEquals(Status.RUNNING, Helpers.stringToStatus("RUNNING"));
	Assertions.assertEquals(Status.ACHIEVED, Helpers.stringToStatus("ACHIEVED"));
    }

    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#isEmpty(java.lang.String)}.
     */
    @Test
    @DisplayName("False = isEmpty(null)")
    final void testIsEmptyNull()
    {	
	Assertions.assertTrue(Helpers.isEmpty(null));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#isEmpty(java.lang.String)}.
     */
    @Test
    @DisplayName("False = isEmpty(\"anystring\")")
    final void testIsEmptyAnyString()
    {	
	Assertions.assertFalse(Helpers.isEmpty("anystring"));
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.util.Helpers#isEmpty(java.lang.String)}.
     */
    @Test
    @DisplayName("True = isEmpty(\"\")")
    final void testIsEmptyEmptyString()
    {	
	Assertions.assertTrue(Helpers.isEmpty(""));
    }
}
