/**
 * 
 */
package org.gmt.rest.service.resource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.SubGoalsTO;
import org.gmt.rest.service.resource.SubGoals;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.rest.service.resource.SubGoals
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.rest.service.resource.SubGoals")
class SubGoalsTest
{
    private static SubGoals subGoalsTest; 
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.SubGoals");
        subGoalsTest = mock(SubGoals.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.SubGoals completed.");
    }

    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#getAllSubGoalsList()}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getAllSubGoalsList()")
    final void testGetAllSubGoalsListRetEmptySubGoalsTO()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	
	// When
	when(subGoalsTest.getAllSubGoalsList()).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.getAllSubGoalsList(), subGoalsTO);
	assertNull(subGoalsTO.getMessage());
	assertNull(subGoalsTO.getReturncode());
	assertEquals(subGoalsList, subGoalsTO.getResults());
    }
        
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#getAllSubGoalsList()}.
     */
    @Test
    @DisplayName("SubGoalsTO = getAllSubGoalsList()")
    final void testGetAllSubGoalsListRetSubGoalsTO()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get all subgoals test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.getAllSubGoalsList()).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.getAllSubGoalsList(), subGoalsTO);
	assertEquals("Get all subgoals test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(subGoalsList, subGoalsTO.getResults());
    }     
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#getSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(\"-1\")")
    final void testGetSubGoalByIdMinusOne()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by negative id test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.getSubGoalById("-1")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.getSubGoalById("-1"), subGoalsTO);
	assertEquals("Get a subgoal by negative id test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#getSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(\"0\")")
    final void testGetSubGoalByIdZero()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 0 test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.getSubGoalById("0")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.getSubGoalById("0"), subGoalsTO);
	assertEquals("Get a subgoal by id 0 test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#getSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("SubGoalsTO = getSubGoalById(\"1\")")
    final void testGetSubGoalByIdOne()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 1 test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.getSubGoalById("1")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.getSubGoalById("1"), subGoalsTO);
	assertEquals("Get a subgoal by id 1 test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#getSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("SubGoalsTO = getSubGoalById(\"10\")")
    final void testGetSubGoalByIdTen()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id 10 test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.getSubGoalById("10")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.getSubGoalById("10"), subGoalsTO);
	assertEquals("Get a subgoal by id 10 test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());	
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#getSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = getSubGoalById(\"anystring\")")
    final void testGetSubGoalByIdAnyString()
    {
	// Given
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Get a subgoal by id=anystring test");
	subGoalsTO.setReturncode(-1);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.getSubGoalById("0")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.getSubGoalById("0"), subGoalsTO);
	assertEquals("Get a subgoal by id=anystring test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#createSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = createSubGoal(null)")
    final void testCreateSubGoalNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.createSubGoal(null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.createSubGoal(null), subGoalsTO);
	assertEquals("Create a subgoal null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#createSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("SubGoalsTO = createSubGoal(SubGoalDO)")
    final void testCreateSubGoalSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	subGoal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	subGoal.setEndDate(LocalDateTime.parse("2019-01-01T09:00"));
	subGoal.setDescription("Get a plan.");
	subGoal.setShortTerm("Planning");
	subGoal.setNotifications(true);
	subGoal.setMilestones(new ArrayList<MilestoneDO>());
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Create a subgoal test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.createSubGoal(subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.createSubGoal(subGoal), subGoalsTO);
	assertEquals("Create a subgoal test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), subGoalsTO.getResults().get(0).getBeginDate());
	assertEquals(LocalDateTime.parse("2019-01-01T09:00"), subGoalsTO.getResults().get(0).getEndDate());
	assertEquals("Get a plan.", subGoalsTO.getResults().get(0).getDescription());
	assertEquals("Planning", subGoalsTO.getResults().get(0).getShortTerm());
	assertEquals(true, subGoalsTO.getResults().get(0).getNotifications());
	assertTrue(subGoalsTO.getResults().get(0).getMilestones().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(\"-1\", null)")
    final void testUpdateSubGoalMinusOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.updateSubGoalById("-1", null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("-1", null), subGoalsTO);
	assertEquals("Update a subgoal -1, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(\"0\", null)")
    final void testUpdateSubGoalZeroNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 0, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.updateSubGoalById("0", null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("0", null), subGoalsTO);
	assertEquals("Update a subgoal 0, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(\"1\", null)")
    final void testUpdateSubGoalOneNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 1, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.updateSubGoalById("1", null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("1", null), subGoalsTO);
	assertEquals("Update a subgoal 1, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(\"10\", null)")
    final void testUpdateSubGoalTenNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.updateSubGoalById("10", null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("10", null), subGoalsTO);
	assertEquals("Update a subgoal 10, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(\"anystring\", null)")
    final void testUpdateSubGoalAnyStringNull()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal anystring, null test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.updateSubGoalById("anystring", null)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("anystring", null), subGoalsTO);
	assertEquals("Update a subgoal anystring, null test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());	
    }
       
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(\"-1\", SubGoalDO)")
    final void testUpdateSubGoalMinusOneSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(-1L);
	subGoal.setDescription("Make a better car from wood.");
	subGoal.setStatus(Status.RUNNING);
		
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1, subGoal test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.updateSubGoalById("-1", subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("-1", subGoal), subGoalsTO);
	assertEquals("Update a subgoal -1, subGoal test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = updateSubGoal(\"0\", SubGoalDO)")
    final void testUpdateSubGoalZeroSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(-1L);
	subGoal.setDescription("Make a better car from wood.");
	subGoal.setStatus(Status.RUNNING);
		
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal -1, subGoal test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.updateSubGoalById("-1", subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("-1", subGoal), subGoalsTO);
	assertEquals("Update a subgoal -1, subGoal test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("SubGoalsTO = updateSubGoal(\"1\", SubGoalDO)")
    final void testUpdateSubGoalOneSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);
	subGoal.setDescription("Make a better car from wood.");
	subGoal.setStatus(Status.RUNNING);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 1, subGoal test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.updateSubGoalById("1", subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("1", subGoal), subGoalsTO);
	assertEquals("Update a subgoal 1, subGoal test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
	assertEquals("Make a better car from wood.", subGoalsTO.getResults().get(0).getDescription());
	assertEquals(Status.RUNNING, subGoalsTO.getResults().get(0).getStatus());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("SubGoalsTO = updateSubGoal(\"10\", SubGoalDO)")
    final void testUpdateSubGoalTenSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);
	subGoal.setDescription("Make a better car from wood.");
	subGoal.setStatus(Status.RUNNING);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal 10, subGoal test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.updateSubGoalById("10", subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("10", subGoal), subGoalsTO);
	assertEquals("Update a subgoal 10, subGoal test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
	assertEquals("Make a better car from wood.", subGoalsTO.getResults().get(0).getDescription());
	assertEquals(Status.RUNNING, subGoalsTO.getResults().get(0).getStatus());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#updateSubGoal(org.gmt.persistence.model.SubGoalDO)}.
     */
    @Test
    @DisplayName("SubGoalsTO = updateSubGoal(\"anystring\", SubGoalDO)")
    final void testUpdateSubGoalAnyStringSubGoalDO()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(100L);
	subGoal.setDescription("Make a better car from wood.");
	subGoal.setStatus(Status.RUNNING);
	
	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);
	
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Update a subgoal subGoal test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.updateSubGoalById("anystring", subGoal)).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.updateSubGoalById("anystring", subGoal), subGoalsTO);
	assertEquals("Update a subgoal subGoal test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(100L), subGoalsTO.getResults().get(0).getId());
	assertEquals("Make a better car from wood.", subGoalsTO.getResults().get(0).getDescription());
	assertEquals(Status.RUNNING, subGoalsTO.getResults().get(0).getStatus());
    }    

    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#deleteSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(\"-1\")")
    final void testDeleteSubGoalByIdMinusOne()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal id -1 test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.deleteSubGoalById("-1")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.deleteSubGoalById("-1"), subGoalsTO);
	assertEquals("Delete a subgoal id -1 test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#deleteSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(\"0\")")
    final void testDeleteSubGoalByIdZero()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal id 0 test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.deleteSubGoalById("0")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.deleteSubGoalById("0"), subGoalsTO);
	assertEquals("Delete a subgoal id 0 test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#deleteSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("SubGoalsTO = deleteSubGoalById(\"1\")")
    final void testDeleteSubGoalByIdOne()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(1L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal by id 1 test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.getSubGoalById("1")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.getSubGoalById("1"), subGoalsTO);
	assertEquals("Delete a subgoal by id 1 test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), subGoalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#deleteSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("SubGoalsTO = deleteSubGoalById(\"10\")")
    final void testDeleteSubGoalByIdTen()
    {
	// Given
	SubGoalDO subGoal = new SubGoalDO();
	subGoal.setId(10L);

	List<SubGoalDO> subGoalsList = new ArrayList<>();
	subGoalsList.add(subGoal);

	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal by id 10 test");
	subGoalsTO.setReturncode(0);
	subGoalsTO.getResults().addAll(subGoalsList);

	// When
	when(subGoalsTest.deleteSubGoalById("10")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.deleteSubGoalById("10"), subGoalsTO);
	assertEquals("Delete a subgoal by id 10 test", subGoalsTO.getMessage());
	assertEquals(0, subGoalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), subGoalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.SubGoals#deleteSubGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty SubGoalsTO = deleteSubGoalById(\"anystring\")")
    final void testDeleteSubGoalByIdAnyString()
    {
	// Given
	SubGoalsTO subGoalsTO = new SubGoalsTO();
	subGoalsTO.setMessage("Delete a subgoal id=anystring test");
	subGoalsTO.setReturncode(-1);

	// When
	when(subGoalsTest.deleteSubGoalById("anystring")).thenReturn(subGoalsTO);

	// Then
	assertEquals(subGoalsTest.deleteSubGoalById("anystring"), subGoalsTO);
	assertEquals("Delete a subgoal id=anystring test", subGoalsTO.getMessage());
	assertEquals(-1, subGoalsTO.getReturncode().intValue());
	assertTrue(subGoalsTO.getResults().isEmpty());
    }
}
