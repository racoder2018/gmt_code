/**
 * 
 */
package org.gmt.rest.service.resource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.GoalDO;
import org.gmt.persistence.model.Status;
import org.gmt.persistence.model.SubGoalDO;
import org.gmt.persistence.model.util.GoalsTO;
import org.gmt.rest.service.resource.Goals;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.rest.service.resource.Goals
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.rest.service.resource.Goals")
class GoalsTest
{
   private static Goals goalsTest; 
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Goals");
        goalsTest = mock(Goals.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Goals completed.");
    }
  

    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getAllGoalsList(java.lang.String)}.
     */
    @Test
    @DisplayName("GoalsTO = getAllGoalsList(null)")
    final void testGetAllGoalsListNull()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get all goals test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getAllGoalsList(null)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getAllGoalsList(null), goalsTO);
	assertEquals("Get all goals test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(goalsList, goalsTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getAllGoalsList(java.lang.String)}.
     */
    @Test
    @DisplayName("GoalsTO = getAllGoalsList(\"DEFINED\")")
    final void testGetAllGoalsListStatus()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get all goals with status=DEFINED test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getAllGoalsList("DEFINED")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getAllGoalsList("DEFINED"), goalsTO);
	assertEquals("Get all goals with status=DEFINED test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(goalsList, goalsTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getAllGoalsList(java.lang.String)}.
     */
    @Test
    @DisplayName("empty GoalsTO = getAllGoalsList(\"anystring\")")
    final void testGetAllGoalsListAnyString()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get all goals with status=anystring test");
	goalsTO.setReturncode(-1);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getAllGoalsList("anystring")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getAllGoalsList("anystring"), goalsTO);
	assertEquals("Get all goals with status=anystring test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }     
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getAllGoalsList(java.lang.String)}.
     */
    @Test
    @DisplayName("empty GoalsTO = getAllGoalsList(\"42\")")
    final void testGetAllGoalsListNumber()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setStatus(Status.UNKNOWN);
	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get all goals with 42 test");
	goalsTO.setReturncode(-1);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getAllGoalsList("anystring")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getAllGoalsList("anystring"), goalsTO);
	assertEquals("Get all goals with 42 test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertEquals(Status.UNKNOWN, goalsTO.getResults().get(0).getStatus());
    }     
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty GoalsTO = getGoalById(\"-1\")")
    final void testGetGoalByIdMinusOne()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by negative id test");
	goalsTO.setReturncode(-1);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getGoalById("-1")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getGoalById("-1"), goalsTO);
	assertEquals("Get a goal by negative id test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty GoalsTO = getGoalById(\"0\")")
    final void testGetGoalByIdZero()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 0 test");
	goalsTO.setReturncode(-1);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getGoalById("0")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getGoalById("0"), goalsTO);
	assertEquals("Get a goal by id 0 test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("GoalsTO = getGoalById(\"1\")")
    final void testGetGoalByIdOne()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 1 test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getGoalById("1")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getGoalById("1"), goalsTO);
	assertEquals("Get a goal by id 1 test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("GoalsTO = getGoalById(\"10\")")
    final void testGetGoalByIdTen()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);
	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 10 test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getGoalById("10")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getGoalById("10"), goalsTO);
	assertEquals("Get a goal by id 10 test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());	
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#getGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty GoalsTO = getGoalById(\"anystring\")")
    final void testGetGoalByIdAnyString()
    {
	// Given
	List<GoalDO> goalsList = new ArrayList<>();
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id=anystring test");
	goalsTO.setReturncode(-1);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getGoalById("0")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getGoalById("0"), goalsTO);
	assertEquals("Get a goal by id=anystring test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#createGoal(org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("empty GoalsTO = createGoal(null)")
    final void testCreateGoalNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Create a goal null test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsTest.createGoal(null)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.createGoal(null), goalsTO);
	assertEquals("Create a goal null test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#createGoal(org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("GoalsTO = createGoal(GoalDO)")
    final void testCreateGoalGoalDO()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	goal.setBeginDate(LocalDateTime.parse("2019-01-01T10:00"));
	goal.setEndDate(LocalDateTime.parse("2019-02-01T08:00"));
	goal.setCategory(Category.HOBBY);
	goal.setDescription("Make a car from wood.");
	goal.setShortTerm("Woodcar");
	goal.setNotifications(true);
	goal.setStatus(Status.DEFINED);
	goal.setSubGoals(new ArrayList<SubGoalDO>());
	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Create a goal null test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);


	// When
	when(goalsTest.createGoal(goal)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.createGoal(goal), goalsTO);
	assertEquals("Create a goal null test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
	assertEquals(LocalDateTime.parse("2019-01-01T10:00"), goalsTO.getResults().get(0).getBeginDate());
	assertEquals(LocalDateTime.parse("2019-02-01T08:00"), goalsTO.getResults().get(0).getEndDate());
	assertEquals(Category.HOBBY, goalsTO.getResults().get(0).getCategory());
	assertEquals("Make a car from wood.", goalsTO.getResults().get(0).getDescription());
	assertEquals("Woodcar", goalsTO.getResults().get(0).getShortTerm());
	assertEquals(true, goalsTO.getResults().get(0).getNotifications());
	assertEquals(Status.DEFINED, goalsTO.getResults().get(0).getStatus());
	assertTrue(goalsTO.getResults().get(0).getSubGoals().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#updateGoal(org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("empty GoalsTO = updateGoal(null)")
    final void testUpdateGoalNull()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Update a goal null test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsTest.updateGoal(null)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.updateGoal(null), goalsTO);
	assertEquals("Update a goal null test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#updateGoal(org.gmt.persistence.model.GoalDO)}.
     */
    @Test
    @DisplayName("GoalsTO = updateGoal(GoalDO)")
    final void testUpdateGoalGoalDO()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);
	goal.setDescription("Make a better car from wood.");
	goal.setStatus(Status.RUNNING);

	
	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);
	
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Update a goal null test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);


	// When
	when(goalsTest.updateGoal(goal)).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.updateGoal(goal), goalsTO);
	assertEquals("Update a goal null test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
	assertEquals("Make a better car from wood.", goalsTO.getResults().get(0).getDescription());
	assertEquals(Status.RUNNING, goalsTO.getResults().get(0).getStatus());
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#deleteGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty GoalsTO = deleteGoalById(\"-1\")")
    final void testDeleteGoalByIdMinusOne()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Delete a goal id -1 test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsTest.deleteGoalById("-1")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.deleteGoalById("-1"), goalsTO);
	assertEquals("Delete a goal id -1 test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#deleteGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty GoalsTO = deleteGoalById(\"0\")")
    final void testDeleteGoalByIdZero()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Delete a goal id 0 test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsTest.deleteGoalById("0")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.deleteGoalById("0"), goalsTO);
	assertEquals("Delete a goal id 0 test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#deleteGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("GoalsTO = deleteGoalById(\"1\")")
    final void testDeleteGoalByIdOne()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(1L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 1 test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.getGoalById("1")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.getGoalById("1"), goalsTO);
	assertEquals("Get a goal by id 1 test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(new Long(1L), goalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#deleteGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("GoalsTO = deleteGoalById(\"10\")")
    final void testDeleteGoalByIdTen()
    {
	// Given
	GoalDO goal = new GoalDO();
	goal.setId(10L);

	List<GoalDO> goalsList = new ArrayList<>();
	goalsList.add(goal);

	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Get a goal by id 10 test");
	goalsTO.setReturncode(0);
	goalsTO.getResults().addAll(goalsList);

	// When
	when(goalsTest.deleteGoalById("10")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.deleteGoalById("10"), goalsTO);
	assertEquals("Get a goal by id 10 test", goalsTO.getMessage());
	assertEquals(0, goalsTO.getReturncode().intValue());
	assertEquals(new Long(10L), goalsTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Goals#deleteGoalById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty GoalsTO = deleteGoalById(\"anystring\")")
    final void testDeleteGoalByIdAnyString()
    {
	// Given
	GoalsTO goalsTO = new GoalsTO();
	goalsTO.setMessage("Delete a goal id=anystring test");
	goalsTO.setReturncode(-1);

	// When
	when(goalsTest.deleteGoalById("anystring")).thenReturn(goalsTO);

	// Then
	assertEquals(goalsTest.deleteGoalById("anystring"), goalsTO);
	assertEquals("Delete a goal id=anystring test", goalsTO.getMessage());
	assertEquals(-1, goalsTO.getReturncode().intValue());
	assertTrue(goalsTO.getResults().isEmpty());
    }
}