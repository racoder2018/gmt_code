/**
 * 
 */
package org.gmt.rest.service.resource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.CategoryDO;
import org.gmt.persistence.model.util.CategoriesTO;
import org.gmt.rest.service.resource.Categories;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.rest.service.resource.Categories
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.rest.service.resource.Categories")
class CategoriesTest
{
    private static Categories categoriesTest;
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Categories");
        categoriesTest = mock(Categories.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Categories completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Categories#getAllCategoriesList()}.
     */   
    @Test
    @DisplayName("empty CategoriesTO = getAllCategoriesList()")
    final void testGetAllCategoriesListRetEmptyCategoriesTO()
    {
	// Given
	List<CategoryDO> categoriesList = new ArrayList<>();
	CategoriesTO categoriesTO = new CategoriesTO();

	// When
	when(categoriesTest.getAllCategoriesList()).thenReturn(categoriesTO);

	// Then
	assertEquals(categoriesTest.getAllCategoriesList(), categoriesTO);
	assertNull(categoriesTO.getMessage());
	assertNull(categoriesTO.getReturncode());
	assertEquals(categoriesList, categoriesTO.getResults());
    }
     
    /**
     * Test method for {@link org.gmt.rest.service.resource.Categories#getAllCategoriesList()}.
     */
    @Test
    @DisplayName("CategoriesTO = getAllCategoriesList()")
    final void testGetAllCategoriesListRetCategoriesTO()
    {
	// Given
	List<CategoryDO> categoriesList = new ArrayList<>();
	
	CategoriesTO categoriesTO = new CategoriesTO();
	categoriesTO.setMessage("Categories test");
	categoriesTO.setReturncode(0);
	categoriesTO.getResults().addAll(categoriesList);
	
	// When
	when(categoriesTest.getAllCategoriesList()).thenReturn(categoriesTO);

	// Then
	assertEquals(categoriesTest.getAllCategoriesList(), categoriesTO);
	assertEquals("Categories test", categoriesTO.getMessage());
	assertEquals(0, categoriesTO.getReturncode().intValue());
	assertEquals(categoriesList, categoriesTO.getResults());
    }    
}
