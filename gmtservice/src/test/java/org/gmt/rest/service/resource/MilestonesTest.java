/**
 * 
 */
package org.gmt.rest.service.resource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.MilestoneDO;
import org.gmt.persistence.model.util.MilestonesTO;
import org.gmt.rest.service.resource.Milestones;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.rest.service.resource.Milestones
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.rest.service.resource.Milestones")
class MilestonesTest
{
    private static Milestones milestonesTest; 
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Milestones");
        milestonesTest = mock(Milestones.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Milestones completed.");
    }
    

    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#getAllMilestonesList()}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getAllMilestonesList()")
    final void testGetAllMilestonesListRetEmptyMilestonesTO()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	
	// When
	when(milestonesTest.getAllMilestonesList()).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.getAllMilestonesList(), milestonesTO);
	assertNull(milestonesTO.getMessage());
	assertNull(milestonesTO.getReturncode());
	assertEquals(milestonesList, milestonesTO.getResults());
    }
        
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#getAllMilestonesList()}.
     */
    @Test
    @DisplayName("MilestonesTO = getAllMilestonesList()")
    final void testGetAllMilestonesListRetMilestonesTO()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get all milestones test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.getAllMilestonesList()).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.getAllMilestonesList(), milestonesTO);
	assertEquals("Get all milestones test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(milestonesList, milestonesTO.getResults());
    }     
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#getMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(\"-1\")")
    final void testGetMilestoneByIdMinusOne()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by negative id test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.getMilestoneById("-1")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.getMilestoneById("-1"), milestonesTO);
	assertEquals("Get a milestone by negative id test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#getMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(\"0\")")
    final void testGetMilestoneByIdZero()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 0 test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.getMilestoneById("0")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.getMilestoneById("0"), milestonesTO);
	assertEquals("Get a milestone by id 0 test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());	
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#getMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("MilestonesTO = getMilestoneById(\"1\")")
    final void testGetMilestoneByIdOne()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);
	
	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 1 test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.getMilestoneById("1")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.getMilestoneById("1"), milestonesTO);
	assertEquals("Get a milestone by id 1 test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(1L), milestonesTO.getResults().get(0).getId());
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#getMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("MilestonesTO = getMilestoneById(\"10\")")
    final void testGetMilestoneByIdTen()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(10L);
	
	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10 test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.getMilestoneById("10")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.getMilestoneById("10"), milestonesTO);
	assertEquals("Get a milestone by id 10 test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(10L), milestonesTO.getResults().get(0).getId());	
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#getMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = getMilestoneById(\"anystring\")")
    final void testGetMilestoneByIdAnyString()
    {
	// Given
	List<MilestoneDO> milestonesList = new ArrayList<>();
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id=anystring test");
	milestonesTO.setReturncode(-1);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.getMilestoneById("0")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.getMilestoneById("0"), milestonesTO);
	assertEquals("Get a milestone by id=anystring test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#createMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = createMilestone(null)")
    final void testCreateMilestoneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.createMilestone(null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.createMilestone(null), milestonesTO);
	assertEquals("Create a milestone null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#createMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("MilestonesTO = createMilestone(MilestoneDO)")
    final void testCreateMilestoneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);
	milestone.setDescription("The plan is ready.");
	milestone.setNotifications(true);
	milestone.setName("MS1");
	milestone.setTodosFinished(new ArrayList<String>());
	
	
	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Create a milestone test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.createMilestone(milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.createMilestone(milestone), milestonesTO);
	assertEquals("Create a milestone test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	
	assertEquals(new Long(1L), milestonesTO.getResults().get(0).getId());
	assertEquals("The plan is ready.", milestonesTO.getResults().get(0).getDescription());
	assertEquals(true, milestonesTO.getResults().get(0).getNotifications());
	assertEquals("MS1", milestonesTO.getResults().get(0).getName());
	assertTrue(milestonesTO.getResults().get(0).getTodosFinished().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(\"-1\", null)")
    final void testUpdateMilestoneMinusOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.updateMilestoneById("-1", null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("-1", null), milestonesTO);
	assertEquals("Update a milestone -1, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(\"0\", null)")
    final void testUpdateMilestoneZeroNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 0, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.updateMilestoneById("0", null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("0", null), milestonesTO);
	assertEquals("Update a milestone 0, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(\"1\", null)")
    final void testUpdateMilestoneOneNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 1, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.updateMilestoneById("1", null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("1", null), milestonesTO);
	assertEquals("Update a milestone 1, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(\"10\", null)")
    final void testUpdateMilestoneTenNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.updateMilestoneById("10", null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("10", null), milestonesTO);
	assertEquals("Update a milestone 10, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());	
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(\"anystring\", null)")
    final void testUpdateMilestoneAnyStringNull()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone anystring, null test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.updateMilestoneById("anystring", null)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("anystring", null), milestonesTO);
	assertEquals("Update a milestone anystring, null test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());	
    }
       
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(\"-1\", MilestoneDO)")
    final void testUpdateMilestoneMinusOneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(-1L);
	milestone.setDescription("Make a better car from wood.");
		
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1, milestone test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.updateMilestoneById("-1", milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("-1", milestone), milestonesTO);
	assertEquals("Update a milestone -1, milestone test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(\"0\", MilestoneDO)")
    final void testUpdateMilestoneZeroMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(-1L);
	milestone.setDescription("The plan is ready.");
		
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone -1, milestone test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.updateMilestoneById("-1", milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("-1", milestone), milestonesTO);
	assertEquals("Update a milestone -1, milestone test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("MilestonesTO = updateMilestone(\"1\", MilestoneDO)")
    final void testUpdateMilestoneOneMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);
	milestone.setDescription("The plan is ready.");
	milestone.setName("MS1");
	milestone.setTodosFinished(new ArrayList<String>());

	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 1, milestone test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.updateMilestoneById("1", milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("1", milestone), milestonesTO);
	assertEquals("Update a milestone 1, milestone test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(1L), milestonesTO.getResults().get(0).getId());
	assertEquals("The plan is ready.", milestonesTO.getResults().get(0).getDescription());
	assertEquals("MS1", milestonesTO.getResults().get(0).getName());
	assertTrue(milestonesTO.getResults().get(0).getTodosFinished().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("MilestonesTO = updateMilestone(\"10\", MilestoneDO)")
    final void testUpdateMilestoneTenMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(10L);
	milestone.setDescription("The plan is ready.");
	milestone.setName("MS1");
	milestone.setTodosFinished(new ArrayList<String>());

	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone 10, milestone test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.updateMilestoneById("10", milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("10", milestone), milestonesTO);
	assertEquals("Update a milestone 10, milestone test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(10L), milestonesTO.getResults().get(0).getId());
	assertEquals("The plan is ready.", milestonesTO.getResults().get(0).getDescription());
	assertEquals("MS1", milestonesTO.getResults().get(0).getName());
	assertTrue(milestonesTO.getResults().get(0).getTodosFinished().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#updateMilestone(org.gmt.persistence.model.MilestoneDO)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = updateMilestone(\"anystring\", MilestoneDO)")
    final void testUpdateMilestoneAnyStringMilestoneDO()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(100L);
	milestone.setDescription("The plan is ready.");
	
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Update a milestone milestone test");
	milestonesTO.setReturncode(0);

	// When
	when(milestonesTest.updateMilestoneById("anystring", milestone)).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.updateMilestoneById("anystring", milestone), milestonesTO);
	assertEquals("Update a milestone milestone test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }    

    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#deleteMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(\"-1\")")
    final void testDeleteMilestoneByIdMinusOne()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestones id -1 test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.deleteMilestoneById("-1")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.deleteMilestoneById("-1"), milestonesTO);
	assertEquals("Delete a milestones id -1 test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#deleteMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(\"0\")")
    final void testDeleteMilestoneByIdZero()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone id 0 test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.deleteMilestoneById("0")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.deleteMilestoneById("0"), milestonesTO);
	assertEquals("Delete a milestone id 0 test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#deleteMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("MilestonesTO = deleteMilestoneById(\"1\")")
    final void testDeleteMilestoneByIdOne()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(1L);

	List<MilestoneDO> miletonesList = new ArrayList<>();
	miletonesList.add(milestone);

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 1 test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(miletonesList);

	// When
	when(milestonesTest.getMilestoneById("1")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.getMilestoneById("1"), milestonesTO);
	assertEquals("Get a milestone by id 1 test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(1L), milestonesTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#deleteMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("MilestonesTO = deleteMilestoneById(\"10\")")
    final void testDeleteMilestoneByIdTen()
    {
	// Given
	MilestoneDO milestone = new MilestoneDO();
	milestone.setId(10L);

	List<MilestoneDO> milestonesList = new ArrayList<>();
	milestonesList.add(milestone);

	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Get a milestone by id 10 test");
	milestonesTO.setReturncode(0);
	milestonesTO.getResults().addAll(milestonesList);

	// When
	when(milestonesTest.deleteMilestoneById("10")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.deleteMilestoneById("10"), milestonesTO);
	assertEquals("Get a milestone by id 10 test", milestonesTO.getMessage());
	assertEquals(0, milestonesTO.getReturncode().intValue());
	assertEquals(new Long(10L), milestonesTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Milestones#deleteMilestoneById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty MilestonesTO = deleteMilestoneById(\"anystring\")")
    final void testDeleteMilestoneByIdAnyString()
    {
	// Given
	MilestonesTO milestonesTO = new MilestonesTO();
	milestonesTO.setMessage("Delete a milestone id=anystring test");
	milestonesTO.setReturncode(-1);

	// When
	when(milestonesTest.deleteMilestoneById("anystring")).thenReturn(milestonesTO);

	// Then
	assertEquals(milestonesTest.deleteMilestoneById("anystring"), milestonesTO);
	assertEquals("Delete a milestone id=anystring test", milestonesTO.getMessage());
	assertEquals(-1, milestonesTO.getReturncode().intValue());
	assertTrue(milestonesTO.getResults().isEmpty());
    }
}
