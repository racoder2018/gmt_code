/**
 * 
 */
package org.gmt.rest.service.resource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.gmt.persistence.model.UserDO;
import org.gmt.persistence.model.util.UsersTO;
import org.gmt.rest.service.resource.Users;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 * Unit test cases for the class org.gmt.rest.service.resource.Users
 * 
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.rest.service.resource.Users")
class UsersTest
{
    private static Users usersTest; 
    
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Users");
        usersTest = mock(Users.class);
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.resource.Users completed.");
    }


    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#getAllUsersList()}.
     */
    @Test
    @DisplayName("empty UsersTO = getAllUsersList()")
    final void testGetAllUsersListRetEmptyUsersTO()
    {
	// Given
	List<UserDO> usersList = new ArrayList<>();
	UsersTO usersTO = new UsersTO();

	// When
	when(usersTest.getAllUsersList()).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.getAllUsersList(), usersTO);
	assertNull(usersTO.getMessage());
	assertNull(usersTO.getReturncode());
	assertEquals(usersList, usersTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#getAllUsersList()}.
     */
    @Test
    @DisplayName("UsersTO = getAllUsersList()")
    final void testGetAllUsersListRetUsersTO()
    {
	// Given
	List<UserDO> usersList = new ArrayList<>();
	
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get all users test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersTest.getAllUsersList()).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.getAllUsersList(), usersTO);
	assertEquals("Get all users test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(usersList, usersTO.getResults());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#getUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty UsersTO = getUserById(\"-1\")")
    final void testGetUserByIdMinusOne()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by negative id test");
	usersTO.setReturncode(-1);

	// When
	when(usersTest.getUserById("-1")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.getUserById("-1"), usersTO);
	assertEquals("Get a user by negative id test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#getUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty UsersTO = getUserById(\"0\")")
    final void testGetUserByIdZero()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id 0 test");
	usersTO.setReturncode(-1);

	// When
	when(usersTest.getUserById("0")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.getUserById("0"), usersTO);
	assertEquals("Get a user by id 0 test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#getUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("UsersTO = getUserById(\"1\")")
    final void testGetUserByIdOne()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id=1 test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersTest.getUserById("1")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.getUserById("1"), usersTO);
	assertEquals("Get a user by id=1 test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(usersList, usersTO.getResults());
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#getUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("UsersTO = getUserById(\"10\")")
    final void testGetUserByIdTen()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(10L);
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);
	
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id 10 test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersTest.getUserById("10")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.getUserById("10"), usersTO);
	assertEquals("Get a user by id 10 test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(usersList, usersTO.getResults());
	assertEquals(new Long(10L), usersTO.getResults().get(0).getId());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#getUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty UsersTO = getUserById(\"anystring\")")
    final void testGetUserByIdAnyString()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Get a user by id=anystring test");
	usersTO.setReturncode(-1);

	// When
	when(usersTest.getUserById("anystring")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.getUserById("anystring"), usersTO);
	assertEquals("Get a user by id=anystring test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#createUser(org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("empty UsersTO = createUser(null)")
    final void testCreateUserNull()
    {
	// Given			
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a user null test");
	usersTO.setReturncode(-1);	

	// When
	when(usersTest.createUser(null)).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.createUser(null), usersTO);
	assertEquals("Create a user null test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#createUser(org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("UsersTO = createUser(UserDO)")
    final void testCreateUserUserDO()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Feuerstein");
	user.setLoginName("hfeuerstein");
	user.setPassword("abcdef1234");
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Create a user test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersTest.createUser(user)).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.createUser(user), usersTO);
	assertEquals("Create a user test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	
	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Feuerstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hfeuerstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#updateUser(org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("empty UsersTO = updateUser(null)")
    final void testUpdateUserNull()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Update a user null test");
	usersTO.setReturncode(-1);

	// When
	when(usersTest.updateUser(null)).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.updateUser(null), usersTO);
	assertEquals("Update a user null test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#updateUser(org.gmt.persistence.model.UserDO)}.
     */
    @Test
    @DisplayName("UsersTO = updateUser(UserDO)")
    final void testUpdateUserUserDO()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);
	user.setFirstName("Herbert");
	user.setLastName("Blankenstein");
	user.setLoginName("hblankenstein");
	user.setPassword("abcdef1234");

	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);

	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Update a user test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersTest.updateUser(user)).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.updateUser(user), usersTO);
	assertEquals("Update a user test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());

	assertEquals(usersList, usersTO.getResults());	
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());
	assertEquals("Herbert", usersTO.getResults().get(0).getFirstName());
	assertEquals("Blankenstein", usersTO.getResults().get(0).getLastName());
	assertEquals("hblankenstein", usersTO.getResults().get(0).getLoginName());
	assertEquals("abcdef1234", usersTO.getResults().get(0).getPassword());    
    }

    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#deleteUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty UsersTO = deleteUserById(\"-1\")")
    final void testDeleteUserByIdMinusOne()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id -1 test");
	usersTO.setReturncode(-1);

	// When
	when(usersTest.deleteUserById("-1")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.deleteUserById("-1"), usersTO);
	assertEquals("Delete a user id -1 test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());	
    }

    /** Test method for {@link org.gmt.rest.service.resource.Users#deleteUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty UsersTO = deleteUserById(\"0\")")
    final void testDeleteUserByIdZero()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id 0 test");
	usersTO.setReturncode(-1);

	// When
	when(usersTest.deleteUserById("0")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.deleteUserById("0"), usersTO);
	assertEquals("Delete a user id 0 test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());    
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#deleteUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("UsersTO = deleteUserById(\"1\")")
    final void testDeleteUserByIdOne()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(1L);
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);
	
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id 1 test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersTest.deleteUserById("1")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.deleteUserById("1"), usersTO);
	assertEquals("Delete a user id 1 test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(new Long(1L), usersTO.getResults().get(0).getId());   
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#deleteUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("UsersTO = deleteUserById(\"10\")")
    final void testDeleteUserByIdTen()
    {
	// Given
	UserDO user = new UserDO();
	user.setId(10L);
	
	List<UserDO> usersList = new ArrayList<>();
	usersList.add(user);
	
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id 10 test");
	usersTO.setReturncode(0);
	usersTO.getResults().addAll(usersList);

	// When
	when(usersTest.deleteUserById("10")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.deleteUserById("10"), usersTO);
	assertEquals("Delete a user id 10 test", usersTO.getMessage());
	assertEquals(0, usersTO.getReturncode().intValue());
	assertEquals(new Long(10L), usersTO.getResults().get(0).getId());   
    }
    
    /**
     * Test method for {@link org.gmt.rest.service.resource.Users#deleteUserById(java.lang.String)}.
     */
    @Test
    @DisplayName("empty UsersTO = deleteUserById(\"anystring\")")
    final void testDeleteUserByIdAnyString()
    {
	// Given
	UsersTO usersTO = new UsersTO();
	usersTO.setMessage("Delete a user id=anystring test");
	usersTO.setReturncode(-1);

	// When
	when(usersTest.deleteUserById("anystring")).thenReturn(usersTO);

	// Then
	assertEquals(usersTest.deleteUserById("anystring"), usersTO);
	assertEquals("Delete a user id=anystring test", usersTO.getMessage());
	assertEquals(-1, usersTO.getReturncode().intValue());
	assertTrue(usersTO.getResults().isEmpty());    
    }
}
