/**
 * 
 */
package org.gmt.service;

import org.gmt.rest.service.GmtServiceApplication;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


/**
 *  Unit test cases for the class org.gmt.rest.service.GmtServiceApplication
 *  
 * @author Ralf Mehle
 */
@DisplayName("TestCases for org.gmt.rest.service.GmtServiceApplication")
class GmtServiceApplicationTest
{
    @BeforeAll
    static void beforeAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.GmtServiceApplication");       
    }
 
    @BeforeEach
    void beforeEach() 
    {
//        System.out.println(" Before each test method ");
    }
 
    @AfterEach
    void afterEach() 
    {
//        System.out.println(" After each test method ");
    }
 
    @AfterAll
    static void afterAll() 
    {
        System.out.println("==> TestCases for org.gmt.rest.service.GmtServiceApplication completed.");
    }    
    
    
    /**
     * Test method for {@link org.gmt.rest.service.GmtServiceApplication()}.
     */
    @Test
    @DisplayName("not null = new GmtServiceApplication()")
    final void testCreateGmtServiceApplicationObj()
    {
	GmtServiceApplication gmtServiceApplication = new GmtServiceApplication();
	Assertions.assertNotNull(gmtServiceApplication);
    }
    
}
