# Goal Management Tool (GMT)

The Goal Management Tool (GMT) is a web application platform for managing your goals in life. 
Every goal you define can be associated to the following predefined categories:

* PERSONAL
* PROFESSION
* FAMILY
* HOBBY
* SCHOOL
* UNIVERSITY
* BUSINESS

Besides, for a goal the following status can be set:

* DEFINED
* RUNNING
* ACHIEVED

A goal can contain a list of subgoals. Every subgoal can contain a list of milestones. See the chapter "Examples" for the details. 

Benefits of the GMT Live-Demo project:

* build a useful web application and get it running online  
* CRUD functionality for managing *goals, subgoals and milestones*, covered by the GMT REST-Service
* CRUD main functionality for visualizing and managing *only goals* via GMT Web-Client 
* demonstrate the use of current JAVA technologies for the complete application stack 
* the gmtclient (Web) consumes the methods the gmtservice provides (REST-API + database access)

Technologies used for implementation:

* PostgreSQL 10.6 DBMS 
* Java EE8 with Glassfish 5.0 application server
* Eclipse IDE for Java EE developers 
* Eclipselink with JTA and JDBC for database access
* EJBs for persistence (Glassfish's EJB container)
* JPA for persistence 
* JAX-RS for REST-API definition (Glassfish's Servlet container)
* ORM / schema generation with DDL scripts
* JUnit 5.0 for unit testing
* JSF, CLI, EL, PrimeFaces for the web client component (a second Servlet)

Technologies used for deployment:

* Amazon Web Services (AWS) - RDS for hosting the database, EC2 for hosting the REST service and web client component

## Getting Started

The Live-Demo version is available at the website:

``http://ec2-3-122-177-115.eu-central-1.compute.amazonaws.com/gmtclient/index.jsf``

The REST-API service is available at:

``http://ec2-3-122-177-115.eu-central-1.compute.amazonaws.com/gmtservice/resources``

Additionally, a subroute has to be added. To see a quick response, you could add ``/categories`` to the Url. See the chapter **"gmtservice REST routes"** for an overview of all implemented REST routes. 

To log on the website, **one** predefined user account can be used:
	
	User: guest2019
	Password: guestGUEST 

### gmtservice REST routes 

	The REST service base path: /gmtservice/resources  

	/goals
	GET - returns a list of all goals defined, including subgoals and milestones
	POST - creates a new goal

	/goals?{status=<value>}
	GET - returns a list of all goals of a certain status, e.g. all achieved goals - status=ACHIEVED

	/goals/{id}
	GET - returns a certain goal with id {id}, including its subgoals and milestones (detail data)
	PUT - changes a certain goal with id {id} 
	DELETE - deletes a certain goal with id {id}

	/goals/{id}/subgoals
	GET - returns all subgoals of a goal
	POST - creates a new subgoal 

	/goals/{id}/subgoals/{id}
	GET - returns a certain subgoal with id {id}, including its milestones
	PUT - changes a certain goal with id {id}
	DELETE - deletes a certain subgoal with id {id}

	/goals/{id}/subgoals/{id}/milestones
	GET - returns all milestones of a subgoal
	POST - creates a new milestone

	/goals/{id}/subgoals/{id}/milestones/{id}
	GET - returns a certain milestone of a subgoal with id {id}
	PUT - changes a certain milestone with id {id}
	DELETE - deletes a certain milestone with id {id}

	/categories
	GET - returns a list of all goal categories defined
	
	/status
	GET - returns a list of all goal status defined
	
	/users
	GET - returns a list of all users defined
	POST - creates a new user 

	/users/{id}
	GET - returns a certain user with id {id}
	PUT - changes a certain user with id {id}
	DELETE - deetes a certain user with id {id}

### Examples for using the REST-API

The following examples can be used to interact with *gmtservice*. Install a browser add-on like *"RESTClient"* for Firefox to execute the *gmtservice* REST routes. Make sure to set the HTTP-Header *"Content-Type":"application/json"* to view JSON requests/responses.

List all goals defined:

	Method: GET
	Url: ../gmtservice/resources/goals
	
	JSON-Response:
	{"message":"2 Goals","results":[{"beginDate":"2020-02-01T10:00:00","description":"Within a year I will improve my knowledge of the Spanish language to speak fluently.","endDate":"2020-09-30T10:00:00","notifications":true,"shortTerm":"Improve Spanish knowledge","category":"PERSONAL","id":15,"status":"DEFINED","subGoals":[...]},{"beginDate":"2020-04-01T09:00:00","description":"I will plant a new cherry tree in my garden.","endDate":"2020-04-15T15:00:00","notifications":true,"shortTerm":"Plant a tree","category":"FAMILY","id":2,"status":"DEFINED","subGoals":[...]}],"returncode":0}

Creating a goal:

	Method: POST
	Url: ../gmtservice/resources/goals
	
	JSON payload:
	{ "id":"100", "shortTerm":"Improve Spanish knowledge", "description":"Within a year I will improve my knowledge of the Spanish language to speak fluently.", "beginDate":"2020-02-01T10:00:00", "endDate":"2020-09-30T10:00:00", "category":"PERSONAL", "status":"DEFINED", "subGoals":[], "notifications":"true" }

Creating a goal with a subgoal and a milestone:
	
	Method: POST
	Url: ../gmtservice/resources/goals
	
	JSON payload:
	{ "id":"100", "shortTerm":"Improve Spanish knowledge", "description":"Within a year I will improve my knowledge of the Spanish language to speak fluently.", "beginDate":"2020-02-01T10:00:00", "endDate":"2020-09-30T10:00:00", "category":"PERSONAL", "status":"DEFINED", "subGoals":[{ "id":"10", "shortTerm":"Spanish course", "description":"Do a Spanish course", "beginDate":"2020-02-03T08:00:00", "endDate":"2020-07-31T16:00:00", "status":"DEFINED", "milestones":[{ "id":"1", "name":"MS1", "description":"Book a Spanish course", "todos":["Compare results in Google", "Pick and book a course"], "notifications":"true" },{ "id":"2", "name":"MS2", "description":"Conversation skills improvement", "todos":["Talk with people speaking spanish", "Install and use a spanish language app"], "notifications":"true" }], "notifications":"true" }], "notifications":"true" }

Update a goal (change beginDate and endDate):

	Method: PUT
	Url: ../gmtservice/resources/goals{100}
	
	JSON payload:
	{ "id":"100", "shortTerm":"Improve Spanish knowledge", "description":"Within a year I will improve my knowledge of the Spanish language to speak fluently.", "beginDate":"2020-02-03T08:00:00", "endDate":"2020-10-15T15:00:00", "category":"PERSONAL", "status":"DEFINED", "subGoals":[{ "id":"10", "shortTerm":"Spanish course", "description":"Do a Spanish course", "beginDate":"2020-02-03T08:00:00", "endDate":"2020-07-31T16:00:00", "status":"DEFINED", "milestones":[{ "id":"1", "name":"MS1", "description":"Book a Spanish course", "todos":["Compare results in Google", "Pick and book a course"], "notifications":"true" },{ "id":"2", "name":"MS2", "description":"Conversation skills improvement", "todos":["Talk with people speaking spanish", "Install and use a spanish language app"], "notifications":"true" }], "notifications":"true" }], "notifications":"true" }

Delete a goal:

	Method: DELETE
	Url: ../gmtservice/resources/goals{100}
	
	JSON payload: - 
	
List all goals with status=DEFINED:

	Method: GET
	Url: ../gmtservice/resources/goals?status=DEFINED
	
	JSON response:
	{"message":"1 Goals","results":[{"beginDate":"2020-02-01T10:00:00","description":"Within a year I will improve my knowledge of the Spanish language to speak fluently.","endDate":"2020-09-30T10:00:00","notifications":true,"shortTerm":"Improve Spanish knowledge","category":"PERSONAL","id":15,"status":"DEFINED","subGoals":[{"beginDate":"2020-02-03T08:00:00","description":"Do a Spanish course","endDate":"2020-07-31T16:00:00","notifications":true,"shortTerm":"Spanish course","id":1,"milestones":[{"description":"Book a Spanish course","id":1,"name":"MS1","notifications":true,"todosFinished":[]},{"description":"Conversation skills improvement","id":2,"name":"MS2","notifications":true,"todosFinished":[]}],"status":"DEFINED"}]}],"returncode":0}
	

### Prerequisites

The following software has to be installed locally:

1. Java EE 8 
2. Glassfish 5.0 
3. Eclipse IDE for EE developers with "Eclipse Data Tools Platform", "Glassfish Tools" and Git
4. PostgreSQL DBMS 
5. A web browser, e.g. Firefox

### Installing

The following steps can be executed for GMT local installation:

1. Clone the git repository: *https://bitbucket.org/rmcoder2018/gmt_code.git* in Eclipse
2. Replace the local *domain.xml* file of Glassfish with the one in the *deploy* directory + restart *domain1*
3. Create a database *gmt_db* and a user *gmtuser*, password *gmtuser2019*
4. Import the Java projects *gmtclient, gmtpersistence, gmtservice*
5. Add a Glassfish 5 server to the Servers View
6. Run *gmtservice* or *gmtclient* by "Run As" -> "Run on server" on Right-click

## Running the tests

Unit tests are implemented for the *gmtpersistence* component. In Eclipse open ejbModule/src/test/java -> Right-Click
-> Select "Run As" -> "JUnit Test" 

### Break down into end to end tests

no end to end tests 

### And coding style tests

no coding style tests

## Deployment

The component *gmtpersistence* is built as jar library. The *gmtservice* component is built with *gmtpersistence* as reference project. The *gmtclient* component is built with *gmtpersistence* and *gmtservice* as reference projects.
The exported .war files in Eclipse for *gmtclient* and *gmtservice*, have to be deployed to the autodeploy directory in the glassfish domain: 

	<path to glassfish>/domains/domain1/autodeploy 

## Constraints

The application still has some issues. The focus was to get the functionality to run at first hand. 
The issues are fixed in further versions:


* data inconsistency issue: multiple users logged in their accounts can see the data of other users
* the goals overview can stack 9 goals horizontally - goals > 9 are there but not displayed 
* "flickering" effect during logout
* REST-API is not HATEOAS compliant, not session-dependent and has no authentication mechanism

## Versioning

For the moment there is no versioning, just the Live-Demo. Versioning will be added to further implementations.

## Authors

* **Ralf Mehle** - *Initial work* 

## License

This project is not licensed actually.
