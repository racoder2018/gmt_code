/**
 * 
 */
package org.gmt.rest.client.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.gmt.rest.client.GoalClient;
import org.gmt.rest.client.model.Goal;
import org.gmt.rest.client.view.GoalTimeline;
import org.primefaces.event.timeline.TimelineModificationEvent;
import org.primefaces.model.timeline.TimelineModel;


/**
 * The BackingBean 'GoalController' controls CRUD operations for goals. 
 * 
 * @author Ralf Mehle
 */
@Named
@ViewScoped
public class GoalController implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    @Inject
    private GoalClient goalClient;
    private List<Goal> goals;
    private Goal goal;
    private List<String> subGoals;   	// just a placeholder...
    private GoalTimeline goalTimeline;
    private String buttonValue;     	// label string for save/update button 
    private Long goalId = -1L;		// for deletion ...
    
    
    @PostConstruct
    public void init() 
    {        
	goal = new Goal();
	
	subGoals = new ArrayList<String>();
	subGoals.add("test");
	
	goals = goalClient.getAllGoals();	
	goalTimeline = new GoalTimeline(goals);
    }       
        
    /**
     * @return the goal
     */
    public Goal getGoal()
    {
        return goal;
    }
    
    /**
     * @param goal the goal to set
     */
    public void setGoal(Goal goal)
    {
        this.goal = goal;
    }
    
    /**
     * @return the subGoals
     */
    public List<String> getSubGoals()
    {
        return subGoals;
    }
    
    /**
     * @param subGoals the subGoals to set
     */
    public void setSubGoals(List<String> subGoals)
    {
        this.subGoals = subGoals;
    }
    
    /**
     * @return the goalTimeline
     */
    public GoalTimeline getGoalTimeline()
    {
        return goalTimeline;
    }
    
    /**
     * @param goalTimeline the goalTimeline to set
     */
    public void setGoalTimeline(GoalTimeline goalTimeline)
    {
        this.goalTimeline = goalTimeline;
    }
    
    /**
     * @return the buttonValue
     */
    public String getButtonValue()
    {
	return buttonValue;
    }

    /**
     * @param buttonValue the buttonValue to set
     */
    public void setButtonValue(String buttonValue)
    {
	this.buttonValue = buttonValue;
    }
    
    /**
     * Returns the GoalTimeLineModel
     * 
     * @return	the goal TimeLineModel
     */
    public TimelineModel getGoalTimelineModel()
    {	
	return goalTimeline.getGoalTimelineModel();	
    }
        
    /**
     * Save a new goal in the backend via REST-API
     * 
     * @return	responseMsg	the response message as string
     */
    public String saveNewGoal()
    {
	// add subgoals and milestones before saving
	String responseMsg = goalClient.createGoal(goal);
	doPageReload();
	
	return responseMsg;
    }
    
    /**
     * Save all subgoals in a goal
     * 
     * @return	an empty string
     */
    public String saveSubGoals()
    {
	//subGoals.put();
	
	return "";	
    }
    
    /**
     * Save all milestones in a subgoal
     * 
     * @return	an empty string
     */
    public String saveMilestone()
    {
	//milestones.put();
	
	return "";	
    }

    /**
     * Saves an edited goal in the backend via REST-API
     * 
     * @return responseMsg
     */
    public String updateGoal()
    {
	// add subgoals and milestones before saving
	String responseMsg = goalClient.updateGoal(goal);
	doPageReload();

	return responseMsg;
    }
           
    /**
     * Deletes a goal by its Id
     * 
     * @return	retVal	the status value returned as integer
     */
    public Integer deleteGoal()
    {
	Integer retVal = goalClient.deleteGoalById(goalId);
	doPageReload();
	
	return retVal;
    }
    
    /**
     * Performs actions on creating a new goal 
     */
     public void onNew()
    {
	setButtonValue("Save");   // set label string to "Save"
    }
    
    /**
     * Gets the goal being edited via REST-API
     * 
     * @param	eventMod	the TimelineModificationEvent
     */
    public void onEdit(TimelineModificationEvent eventMod)
    {		
	setButtonValue("Update");   // set label string to "Update"
	
	Long goalId = -1L;
	for (Goal tempGoal: goals)
	{
	    String shortTermFromEvent = eventMod.getTimelineEvent().getData().toString();
	    if (tempGoal.getShortTerm().equals(shortTermFromEvent) == true)
	    {
	       goalId = tempGoal.getId();
	       break;
	    }
	}	
	goal = goalClient.getGoalById(goalId);	
    }
        
    /**
     * Delete an existing goal in the backend via REST-API
     * 
     * RF: we need a confirm dialog here instead sleep(): on deletion db needs 
     *     some time to delete multiple records
     *     
     * @param	eventMod	the TimelineModificationEvent
     */
    public void onDelete(TimelineModificationEvent eventMod)
    {	
	for (Goal tempGoal: goals)
	{
	    String shortTermFromEvent = eventMod.getTimelineEvent().getData().toString();
	    if (tempGoal.getShortTerm().equals(shortTermFromEvent) == true)
	    {
	       goalId = tempGoal.getId();
	       break;
	    }
	}	
    }

    /**
     * Triggers page reload 
     */
    public void doPageReload() 
    {
	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	try
	{
	    ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
	} catch (IOException e)
	{
	    e.printStackTrace();
	}
    }
}
