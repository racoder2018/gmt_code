/**
 * 
 */
package org.gmt.rest.client;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.ejb.Stateless;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.gmt.rest.client.model.Category;


/**
 * The CategoryClient consumes the GMT REST-API for categories
 * CRUD operations
 * 
 * @author Ralf Mehle
 */
@Stateless
public class CategoryClient
{
    private URI uri;
    private Client client;
    
    
    /**
     * Creates a new CategoryClient
     */
    public CategoryClient() 
    {
	ResourceBundle configProps = GmtClientApplication.getConfigProps();	
	uri = UriBuilder
		.fromUri(configProps.getString("client.baseUri") + ":" + 
			configProps.getString("client.port") + 
			"/gmtservice/resources/categories")
		.port(Integer.parseInt(configProps.getString("client.port"))).build();
	
	client = ClientBuilder.newClient();
    }
    
    /**
     * Gets all categories from the GMT rest service 
     * 
     * @return categories
     */
    public List<Category> getAllCategories()
    {	
	Response response = client.target(uri)
		.request(MediaType.APPLICATION_JSON)
	 	.get();

	// JSON data -> list of categories objects
	final JsonObject jsonObject = response.readEntity(JsonObject.class);
	JsonArray results = jsonObject.getJsonArray("results");
	
	List<Category> categories = new ArrayList<>();
	for (int i = 0; i < results.size(); i++)
	{
	    JsonObject jsonCategoryObject = results.get(i).asJsonObject();
	    Category jsonCategory = new Category();
	    jsonCategory.setCategory(jsonCategoryObject.getString("category"));
	    //jsonCategory.setId(Long.valueOf(jsonCategoryObject.getString("id")));
	    
	    categories.add(jsonCategory);
	}	
	return categories;
    }
    
    // ... more Users CRUD functionality goes here.     

    /**
     * Closes the client application
     */
    public void close()
    {
        client.close();
    }
}
