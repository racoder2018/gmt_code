/**
 * 
 */
package org.gmt.rest.client;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.ejb.Stateless;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.gmt.rest.client.model.Status;


/**
 * The StatusClient consumes the GMT REST-API for status
 * CRUD operations
 * 
 * @author Ralf Mehle
 */
@Stateless
public class StatusClient
{
    private URI uri;
    private Client client;
    
    
    /**
     * Creates a new StatusClient
     */
    public StatusClient() 
    {
	ResourceBundle configProps = GmtClientApplication.getConfigProps();	
	uri = UriBuilder
		.fromUri(configProps.getString("client.baseUri") + ":" + 
			configProps.getString("client.port") + 
			"/gmtservice/resources/status")
		.port(Integer.parseInt(configProps.getString("client.port"))).build();
	
	client = ClientBuilder.newClient();
    }
    
    /**
     * Gets all status from the GMT rest service 
     * 
     * @return	status	the status list
     */
    public List<Status> getAllStatus()
    {	
	Response response = client.target(uri)
		.request(MediaType.APPLICATION_JSON)
	 	.get();

	// JSON data -> list of status objects
	final JsonObject jsonObject = response.readEntity(JsonObject.class);
	JsonArray results = jsonObject.getJsonArray("results");
	
	List<Status> status = new ArrayList<>();
	for (int i = 0; i < results.size(); i++)
	{
	    JsonObject jsonStatusObject = results.get(i).asJsonObject();
	    Status jsonStatus = new Status();
	    jsonStatus.setStatus(jsonStatusObject.getString("status"));
	    //jsonStatus.setId(Long.valueOf(jsonStatusObject.getString("id")));
	    
	    status.add(jsonStatus);
	}	
	return status;
    }

    // ... more Users CRUD functionality goes here.     
    
    /**
     * Closes the client application
     */
    public void close()
    {
        client.close();
    }
}
