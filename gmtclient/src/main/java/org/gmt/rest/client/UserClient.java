/**
 * 
 */
package org.gmt.rest.client;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.gmt.rest.client.model.User;

import javax.json.*;


/**
 * The UserClient consumes the GMT REST-API for user login
 * (+ CRUD)
 * 
 * @author Ralf Mehle
 */
@Stateless
public class UserClient
{ 
    private URI uri;
    private Client client;
    
    
    /**
     * Creates a new UserClient
     */
    public UserClient() 
    {
	ResourceBundle configProps = GmtClientApplication.getConfigProps();	
	uri = UriBuilder
		.fromUri(configProps.getString("client.baseUri") + ":" + 
			configProps.getString("client.port") + 
			"/gmtservice/resources/users")
		.port(Integer.parseInt(configProps.getString("client.port"))).build();
	
	client = ClientBuilder.newClient();
    }
    
    /**
     * Gets all users from the GMT rest service 
     * 
     * @return users
     */
    public List<User> getAllUsers()
    {	
	Response response = client.target(uri)
		.request(MediaType.APPLICATION_JSON)
	 	.get();

	// JSON data -> list of user objects
	final JsonObject jsonObject = response.readEntity(JsonObject.class);
	JsonArray results = jsonObject.getJsonArray("results");
	
	List<User> users = new ArrayList<>();
	for (int i = 0; i < results.size(); i++)
	{
	    JsonObject jsonUserObject = results.get(i).asJsonObject();
	    User jsonUser = new User();
	    jsonUser.setUsername(jsonUserObject.getString("loginName"));
	    jsonUser.setPassword(jsonUserObject.getString("password"));
	    //firstname
	    //lastname
	    //id
	    users.add(jsonUser);
	}	
	return users;
    }
    
    // ... more Users CRUD functionality goes here.     

    /**
     * Closes the client application
     */
    public void close()
    {
        client.close();
    }
}
