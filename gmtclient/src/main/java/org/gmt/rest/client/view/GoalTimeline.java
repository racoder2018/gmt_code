/**
 * 
 */
package org.gmt.rest.client.view;

import java.io.Serializable;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Named;

import org.gmt.rest.client.model.Goal;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;


/**
 * Class for definition of the timeline model for displaying the goals 
 *  
 * @author Ralf Mehle
 */
@Named
public class GoalTimeline implements Serializable 
{
    private static final long serialVersionUID = 1L;
    private TimelineModel goalTimelineModel;
    private Date startEvent;  
    private Date endEvent;
    private List<Goal> goalsList;
        
    public GoalTimeline(List<Goal> goals)
    {
	goalsList = goals;        
	init();
    }
    
    
    public void init()
    {
	// set initial start / end dates for the axis of the timeline  
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));  
  
        calendar.set(2018, 1, 1);
        startEvent = calendar.getTime();  
   
        calendar.set(2020, 31, 12);
        endEvent = calendar.getTime();  
        
	createTimeline(calendar);
    }
    
    /**
     * Creates the Timeline chart model component
     * 
     * RF: daylightsaving time in zoneoffset cannot be parsed from the
     *     timestamp string in db/REST-API 
     */
    private void createTimeline(Calendar calendar)
    { 	
	goalTimelineModel = new TimelineModel();
	
	ZoneOffset offset = OffsetDateTime.now().getOffset();	 
	for (int i = 0; i < goalsList.size(); i++)
	{	    
	    Goal goal = goalsList.get(i);
	    
	    Instant instantBegin = goal.getBeginDate().toInstant(offset);
	    calendar.setTime(Date.from(instantBegin));
	    Date startGoal = calendar.getTime();	       
	    
	    Instant instantEnd = goal.getEndDate().toInstant(offset);
	    calendar.setTime(Date.from(instantEnd));	 
	    Date endGoal = calendar.getTime();

	    goalTimelineModel.add(new TimelineEvent(
		    goal.getShortTerm(), startGoal, endGoal, true, null, goal.getShortTerm().toLowerCase()));
	}
    }   

    /**
     * @return the goalTimelineModel
     */
    public TimelineModel getGoalTimelineModel()
    {
        return goalTimelineModel;
    }
  
    /**
     * @return the startEvent
     */
    public Date getStartEvent()
    {
        return startEvent;
    }
    
    /**
     * @param startEvent the startEvent to set
     */
    public void setStartEvent(Date startEvent)
    {
        this.startEvent = startEvent;
    }
    
    /**
     * @return the endEvent
     */
    public Date getEndEvent()
    {
        return endEvent;
    }
    
    /**
     * @param endEvent the endEvent to set
     */
    public void setEndEvent(Date endEvent)
    {
        this.endEvent = endEvent;
    }

    /**
     * @return the goalsList
     */
    public List<Goal> getGoalsList()
    {
        return goalsList;
    }

    /**
     * @param goalsList the goalsList to set
     */
    public void setGoalsList(List<Goal> goalsList)
    {
        this.goalsList = goalsList;
    }
}
