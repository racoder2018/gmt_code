/**
 * 
 */
package org.gmt.rest.client.model;


/**
 * 
 * @author Ralf Mehle
 */
public class Category
{
    private String category;
    private Long id;
   
    
    /**
     * @return the category
     */
    public String getCategory()
    {
        return category;
    }
    
    /**
     * @param category the category to set
     */
    public void setCategory(String category)
    {
        this.category = category;
    }
    
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }        
}
