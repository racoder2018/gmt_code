/**
 * 
 */
package org.gmt.rest.client.model;

import java.util.List;

/**
 * 
 * @author Ralf Mehle
 */
public class Milestone
{
    private String description;
    private String name;
    private Long id;
    private Boolean notifications;
    private List<String> todosFinished;
  
    
    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }
    
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }
    
    /**
     * @return the notifications
     */
    public Boolean getNotifications()
    {
        return notifications;
    }
    
    /**
     * @param notifications the notifications to set
     */
    public void setNotifications(Boolean notifications)
    {
        this.notifications = notifications;
    }
    
    /**
     * @return the todosFinished
     */
    public List<String> getTodosFinished()
    {
        return todosFinished;
    }
    
    /**
     * @param todosFinished the todosFinished to set
     */
    public void setTodosFinished(List<String> todosFinished)
    {
        this.todosFinished = todosFinished;
    }       
}
