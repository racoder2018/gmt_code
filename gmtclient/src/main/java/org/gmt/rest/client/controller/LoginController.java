/**
 * 
 */
package org.gmt.rest.client.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.gmt.rest.client.UserClient;
import org.gmt.rest.client.model.User;


/**
 * The BackingBean 'LoginController' controls login and logout of a user. 
 * 
 * FT: A spinner would be fine. The user logs in and gets no indication
 *     what happens during the login process.
 * 
 * @author Ralf Mehle
 */
@Named
@RequestScoped
public class LoginController implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private String username;
    private String password;
    
    @Inject
    private UserClient userClient;
    private List<User> users;
       
    
    /**
     * @return the username
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    /**
     * Login a valid user, reject an invalid one
     * 
     * @return retVal	the redirect string to the page after login procedure
     */
    public String login()
    {		
	String retVal = "";
	FacesContext context = FacesContext.getCurrentInstance();
	
	users = userClient.getAllUsers();
	for (User tempUser : users)
	{
	    if (this.username.equals(tempUser.getUsername()) && 
		    this.password.equals(tempUser.getPassword()))
	    {
		context.getExternalContext().getSessionMap().put("user", username);
		//context.getExternalContext().redirect("main.xhtml");
		retVal = "main";
		break;
	    }
	    else
	    {
		retVal = "reject";
	    }
	}
	return retVal;
    }
}
