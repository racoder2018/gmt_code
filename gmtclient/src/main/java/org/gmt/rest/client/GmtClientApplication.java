/**
 * 
 */
package org.gmt.rest.client;

import java.util.ResourceBundle;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;


/**
 * Initialization class for the Goals Management Tool (GMT) 
 * JSF client application 
 * 
 * Note: The authentication mechanism is based on the Java EE8 Security API
 * 
 * @author Ralf Mehle
 */
@FacesConfig
@ApplicationScoped
public class GmtClientApplication 
{   
    private static ResourceBundle configProps = ResourceBundle.getBundle("config");


    /**
     * @return the configProps
     */
    public static ResourceBundle getConfigProps()
    {
	return configProps;
    }
}
