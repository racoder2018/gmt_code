/**
 * 
 */
package org.gmt.rest.client.controller;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.gmt.rest.client.StatusClient;
import org.gmt.rest.client.model.Status;


/**
 * The BackingBean 'StatusController' controls CRUD operations for status. 
 * 
 * @author Ralf Mehle
 */
@Named
@RequestScoped
public class StatusController implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    @Inject
    private StatusClient statusClient;
    private Map<String,Object> allStatus;

    
    @PostConstruct
    public void init() 
    {
	// Gets all status defined via REST-API call
	List<Status> status = statusClient.getAllStatus();
	
	allStatus = new LinkedHashMap<String,Object>();
	for (Status tempStatus : status)
	{
	    allStatus.put(tempStatus.getStatus().toString(), tempStatus.getStatus());
	}     
    }    
    
    /**
     * Returns all status
     * 
     * @return	allStatus	a map containing all status 
     */
    public Map<String,Object> getAllStatus()
    {	
	return allStatus;
    }
}
