/**
 * 
 */
package org.gmt.rest.client.model;

import java.time.LocalDateTime;
import java.util.List;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.Status;

/**
 * 
 * @author Ralf Mehle
 */
public class SubGoal
{
    private LocalDateTime beginDate;
    private LocalDateTime endDate;
    private String description;
    private String shortTerm;
    private Long id;
    private Category category;
    private Status status;
    private List<Milestone> milestones;
    private Boolean notifications;
   
    
    /**
     * @return the beginDate
     */
    public LocalDateTime getBeginDate()
    {
        return beginDate;
    }
    
    /**
     * @param beginDate the beginDate to set
     */
    public void setBeginDate(LocalDateTime beginDate)
    {
        this.beginDate = beginDate;
    }
    
    /**
     * @return the endDate
     */
    public LocalDateTime getEndDate()
    {
        return endDate;
    }
    
    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDateTime endDate)
    {
        this.endDate = endDate;
    }
    
    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    /**
     * @return the shortTerm
     */
    public String getShortTerm()
    {
        return shortTerm;
    }
    
    /**
     * @param shortTerm the shortTerm to set
     */
    public void setShortTerm(String shortTerm)
    {
        this.shortTerm = shortTerm;
    }
    
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }
    
    /**
     * @return the category
     */
    public Category getCategory()
    {
        return category;
    }
    
    /**
     * @param category the category to set
     */
    public void setCategory(Category category)
    {
        this.category = category;
    }
    
    /**
     * @return the status
     */
    public Status getStatus()
    {
        return status;
    }
    
    /**
     * @param status the status to set
     */
    public void setStatus(Status status)
    {
        this.status = status;
    }
    
    /**
     * @return the milestones
     */
    public List<Milestone> getMilestones()
    {
        return milestones;
    }
    
    /**
     * @param milestones the milestones to set
     */
    public void setMilestones(List<Milestone> milestones)
    {
        this.milestones = milestones;
    }
    
    /**
     * @return the notifications
     */
    public Boolean getNotifications()
    {
        return notifications;
    }
    
    /**
     * @param notifications the notifications to set
     */
    public void setNotifications(Boolean notifications)
    {
        this.notifications = notifications;
    }       
}
