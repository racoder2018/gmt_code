/**
 * 
 */
package org.gmt.rest.client.controller;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.gmt.rest.client.CategoryClient;
import org.gmt.rest.client.model.Category;


/**
 * The BackingBean 'CategoryController' controls CRUD operations for categories. 
 * @author Ralf Mehle
 */
@Named
@RequestScoped
public class CategoryController implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    @Inject
    private CategoryClient categoryClient;
    private Map<String,Object> allCategories;

    
    @PostConstruct
    public void init() 
    {
	// Gets all categories defined via REST-API call
	List<Category> categories = categoryClient.getAllCategories();
	
	allCategories = new LinkedHashMap<String,Object>();
	for (Category tempCategory : categories)
	{
	    allCategories.put(tempCategory.getCategory().toString(), tempCategory.getCategory());
	}     
    }  
    
    /**
     * Gets all categories 
     * 
     * @return	allCategories	the categories
     */
    public Map<String,Object>  getAllCategories()
    {	
	return allCategories;
    }
}