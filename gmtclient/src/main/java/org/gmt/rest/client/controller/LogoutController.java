/**
 * 
 */
package org.gmt.rest.client.controller;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


/**
 * LogoutController called when logout is performed
 * 
 * @author Ralf Mehle
 */
@Named
@RequestScoped
public class LogoutController
{
    /**
     * Logout
     * 
     * @return "login"
     */
    public String logout() 
    {
	FacesContext context = FacesContext.getCurrentInstance();
	context.getExternalContext().invalidateSession();

        return "login";
    }
}
