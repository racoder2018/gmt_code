/**
 * 
 */
package org.gmt.rest.client;

import java.io.Serializable;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.gmt.persistence.model.Category;
import org.gmt.persistence.model.Status;
import org.gmt.rest.client.model.Goal;


/**
 *  The GoalsClient consumes the GMT REST-API for a user's 
 *  goals (CRUD functionality)
 *  
 * @author Ralf Mehle
 */
public class GoalClient implements Serializable
{ 
    private static final long serialVersionUID = 1L;

    private URI uri;
    private Client client;
    private ResourceBundle configProps;
    
    /**
     * Creates a new GoalsClient
     */
    public GoalClient() 
    {
	configProps = GmtClientApplication.getConfigProps();	
	uri = UriBuilder
		.fromUri(configProps.getString("client.baseUri") + ":" + 
			configProps.getString("client.port") + 
			"/gmtservice/resources/goals")
		.port(Integer.parseInt(configProps.getString("client.port"))).build();
	
	client = ClientBuilder.newClient();
    }
   
    /**
     * Closes the client application
     */
    public void close()	
    {
        client.close();
    }
    
    /**
     * Gets all goals from the GMT rest service 
     * 
     * @return goals
     */
    public List<Goal> getAllGoals()
    {
	Response response = client.target(uri)
		.request(MediaType.APPLICATION_JSON)
	 	.get();

	// JSON data -> list of goal objects
	final JsonObject jsonObject = response.readEntity(JsonObject.class);
	JsonArray results = jsonObject.getJsonArray("results");

	List<Goal> goals = new ArrayList<>();
	for (int i = 0; i < results.size(); i++)
	{
	    JsonObject jsonGoalObject = results.get(i).asJsonObject();
	    Goal goal = new Goal();
	    try
	    {
		goal.setBeginDate(LocalDateTime.parse(jsonGoalObject.getString("beginDate")));
		goal.setEndDate(LocalDateTime.parse(jsonGoalObject.getString("endDate")));
		goal.setDescription(jsonGoalObject.getString("description"));
		goal.setShortTerm(jsonGoalObject.getString("shortTerm"));
		goal.setId(Integer.valueOf(jsonGoalObject.getInt("id")).longValue());
		goal.setCategory(Category.valueOf(jsonGoalObject.getString("category"))); 
		goal.setStatus(Status.valueOf(jsonGoalObject.getString("status")));
		//jsonGoal.setSubGoals(jsonUserObject.getString("subGoals"));
		goal.setNotifications(jsonGoalObject.getBoolean("notifications"));
	    } catch (RuntimeException re)
	    {
		System.err.println("The Goal object cannot be built correctly. " + 
			"May be there are no JSON values in the response to be parsed: " + 
			re.getMessage() + re.getStackTrace());
	    }
	    goals.add(goal);
	}	
	return goals;
    }
      
    /**
     * Gets a goal by its id
     * 
     * @param	goalId	the Goal-Id as long value	
     * @return 	goal	the Goal object
     */
    public Goal getGoalById(Long goalId)
    {
	if (goalId == null) return null;

	String path  = uri.getPath();
	String regex = new String("/goals");
	URI uri	     = null;
	
	if (path.contains(regex) == true)
	{	    
	    uri = UriBuilder
			.fromUri(configProps.getString("client.baseUri") + ":" + 
				configProps.getString("client.port") + 
				path.replace(regex, regex + "/" + goalId))
			.port(Integer.parseInt(configProps.getString("client.port"))).build();
	}
	Response response = client.target(uri).request(MediaType.APPLICATION_JSON).get();    			

	// JSON data -> a goal object
	final JsonObject jsonObject = response.readEntity(JsonObject.class);
	JsonArray results = jsonObject.getJsonArray("results");
	JsonObject jsonGoalObject = results.get(0).asJsonObject();
	
	Goal goal = new Goal();    
	try
	{
	    goal.setBeginDate(LocalDateTime.parse(jsonGoalObject.getString("beginDate")));
	    goal.setEndDate(LocalDateTime.parse(jsonGoalObject.getString("endDate")));
	    goal.setDescription(jsonGoalObject.getString("description"));
	    goal.setShortTerm(jsonGoalObject.getString("shortTerm"));
	    goal.setId(Integer.valueOf(jsonGoalObject.getInt("id")).longValue());
	    goal.setCategory(Category.valueOf(jsonGoalObject.getString("category"))); 
	    goal.setStatus(Status.valueOf(jsonGoalObject.getString("status")));
	    //jsonGoal.setSubGoals(jsonUserObject.getString("subGoals"));
	    goal.setNotifications(jsonGoalObject.getBoolean("notifications"));	
	} catch (RuntimeException re)
	{
	    System.err.println("The Goal object cannot be built correctly. " + 
		    "May be there are no JSON values in the response to be parsed: " + 
		    re.getMessage() + re.getStackTrace());
	}
	return goal;
    }

    /**
     * Creates a new goal via REST-API, formatted in JSON
     * 
     * @param	goal		the Goal object
     * @return	responseString	the response from the entity after creation
     */
    public String createGoal(Goal goal)
    {	
	WebTarget target = client.target(uri);
	Entity<Goal> entity = Entity.json(goal);
	Response response = target.request().post(entity);
	
	String responseString = response.readEntity(String.class);
	response.close();
	
	// clear all values for the dialog ...
	goal.clear();
	
        return responseString;
    }
    
    /**
     * Updates a goal via REST-API, formatted in JSON
     * 
     * @param	goal		the Goal object
     * @return	responseString	the response from the entity after update
     */
    public String updateGoal(Goal goal)
    {
	if (goal == null) return null;
	
	String path   = uri.getPath();
	String regex  = new String("/goals");
	URI uri       = null;
	
	if (path.contains(regex) == true)
	{	    
	    uri = UriBuilder
			.fromUri(configProps.getString("client.baseUri") + ":" + 
				configProps.getString("client.port") + 
				path.replace(regex, regex + "/" + goal.getId()))
			.port(Integer.parseInt(configProps.getString("client.port"))).build();
	}	
	
	WebTarget target = client.target(uri);
	Entity<Goal> entity = Entity.json(goal);
	Response response = target.request().put(entity);
	
	String responseString = response.readEntity(String.class);
	response.close();
	
	// clear all values in the form ...
	goal.clear();
	
        return responseString;
    }
    
    /**
     * Deletes a goal by given id in the backend
     * 
     * @param	goalId		the Goal-Id as long value
     * @return 	responseStatus	the response Status as integer after deletion
     */
    public Integer deleteGoalById(Long goalId)
    {
	if (goalId == null) return null;
	
	String path   = uri.getPath();
	String regex  = new String("/goals");
	URI uri       = null;
	
	if (path.contains(regex) == true)
	{	    
	    uri = UriBuilder
			.fromUri(configProps.getString("client.baseUri") + ":" + 
				configProps.getString("client.port") + 
				path.replace(regex, regex + "/" + goalId))
			.port(Integer.parseInt(configProps.getString("client.port"))).build();
	}
	
	Response response = client.target(uri).request(MediaType.APPLICATION_JSON).delete();    			
	return new Integer(response.getStatus());
    }
}
